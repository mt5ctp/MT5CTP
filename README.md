# MT5CTP

#### 介绍

- MT5CTP项目是MT5客户端直连CTP服务器，实现国内期货交易的投资管理工具。
- MT5CTP项目使用mt5的开放性构件，精心设计和实现，充分调用mt5客户端功能和mql5编译开发环境，实现策略开发、历史数据回测、仿真交易和实盘测试，实现量化投资工具的闭环。mt5丰富的软件生态和历史沉淀，使得MT5CTP项目落地就具备高水准，高可用性。
- 基于MQL5开发和运行量化投资策略的几个核心优势：1.事件驱动机制。区别于市场流行的tick驱动机制的商业软件，MQL5内置事件驱动机制，这一机制使策略的设计、开发与运营具有无限可能，除了标准的tick事件、order事件、trade事件、timer事件等之外，MQL5具有自定义事件的生发和处理能力，实现的非常的优雅和高效；2.支持类封装。市场流行的程序化交易软件，提供的脚本语言基本都是面向过程的，复杂的处理有些力不从心，MQL5支持面向对象的开发模式，使得开发语言具有较强的描述能力，并且提供了最大程度的代码重用机制，执行效率接近c++语言，使得高级算法、复杂算法的开发和部署成为可能；3.支持界面自定义。MQL5的开放架构，支持界面开发，客户端可以因人而异，因业务而不同，本地“小”客户端，具有“大”软件平台的支撑能力，难能可贵。
- MT5CTP项目链接MT5客户端和CTP期货交易柜台，给实战国内期货量化投资的朋友们，提供更多选择，提供更为得心应手的工具。

#### 软件架构
![MT5CTP项目架构](https://images.gitee.com/uploads/images/2021/0325/183208_423fda7c_8860760.jpeg "软件结构图.jpg")
- （没有绘图的天分，抱歉，这图我自己都觉得丑）

#### 安装、使用、开发、问答
0.【MT5CTP培训服务】https://guorui-mt5ctp.feishu.cn/wiki/wikcnwpMDdFmoGyOh6tsU4ofjUg

1.【MT5CTP(期货)2.00 build 0025】下载

码云：https://gitee.com/mt5ctp/MT5CTP/releases

飞书：https://guorui-mt5ctp.feishu.cn/wiki/wikcn140hHSVMRB6iOJo0n7A3Wd#mKEq2d

2.【MT5STP(证券)2.00 build 0008】下载

码云：https://gitee.com/mt5ctp/MT5STP/releases

飞书：https://guorui-mt5ctp.feishu.cn/wiki/wikcntmccLmIn97tyYCBdL3anMc。

3.【MT5CTP&MT5STP问答】：https://guorui-mt5ctp.feishu.cn/wiki/wikcnAWXTnUhji48ArTcPswCMXd。

4.【MT5CTP&MT5STP说明文档、开发手册、视频教程】

飞书：https://guorui-mt5ctp.feishu.cn/wiki/wikcng6BSxjFtrlStRdY0C5bLib

知乎：https://www.zhihu.com/column/c_1248950995830595584。

5.【MT5CTP五分钟视频教程】https://guorui-mt5ctp.feishu.cn/wiki/wikcnia07ByQuavCbjHFfjomc9f

6.【MT5CTP零基础视频教程】https://zhuanlan.zhihu.com/p/574403689

7.【MT5STP(证券)说明文档、开发手册、视频教程(知乎专栏)】https://www.zhihu.com/column/c_1405943369189785601。

8.【MT5STP(证券)说明文档、开发手册】https://guorui-mt5ctp.feishu.cn/wiki/wikcnQKV9sGaJEUgjRew7ZG4ifj。

9. 关于账户的回测、模拟以及实盘【重要】 

9.1 simnow账户回测、模拟永久免费。上期技术simnow仿真交易账户申请（交易日）：http://www.simnow.com.cn/ 。

9.2 MT5CTP系统外接与实盘服务申请(https://guorui-mt5ctp.feishu.cn/wiki/wikcnc7RCMxwJFB7DbLLVZxECph)。

9.3【MT5CTP】已实盘开通期货公司:https://guorui-mt5ctp.feishu.cn/wiki/wikcnpvCJc5CX5WeWhq9atLHhrf，不在列表里的期货公司账户需要联系期货公司做程序化交易报备和穿透测试（测试部分项目组跟进）。

10.【MT5CTP】与【英大期货公司济南营业部】有合作，手续费有优惠。

11.【MT5STP】开通华鑫证券实盘直通车，开户可联系项目组或微信群的华鑫客户经理。