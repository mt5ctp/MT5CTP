//+------------------------------------------------------------------+
//|                                                  mt5ctptools.mq5 |
//+------------------------------------------------------------------+
#include <mt5ctp\toolbox.mqh>
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
ToolBox toolbox;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
//--- prepare the chart
   ChartSetInteger(0,CHART_SHOW,false);
   ChartSetInteger(0,CHART_IS_DOCKED,false);
   ChartRedraw();
//---设置推荐人
   toolbox.SetRecommender("我是居间人/员工,ID:");
//---初始化
   toolbox.OnInitEvent();
//---创建界面 
   if(!toolbox.Create(APP_NAME,0,0,(int)ChartGetInteger(0,CHART_WIDTH_IN_PIXELS),(int)ChartGetInteger(0,CHART_HEIGHT_IN_PIXELS)))
   {
      return(INIT_FAILED);
   }
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
   toolbox.OnDeinitEvent(reason);
   ChartSetInteger(0,CHART_SHOW,true);
   ChartSetInteger(0,CHART_IS_DOCKED,true);
   ChartRedraw();
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
//---
}
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
{
//---
   toolbox.OnTimerEvent();
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
{
//---
   toolbox.ChartEvent(id,lparam,dparam,sparam);
}
//+------------------------------------------------------------------+
