//+------------------------------------------------------------------+
//|                                                   SymbolInfo.mqh |
//+------------------------------------------------------------------+
#property copyright     "Copyright 2020,Guorui Holding (Shandong) Co.,Ltd."
#property version       "1.50"
#property link          ""
#property description   "本程序仅用于量化投资爱好者测试、研究。程序使用者承诺："
#property description   "1.本人/本单位熟知本程序的适用范围，自愿承担因商业应用造成的违法、违规、侵权责任。"
#property description   "2.本人/本单位熟知金融衍生品的各种交易及代理风险，并自愿承担损失及违法、违规、违约责任。"
#property description   "3.本人/本单位熟知计算机程序的各种固有风险，并自愿承担损失及违法、违规、违约责任。"
#property description   "4.本人/本单位熟知金融监管规则，依法依规参与金融市场，并自愿承担损失及违法、违规责任。"
#property description   "5.本人/本单位认可开始使用本程序，视为同意并做出上述承诺，停用本程序后，上述承诺依然有效。"
//+------------------------------------------------------------------+
//| include 外部依赖                                                 |
//+------------------------------------------------------------------+
#include <Object.mqh>
#include "mt5ctp.mqh"
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
class CSymbolInfo : public CObject
  {
protected:
   string            m_symbol;      //合约代码
   bool              m_custom_ctp;  //是否自定义ctp合约
public:
                     CSymbolInfo(void);
                    ~CSymbolInfo(void);
   //--- fast access methods to the string symbol propertyes
   //合约代码
   string            Symbol(void) const { return(m_symbol); }
   //合约名称
   string            Name(void) const;
   //交易所代码
   string            ExchangeID(void) const;
   //合约在交易所的代码
   string            ExchangeInstID(void) const;
   //产品代码
   string            ProductID(void) const;
   //产品名称
   string            ProductName(void) const;
   //交易所产品代码
   string            ExchangeProductID(void) const;
   //基础商品代码
   string            UnderlyingInstrID(void) const;
   //交易币种
   string            Currency(void) const;
   //创建日
   string            CreateDate(void) const;
   //上市日
   string            OpenDate(void) const;
   //到期日
   string            ExpireDate(void) const;
   //开始交割日
   string            StartDelivDate(void) const;
   //结束交割日
   string            EndDelivDate(void) const;
   //进入本状态时间
   string            EnterTime(void) const;
   //交易日(行情)
   string            TradingDay(void) const;
   //业务日期(行情)
   string            ActionDay(void) const;
   //最后更新时间(行情)
   string            UpdateTime(void) const;
   //交易时段开始时间
   string            SessionStart(int index) const;
   //交易时段结束时间
   string            SessionEnd(int index) const;
   //--- fast access methods to the double symbol propertyes
   //最小变动价位
   double            PriceTick(void) const;
   //合约基础商品乘数
   double            UnderlyingMultiple(void) const;
   //执行价
   double            StrikePrice(void) const;
   //最新价
   double            LastPrice(void) const;
   //昨结算价
   double            PreSettlement(void) const;
   //昨收盘
   double            PreClose(void) const;
   //今开盘
   double            Open(void) const;
   //最高价
   double            High(void) const;
   //最低价
   double            Low(void) const;
   //今收盘
   double            Close(void) const;
   //今结算价
   double            Settlement(void) const;
   //涨停板价
   double            UpperLimit(void) const;
   //跌停板价
   double            LowerLimit(void) const;
   //申买价一
   double            Bid(void) const;
   //申卖价一
   double            Ask(void) const;
   //申买价二
   double            Bid2(void) const;
   //申卖价二
   double            Ask2(void) const;
   //申买价三
   double            Bid3(void) const;
   //申卖价三
   double            Ask3(void) const;
   //申买价四
   double            Bid4(void) const;
   //申卖价四
   double            Ask4(void) const;
   //申买价五
   double            Bid5(void) const;
   //申卖价五
   double            Ask5(void) const;
   //当日均价
   double            AvgPrice(void) const;
   //成交金额
   double            Turnover(void) const;
   //昨持仓量
   double            PreOpenInterest(void) const;
   //今持仓量
   double            OpenInterest(void) const;
   //昨虚实度
   double            PreDelta(void) const;
   //今虚实度
   double            Delta(void) const;
   //多头保证金率
   double            LongMarginRatioByMoney(void) const;
   //多头保证金费
   double            LongMarginRatioByVolume(void) const;
   //空头保证金率
   double            ShortMarginRatioByMoney(void) const;
   //空头保证金费
   double            ShortMarginRatioByVolume(void) const;
   //交易所多头保证金率
   double            ExchangeLongMarginRatioByMoney(void) const;
   //交易所多头保证金费
   double            ExchangeLongMarginRatioByVolume(void) const;
   //交易所空头保证金率
   double            ExchangeShortMarginRatioByMoney(void) const;
   //交易所空头保证金费
   double            ExchangeShortMarginRatioByVolume(void) const;
   //开仓手续费率
   double            OpenRatioByMoney(void) const;
   //开仓手续费
   double            OpenRatioByVolume(void) const;
   //平仓手续费率
   double            CloseRatioByMoney(void) const;
   //平仓手续费
   double            CloseRatioByVolume(void) const;
   //平今仓手续费率
   double            CloseTodayRatioByMoney(void) const;
   //平今仓手续费
   double            CloseTodayRatioByVolume(void) const;
   //报单手续费
   double            OrderCommByVolume(void) const;
   //撤单手续费
   double            OrderActionCommByVolume(void) const;
   //--- fast access methods to the integer symbol propertyes
   //产品类型
   char              ProductClass(void) const;
   string            ProductClassDescription(void) const;
   //交割年份
   long              DeliveryYear(void) const;
   //交割月份
   long              DeliveryMonth(void) const;
   //市价最大下单量
   long              MaxMarketOrderVolume(void) const;
   //市价最小下单量
   long              MinMarketOrderVolume(void) const;
   //限价最大下单量
   long              MaxLimitOrderVolume(void) const;
   //限价最小下单量
   long              MinLimitOrderVolume(void) const;
   //合约数量/乘数
   long              ContractSize(void) const;
   //小数点位数
   long              Digits(void) const;
   //合约生命周期状态
   char              InstLifePhase(void) const;
   string            InstLifePhaseDescription(void) const;
   //当前是否交易
   bool              IsTrading(void) const;
   //持仓类型
   char              PositionType(void) const;
   string            PositionTypeDescription(void) const;
   //持仓日期类型
   char              PositionDateType(void) const;
   string            PositionDateTypeDescription(void) const;
   //平仓处理类型
   char              CloseDealType(void) const;
   string            CloseDealTypeDescription(void) const;
   //质押资金可用范围
   char              MortgageFundUseRange(void) const;
   string            MortgageFundUseRangeDescription(void) const;
   //是否使用大额单边保证金算法
   char              MaxMarginSideAlgorithm(void) const;
   string            MaxMarginSideAlgorithmDescription(void) const;
   //期权类型
   char              OptionsType(void) const;
   string            OptionsTypeDescription(void) const;
   //组合类型
   char              CombinationType(void) const;
   string            CombinationTypeDescription(void) const;
   //是否指数合约
   bool              IsIndex(void) const;
   //是否主力合约
   bool              IsMain(void) const;
   //是否已订阅行情
   bool              IsSubMarket(void) const;
   //是否交易时间
   bool              SymbolExists(void) const;
   //合约交易状态
   char              Status(void) const;
   string            StatusDescription(void) const;
   //进入本状态原因
   char              EnterReason(void) const;
   string            EnterReasonDescription(void) const;
   //成交数量（行情）
   long              Volume(void) const;
   //最后修改毫秒（行情）
   long              UpdateMillisec(void) const;
   //申买量一（行情）
   long              BidVolume(void) const;
   //申卖量一（行情）
   long              AskVolume(void) const;
   //申买量二（行情）
   long              Bid2Volume(void) const;
   //申卖量二（行情）
   long              Ask2Volume(void) const;
   //申买量三（行情）
   long              Bid3Volume(void) const;
   //申卖量三（行情）
   long              Ask3Volume(void) const;
   //申买量四（行情）
   long              Bid4Volume(void) const;
   //申卖量四（行情）
   long              Ask4Volume(void) const;
   //申买量五（行情）
   long              Bid5Volume(void) const;
   //申卖量五（行情）
   long              Ask5Volume(void) const;
   //交易时段数
   int               SessionsTotal(void) const;
   //交易时段开始时间/本地时间
   datetime          SessionStartTime(int index) const;
   //交易时段结束时间/本地时间
   datetime          SessionEndTime(int index) const;
   //--- access methods to the API MQL5 functions
   bool              InfoInteger(const CTP::ENUM_SYMBOL_INFO_INTEGER prop_id,long& var) const;
   bool              InfoDouble(const CTP::ENUM_SYMBOL_INFO_DOUBLE prop_id,double& var) const;
   bool              InfoString(const CTP::ENUM_SYMBOL_INFO_STRING prop_id,string& var) const;
   //--- service methods
   double            NormalizePrice(const double price) const;
   //--- method for select symbol order
   bool              Select(const string symbol=NULL);
  };
//+------------------------------------------------------------------+
//| Constructor                                                      |
//+------------------------------------------------------------------+
CSymbolInfo::CSymbolInfo(void)
   :m_symbol(NULL)
   ,m_custom_ctp(false)
  {
  }
//+------------------------------------------------------------------+
//| Destructor                                                       |
//+------------------------------------------------------------------+
CSymbolInfo::~CSymbolInfo(void)
  {
  }
//+------------------------------------------------------------------+
//| fast access methods to the string symbol propertyes              |
//+------------------------------------------------------------------+
//合约名称
string CSymbolInfo::Name(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_InstrumentName);
      if(::StringLen(str_res)==0)
        {
         str_res = ::SymbolInfoString(m_symbol,SYMBOL_DESCRIPTION);
        }
     }
   return(str_res);
  }
//交易所代码
string CSymbolInfo::ExchangeID(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_ExchangeID);
      if(::StringLen(str_res)==0)
        {
         str_res = ::SymbolInfoString(m_symbol,SYMBOL_EXCHANGE);
        }
     }
   return(str_res);
  }
//合约在交易所的代码
string CSymbolInfo::ExchangeInstID(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_ExchangeInstID);
     }
   return(str_res);
  }
//产品代码
string CSymbolInfo::ProductID(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_ProductID);
      if(::StringLen(str_res)==0)
        {
         str_res = ::SymbolInfoString(m_symbol,SYMBOL_CATEGORY);
        }
     }
   return(str_res);
  }
//产品名称
string CSymbolInfo::ProductName(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_ProductName);
     }
   return(str_res);
  }
//交易所产品代码
string CSymbolInfo::ExchangeProductID(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_ExchangeProductID);
     }
   return(str_res);
  }
//基础商品代码
string CSymbolInfo::UnderlyingInstrID(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_UnderlyingInstrID);
     }
   return(str_res);
  }
//交易币种
string CSymbolInfo::Currency(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_TradeCurrencyID);
      if(::StringLen(str_res)==0)
        {
         str_res = ::SymbolInfoString(m_symbol,SYMBOL_CURRENCY_BASE);
        }
     }
   return(str_res);
  }
//创建日
string CSymbolInfo::CreateDate(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_CreateDate);
     }
   return(str_res);
  }
//上市日
string CSymbolInfo::OpenDate(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_OpenDate);
      if(::StringLen(str_res)==0)
        {
         str_res = ::TimeToString(::SymbolInfoInteger(m_symbol,SYMBOL_START_TIME),TIME_DATE);
        }
     }
   return(str_res);
  }
//到期日
string CSymbolInfo::ExpireDate(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_ExpireDate);
      if(::StringLen(str_res)==0)
        {
         str_res = ::TimeToString(::SymbolInfoInteger(m_symbol,SYMBOL_EXPIRATION_TIME),TIME_DATE);
        }
     }
   return(str_res);
  }
//开始交割日
string CSymbolInfo::StartDelivDate(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_StartDelivDate);
     }
   return(str_res);
  }
//结束交割日
string CSymbolInfo::EndDelivDate(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_EndDelivDate);
     }
   return(str_res);
  }
//进入本状态时间
string CSymbolInfo::EnterTime(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_EnterTime);
     }
   return(str_res);
  }
//交易日(行情)
string CSymbolInfo::TradingDay(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_TradingDay);
     }
   return(str_res);
  }
//业务日期(行情)
string CSymbolInfo::ActionDay(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_ActionDay);
     }
   return(str_res);
  }
//最后更新时间(行情)
string CSymbolInfo::UpdateTime(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SymbolInfoString(m_symbol,CTP::SYMBOL_UpdateTime);
     }
   return(str_res);
  }
//交易时段开始时间
string CSymbolInfo::SessionStart(int index) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SessionStart(m_symbol,index);
     }
   return(str_res);
  }
//交易时段结束时间
string CSymbolInfo::SessionEnd(int index) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      str_res = CTP::SessionEnd(m_symbol,index);
     }
   return(str_res);
  }
//+------------------------------------------------------------------+
//| fast access methods to the double symbol propertyes              |
//+------------------------------------------------------------------+
//最小变动价位
double CSymbolInfo::PriceTick(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_PriceTick);
      if(dbl_res<=0)
        {
         dbl_res = ::SymbolInfoDouble(m_symbol,SYMBOL_TRADE_TICK_SIZE);
        }
     }
   return(dbl_res);
  }
//合约基础商品乘数
double CSymbolInfo::UnderlyingMultiple(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_UnderlyingMultiple);
     }
   return(dbl_res);
  }
//执行价
double CSymbolInfo::StrikePrice(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_StrikePrice);
      if(dbl_res<=0)
        {
         dbl_res = ::SymbolInfoDouble(m_symbol,SYMBOL_OPTION_STRIKE);
        }
     }
   return(dbl_res);
  }
//最新价
double CSymbolInfo::LastPrice(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_LastPrice);
      if(dbl_res<=0)
        {
         dbl_res = ::SymbolInfoDouble(m_symbol,SYMBOL_LAST);
        }
     }
   return(dbl_res);
  }
//昨结算价
double CSymbolInfo::PreSettlement(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_PreSettlementPrice);
     }
   return(dbl_res);
  }
//昨收盘
double CSymbolInfo::PreClose(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_PreClosePrice);
     }
   return(dbl_res);
  }
//今开盘
double CSymbolInfo::Open(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_OpenPrice);
     }
   return(dbl_res);
  }
//最高价
double CSymbolInfo::High(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_HighestPrice);
     }
   return(dbl_res);
  }
//最低价
double CSymbolInfo::Low(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_LowestPrice);
     }
   return(dbl_res);
  }
//今收盘
double CSymbolInfo::Close(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_ClosePrice);
      if(dbl_res<=0)
        {
         dbl_res = ::SymbolInfoDouble(m_symbol,SYMBOL_LAST);
        }
     }
   return(dbl_res);
  }
//今结算价
double CSymbolInfo::Settlement(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_SettlementPrice);
     }
   return(dbl_res);
  }
//涨停板价
double CSymbolInfo::UpperLimit(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_UpperLimitPrice);
      if(dbl_res<=0)
        {
         dbl_res = SymbolInfoDouble(m_symbol,SYMBOL_SESSION_PRICE_LIMIT_MAX);
        }
     }
   return(dbl_res);
  }
//跌停板价
double CSymbolInfo::LowerLimit(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_LowerLimitPrice);
      if(dbl_res<=0)
        {
         dbl_res = SymbolInfoDouble(m_symbol,SYMBOL_SESSION_PRICE_LIMIT_MIN);
        }
     }
   return(dbl_res);
  }
//申买价一
double CSymbolInfo::Bid(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_BidPrice1);
      if(dbl_res<=0)
        {
         dbl_res = ::SymbolInfoDouble(m_symbol,SYMBOL_BID);
        }
     }
   return(dbl_res);
  }
//申卖价一
double CSymbolInfo::Ask(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_AskPrice1);
      if(dbl_res<=0)
        {
         dbl_res = ::SymbolInfoDouble(m_symbol,SYMBOL_ASK);
        }
     }
   return(dbl_res);
  }
//申买价二
double CSymbolInfo::Bid2(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_BidPrice2);
     }
   return(dbl_res);
  }
//申卖价二
double CSymbolInfo::Ask2(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_AskPrice2);
     }
   return(dbl_res);
  }
//申买价三
double CSymbolInfo::Bid3(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_BidPrice3);
     }
   return(dbl_res);
  }
//申卖价三
double CSymbolInfo::Ask3(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_AskPrice3);
     }
   return(dbl_res);
  }
//申买价四
double CSymbolInfo::Bid4(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_BidPrice4);
     }
   return(dbl_res);
  }
//申卖价四
double CSymbolInfo::Ask4(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_AskPrice4);
     }
   return(dbl_res);
  }
//申买价五
double CSymbolInfo::Bid5(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_BidPrice5);
     }
   return(dbl_res);
  }
//申卖价五
double CSymbolInfo::Ask5(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_AskPrice5);
     }
   return(dbl_res);
  }
//当日均价
double CSymbolInfo::AvgPrice(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_AveragePrice);
      if(dbl_res<=0)
        {
         dbl_res = ::SymbolInfoDouble(m_symbol,SYMBOL_SESSION_AW);
        }
     }
   return(dbl_res);
  }
//成交金额
double CSymbolInfo::Turnover(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_Turnover);
      if(dbl_res<=0)
        {
         dbl_res = ::SymbolInfoDouble(m_symbol,SYMBOL_SESSION_TURNOVER);
        }
     }
   return(dbl_res);
  }
//昨持仓量
double CSymbolInfo::PreOpenInterest(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_PreOpenInterest);
     }
   return(dbl_res);
  }
//今持仓量
double CSymbolInfo::OpenInterest(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_OpenInterest);
     }
   return(dbl_res);
  }
//昨虚实度
double CSymbolInfo::PreDelta(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_PreDelta);
     }
   return(dbl_res);
  }
//今虚实度
double CSymbolInfo::Delta(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_CurrDelta);
      if(dbl_res<=0)
        {
         dbl_res = ::SymbolInfoDouble(m_symbol,SYMBOL_PRICE_DELTA);
        }
     }
   return(dbl_res);
  }
//多头保证金率
double CSymbolInfo::LongMarginRatioByMoney(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_LongMarginRatioByMoney);
      if(dbl_res<=0.0)
        {
         dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_LongMarginRatio);
        }
     }
   return(dbl_res);
  }
//多头保证金费
double CSymbolInfo::LongMarginRatioByVolume(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_LongMarginRatioByVolume);
     }
   return(dbl_res);
  }
//空头保证金率
double CSymbolInfo::ShortMarginRatioByMoney(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_ShortMarginRatioByMoney);
      if(dbl_res<=0.0)
        {
         dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_ShortMarginRatio);
        }
     }
   return(dbl_res);
  }
//空头保证金费
double CSymbolInfo::ShortMarginRatioByVolume(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_ShortMarginRatioByVolume);
     }
   return(dbl_res);
  }
//交易所多头保证金率
double CSymbolInfo::ExchangeLongMarginRatioByMoney(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_ExchangeLongMarginRatioByMoney);
     }
   return(dbl_res);
  }
//交易所多头保证金费
double CSymbolInfo::ExchangeLongMarginRatioByVolume(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_ExchangeLongMarginRatioByVolume);
     }
   return(dbl_res);
  }
//交易所空头保证金率
double CSymbolInfo::ExchangeShortMarginRatioByMoney(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_ExchangeShortMarginRatioByMoney);
     }
   return(dbl_res);
  }
//交易所空头保证金费
double CSymbolInfo::ExchangeShortMarginRatioByVolume(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_ExchangeShortMarginRatioByVolume);
     }
   return(dbl_res);
  }
//开仓手续费率
double CSymbolInfo::OpenRatioByMoney(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_OpenRatioByMoney);
     }
   return(dbl_res);
  }
//开仓手续费
double CSymbolInfo::OpenRatioByVolume(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_OpenRatioByVolume);
     }
   return(dbl_res);
  }
//平仓手续费率
double CSymbolInfo::CloseRatioByMoney(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_CloseRatioByMoney);
     }
   return(dbl_res);
  }
//平仓手续费
double CSymbolInfo::CloseRatioByVolume(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_CloseRatioByVolume);
     }
   return(dbl_res);
  }
//平今仓手续费率
double CSymbolInfo::CloseTodayRatioByMoney(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_CloseTodayRatioByMoney);
     }
   return(dbl_res);
  }
//平今仓手续费
double CSymbolInfo::CloseTodayRatioByVolume(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_CloseTodayRatioByVolume);
     }
   return(dbl_res);
  }
//报单手续费
double CSymbolInfo::OrderCommByVolume(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_OrderCommByVolume);
     }
   return(dbl_res);
  }
//撤单手续费
double CSymbolInfo::OrderActionCommByVolume(void) const
  {
   double dbl_res = NULL;
   if(m_custom_ctp)
     {
      dbl_res = CTP::SymbolInfoDouble(m_symbol,CTP::SYMBOL_OrderActionCommByVolume);
     }
   return(dbl_res);
  }
//+------------------------------------------------------------------+
//| fast access methods to the integer symbol propertyes             |
//+------------------------------------------------------------------+
//产品类型
char CSymbolInfo::ProductClass(void) const
  {
   char int_res = NULL;
   if(m_custom_ctp)
     {
      int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_ProductClass);
     }
   return(int_res);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CSymbolInfo::ProductClassDescription(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      char int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_ProductClass);
      switch(int_res)
        {
         case '1':
            str_res = "THOST_FTDC_PC_Futures:期货";
            break;
         case '2':
            str_res = "THOST_FTDC_PC_Options:期货期权";
            break;
         case '3':
            str_res = "THOST_FTDC_PC_Combination:组合";
            break;
         case '4':
            str_res = "THOST_FTDC_PC_Spot:即期";
            break;
         case '5':
            str_res = "THOST_FTDC_PC_EFP:期转现";
            break;
         case '6':
            str_res = "THOST_FTDC_PC_SpotOption:现货期权";
            break;
         case '7':
            str_res = "THOST_FTDC_PC_TAS:TAS合约";
            break;
         case 'I':
            str_res = "THOST_FTDC_PC_MI:金属指数";
            break;
         default:
            str_res = "Unknown Product Class";
            break;
        }
     }
   return(str_res);
  }
//交割年份
long CSymbolInfo::DeliveryYear(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_DeliveryYear);
     }
   return(int_res);
  }
//交割月份
long CSymbolInfo::DeliveryMonth(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_DeliveryMonth);
     }
   return(int_res);
  }
//市价最大下单量
long CSymbolInfo::MaxMarketOrderVolume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_MaxMarketOrderVolume);
      if(int_res <= 0)
        {
         int_res = (long)::SymbolInfoDouble(m_symbol,SYMBOL_VOLUME_MAX);
        }
     }
   return(int_res);
  }
//市价最小下单量
long CSymbolInfo::MinMarketOrderVolume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_MinMarketOrderVolume);
      if(int_res <= 0)
        {
         int_res = (long)::SymbolInfoDouble(m_symbol,SYMBOL_VOLUME_MIN);
        }
     }
   return(int_res);
  }
//限价最大下单量
long CSymbolInfo::MaxLimitOrderVolume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_MaxLimitOrderVolume);
      if(int_res <= 0)
        {
         int_res = (long)::SymbolInfoDouble(m_symbol,SYMBOL_VOLUME_MAX);
        }
     }
   return(int_res);
  }
//限价最小下单量
long CSymbolInfo::MinLimitOrderVolume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_MinLimitOrderVolume);
      if(int_res <= 0)
        {
         int_res = (long)::SymbolInfoDouble(m_symbol,SYMBOL_VOLUME_MIN);
        }
     }
   return(int_res);
  }
//合约数量/乘数
long CSymbolInfo::ContractSize(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_VolumeMultiple);
      if(int_res <= 0)
        {
         int_res = (long)::SymbolInfoDouble(m_symbol,SYMBOL_TRADE_CONTRACT_SIZE);
        }
     }
   return(int_res);
  }
//小数点位数
long CSymbolInfo::Digits(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_Digit);
      if(int_res <= 0)
        {
         int_res = ::SymbolInfoInteger(m_symbol,SYMBOL_DIGITS);
        }
     }
   return(int_res);
  }
//合约生命周期状态
char CSymbolInfo::InstLifePhase(void) const
  {
   char int_res = NULL;
   if(m_custom_ctp)
     {
      int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_InstLifePhase);
     }
   return(int_res);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CSymbolInfo::InstLifePhaseDescription(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      char int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_InstLifePhase);
      switch(int_res)
        {
         case '0':
            str_res = "THOST_FTDC_IP_NotStart:未上市";
            break;
         case '1':
            str_res = "THOST_FTDC_IP_Started:上市";
            break;
         case '2':
            str_res = "THOST_FTDC_IP_Pause:停牌";
            break;
         case '3':
            str_res = "THOST_FTDC_IP_Expired:到期";
            break;
         default:
            str_res = "Unknown Life Phase";
            break;
        }
     }
   return(str_res);
  }
//当前是否交易
bool CSymbolInfo::IsTrading(void) const
  {
   bool int_res = false;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_IsTrading)>0;
     }
   return(int_res);
  }
//持仓类型
char CSymbolInfo::PositionType(void) const
  {
   char int_res = NULL;
   if(m_custom_ctp)
     {
      int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_PositionType);
     }
   return(int_res);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CSymbolInfo::PositionTypeDescription(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      char int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_PositionType);
      switch(int_res)
        {
         case '1':
            str_res = "THOST_FTDC_PT_Net:净持仓";
            break;
         case '2':
            str_res = "THOST_FTDC_PT_Gross:综合持仓";
            break;
         default:
            str_res = "Unknown Position Type";
            break;
        }
     }
   return(str_res);
  }
//持仓日期类型
char CSymbolInfo::PositionDateType(void) const
  {
   char int_res = NULL;
   if(m_custom_ctp)
     {
      int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_PositionDateType);
     }
   return(int_res);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CSymbolInfo::PositionDateTypeDescription(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      char int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_PositionDateType);
      switch(int_res)
        {
         case '1':
            str_res = "THOST_FTDC_PDT_UseHistory:使用历史持仓";
            break;
         case '2':
            str_res = "THOST_FTDC_PDT_NoUseHistory:不使用历史持仓";
            break;
         default:
            str_res = "Unknown Position Date Type";
            break;
        }
     }
   return(str_res);
  }
//平仓处理类型
char CSymbolInfo::CloseDealType(void) const
  {
   char int_res = NULL;
   if(m_custom_ctp)
     {
      int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_CloseDealType);
     }
   return(int_res);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CSymbolInfo::CloseDealTypeDescription(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      char int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_CloseDealType);
      switch(int_res)
        {
         case '0':
            str_res = "THOST_FTDC_CDT_Normal:正常";
            break;
         case '1':
            str_res = "THOST_FTDC_CDT_SpecFirst:投机平仓优先";
            break;
         default:
            str_res = "Unknown Close Deal Type";
            break;
        }
     }
   return(str_res);
  }
//质押资金可用范围
char CSymbolInfo::MortgageFundUseRange(void) const
  {
   char int_res = NULL;
   if(m_custom_ctp)
     {
      int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_MortgageFundUseRange);
     }
   return(int_res);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CSymbolInfo::MortgageFundUseRangeDescription(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      char int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_MortgageFundUseRange);
      switch(int_res)
        {
         case '0':
            str_res = "THOST_FTDC_MFUR_None:不能使用";
            break;
         case '1':
            str_res = "THOST_FTDC_MFUR_Margin:用于保证金";
            break;
         case '2':
            str_res = "THOST_FTDC_MFUR_All:用于手续费、盈亏、保证金";
            break;
         case '3':
            str_res = "THOST_FTDC_MFUR_CNY3:人民币方案3";
            break;
         default:
            str_res = "Unknown Mortgage Fund UseRange";
            break;
        }
     }
   return(str_res);
  }
//是否使用大额单边保证金算法
char CSymbolInfo::MaxMarginSideAlgorithm(void) const
  {
   char int_res = NULL;
   if(m_custom_ctp)
     {
      int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_MaxMarginSideAlgorithm);
     }
   return(int_res);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CSymbolInfo::MaxMarginSideAlgorithmDescription(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      char int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_MaxMarginSideAlgorithm);
      switch(int_res)
        {
         case '0':
            str_res = "THOST_FTDC_MMSA_NO:不使用大额单边保证金算法";
            break;
         case '1':
            str_res = "THOST_FTDC_MMSA_YES:使用大额单边保证金算法";
            break;
         default:
            str_res = "Unknown Max Margin Side Algorithm";
            break;
        }
     }
   return(str_res);
  }
//期权类型
char CSymbolInfo::OptionsType(void) const
  {
   char int_res = NULL;
   if(m_custom_ctp)
     {
      int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_OptionsType);
     }
   return(int_res);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CSymbolInfo::OptionsTypeDescription(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      char int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_OptionsType);
      switch(int_res)
        {
         case '1':
            str_res = "THOST_FTDC_CP_CallOptions:看涨";
            break;
         case '2':
            str_res = "THOST_FTDC_CP_PutOptions:看跌";
            break;
         default:
            str_res = "Unknown Options Type";
            break;
        }
     }
   return(str_res);
  }
//组合类型
char CSymbolInfo::CombinationType(void) const
  {
   char int_res = NULL;
   if(m_custom_ctp)
     {
      int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_CombinationType);
     }
   return(int_res);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CSymbolInfo::CombinationTypeDescription(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      char int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_CombinationType);
      switch(int_res)
        {
         case '0':
            str_res = "THOST_FTDC_COMBT_Future:期货组合";
            break;
         case '1':
            str_res = "THOST_FTDC_COMBT_BUL:垂直价差BUL";
            break;
         case '2':
            str_res = "THOST_FTDC_COMBT_BER:垂直价差BER";
            break;
         case '3':
            str_res = "THOST_FTDC_COMBT_STD:跨式组合";
            break;
         case '4':
            str_res = "THOST_FTDC_COMBT_STG:宽跨式组合";
            break;
         case '5':
            str_res = "THOST_FTDC_COMBT_PRT:备兑组合";
            break;
         case '6':
            str_res = "THOST_FTDC_COMBT_CLD:时间价差组合";
            break;
         default:
            str_res = "Unknown Combination Type";
            break;
        }
     }
   return(str_res);
  }
//是否指数合约
bool CSymbolInfo::IsIndex(void) const
  {
   bool int_res = false;
   if(m_custom_ctp)
     {
      int_res = m_symbol == ProductID()+INDEX_MAIN || m_symbol == ProductID()+INDEX_VALUE;
     }
   return(int_res);
  }
//是否主力合约
bool CSymbolInfo::IsMain(void) const
  {
   bool int_res = false;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_IsMain)>0;
     }
   return(int_res);
  }
//是否已订阅行情
bool CSymbolInfo::IsSubMarket(void) const
  {
   bool int_res = false;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_SubMarket)>0;
     }
   return(int_res);
  }
//是否交易时间
bool CSymbolInfo::SymbolExists(void) const
  {
   bool int_res = false;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolExists(m_symbol);
     }
   return(int_res);
  }
//合约交易状态
char CSymbolInfo::Status(void) const
  {
   char int_res = NULL;
   if(m_custom_ctp)
     {
      int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_InstrumentStatus);
     }
   return(int_res);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CSymbolInfo::StatusDescription(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      char int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_InstrumentStatus);
      switch(int_res)
        {
         case '0':
            str_res = "THOST_FTDC_IS_BeforeTrading:开盘前";
            break;
         case '1':
            str_res = "THOST_FTDC_IS_NoTrading:非交易";
            break;
         case '2':
            str_res = "THOST_FTDC_IS_Continous:连续交易";
            break;
         case '3':
            str_res = "THOST_FTDC_IS_AuctionOrdering:集合竞价报单";
            break;
         case '4':
            str_res = "THOST_FTDC_IS_AuctionBalance:集合竞价价格平衡";
            break;
         case '5':
            str_res = "THOST_FTDC_IS_AuctionMatch:集合竞价撮合";
            break;
         case '6':
            str_res = "THOST_FTDC_IS_Closed:收盘";
            break;
         default:
            str_res = "Unknown Instrument Status";
            break;
        }
     }
   return(str_res);
  }
//进入本状态原因
char CSymbolInfo::EnterReason(void) const
  {
   char int_res = NULL;
   if(m_custom_ctp)
     {
      int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_EnterReason);
     }
   return(int_res);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CSymbolInfo::EnterReasonDescription(void) const
  {
   string str_res = NULL;
   if(m_custom_ctp)
     {
      char int_res = (char)CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_EnterReason);
      switch(int_res)
        {
         case '1':
            str_res = "THOST_FTDC_IER_Automatic:自动切换";
            break;
         case '2':
            str_res = "THOST_FTDC_IER_Manual:手动切换";
            break;
         case '3':
            str_res = "THOST_FTDC_IER_Fuse:熔断";
            break;
         default:
            str_res = "Unknown Status Enter Reason";
            break;
        }
     }
   return(str_res);
  }
//成交数量（行情）
long CSymbolInfo::Volume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_Volume);
      if(int_res <= 0)
        {
         int_res = (long)::SymbolInfoDouble(m_symbol,SYMBOL_SESSION_VOLUME);
        }
     }
   return(int_res);
  }
//最后修改毫秒（行情）
long CSymbolInfo::UpdateMillisec(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_UpdateMillisec);
     }
   return(int_res);
  }
//申买量一（行情）
long CSymbolInfo::BidVolume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_BidVolume1);
     }
   return(int_res);
  }
//申卖量一（行情）
long CSymbolInfo::AskVolume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_AskVolume1);
     }
   return(int_res);
  }
//申买量二（行情）
long CSymbolInfo::Bid2Volume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_BidVolume2);
     }
   return(int_res);
  }
//申卖量二（行情）
long CSymbolInfo::Ask2Volume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_AskVolume2);
     }
   return(int_res);
  }
//申买量三（行情）
long CSymbolInfo::Bid3Volume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_BidVolume3);
     }
   return(int_res);
  }
//申卖量三（行情）
long CSymbolInfo::Ask3Volume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_AskVolume3);
     }
   return(int_res);
  }
//申买量四（行情）
long CSymbolInfo::Bid4Volume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_BidVolume4);
     }
   return(int_res);
  }
//申卖量四（行情）
long CSymbolInfo::Ask4Volume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_AskVolume4);
     }
   return(int_res);
  }
//申买量五（行情）
long CSymbolInfo::Bid5Volume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_BidVolume5);
     }
   return(int_res);
  }
//申卖量五（行情）
long CSymbolInfo::Ask5Volume(void) const
  {
   long int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SymbolInfoInteger(m_symbol,CTP::SYMBOL_AskVolume5);
     }
   return(int_res);
  }
//交易时段数
int CSymbolInfo::SessionsTotal(void) const
  {
   int int_res = WRONG_VALUE;
   if(m_custom_ctp)
     {
      int_res = CTP::SessionsTotal(m_symbol);
     }
   return(int_res);
  }
//交易时段开始时间/本地时间
datetime CSymbolInfo::SessionStartTime(int index) const
  {
   datetime int_res = NULL;
   if(m_custom_ctp)
     {
      int_res = CTP::SessionStartTime(m_symbol,index);
     }
   return(int_res);
  }
//交易时段结束时间/本地时间
datetime CSymbolInfo::SessionEndTime(int index) const
  {
   datetime int_res = NULL;
   if(m_custom_ctp)
     {
      int_res = CTP::SessionEndTime(m_symbol,index);
     }
   return(int_res);
  }
//+------------------------------------------------------------------+
//| Access functions SymbolInfoInteger(...)                          |
//+------------------------------------------------------------------+
bool CSymbolInfo::InfoInteger(const CTP::ENUM_SYMBOL_INFO_INTEGER prop_id,long& var) const
  {
   if(m_custom_ctp)
     {
      return(mt5ctp::SymbolInfoInteger(m_symbol,prop_id,var));
     }
   return false;
  }
//+------------------------------------------------------------------+
//| Access functions SymbolInfoDouble(...)                           |
//+------------------------------------------------------------------+
bool CSymbolInfo::InfoDouble(const CTP::ENUM_SYMBOL_INFO_DOUBLE prop_id,double& var) const
  {
   if(m_custom_ctp)
     {
      return(mt5ctp::SymbolInfoDouble(m_symbol,prop_id,var));
     }
   return false;
  }
//+------------------------------------------------------------------+
//| Access functions SymbolInfoString(...)                           |
//+------------------------------------------------------------------+
bool CSymbolInfo::InfoString(const CTP::ENUM_SYMBOL_INFO_STRING prop_id,string& var) const
  {
   if(m_custom_ctp)
     {
      return(mt5ctp::SymbolInfoString(m_symbol,prop_id,var));
     }
   return false;
  }
//+------------------------------------------------------------------+
//| Normalize price                                                  |
//+------------------------------------------------------------------+
double CSymbolInfo::NormalizePrice(const double price) const
  {
   double tick_size = PriceTick();
   int digit = (int)Digits();
   if(tick_size>0)
     {
      return(::NormalizeDouble(::MathRound(price/tick_size)*tick_size,digit));
     }
   return(::NormalizeDouble(price,digit));
  }
//+------------------------------------------------------------------+
//| method for select working order                                  |
//+------------------------------------------------------------------+
bool CSymbolInfo::Select(const string this_symbol=NULL)
  {
   m_symbol = (this_symbol==NULL || ::StringLen(this_symbol)==0)?_Symbol:this_symbol;
   bool is_custom;
   if(::SymbolExist(m_symbol,is_custom))
     {
      if(is_custom && ::SymbolInfoString(m_symbol,SYMBOL_PAGE)=="CTP")
        {
         m_custom_ctp = true;
        }
     }
   return(m_custom_ctp);
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
