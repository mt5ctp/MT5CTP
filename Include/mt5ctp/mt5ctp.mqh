//+------------------------------------------------------------------+
//|                                                       mt5ctp.mqh |
//+------------------------------------------------------------------+
#property copyright     "Copyright 2020,Guorui Holding (Shandong) Co.,Ltd."
#property version       "1.50"
#property link          ""
#property description   "本程序仅用于量化投资爱好者测试、研究。程序使用者承诺："
#property description   "1.本人/本单位熟知本程序的适用范围，自愿承担因商业应用造成的违法、违规、侵权责任。"
#property description   "2.本人/本单位熟知金融衍生品的各种交易及代理风险，并自愿承担损失及违法、违规、违约责任。"
#property description   "3.本人/本单位熟知计算机程序的各种固有风险，并自愿承担损失及违法、违规、违约责任。"
#property description   "4.本人/本单位熟知金融监管规则，依法依规参与金融市场，并自愿承担损失及违法、违规责任。"
#property description   "5.本人/本单位认可开始使用本程序，视为同意并做出上述承诺，停用本程序后，上述承诺依然有效。"
//+------------------------------------------------------------------+
//| include 外部依赖                                                 |
//+------------------------------------------------------------------+
#include <mt5ctp\JAson.mqh>
#include <Files\FileTxt.mqh>
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// 连续主力指数代码
#define INDEX_MAIN                  ("888")
// 持仓量加权指数代码
#define INDEX_VALUE                 ("000")
// 交互信息缓冲区大小
#define MT5CTP_CHAR_BUFFER_SIZE     (4096)
// 交易时段配置文件
#define EXPERT_SESSION_FILE         "mt5ctp\\CustomSymbolSessions.json"
// CTP 订单事件
#define EXPERT_CTP_ORDER            (CHARTEVENT_CUSTOM_LAST-7)
// CTP 成交事件
#define EXPERT_CTP_TRADE            (CHARTEVENT_CUSTOM_LAST-8)
// CTP 错误事件
#define EXPERT_CTP_ERROR            (CHARTEVENT_CUSTOM_LAST-9)
// CTP 期权行权/自对冲操作ID(幻数)
#define OPTION_EXEC_EXECUTE         (9874123658)      // 行权执行
#define OPTION_EXEC_ABABDON         (9874123657)      // 行权放弃
#define OPTION_EXEC_SELF_OPTION     (9632147852)      // 自对冲期权仓位
#define OPTION_EXEC_SELF_FUTURE     (9632147851)      // 自对冲期货仓位
// 开仓/平仓/平今仓
#define ORDER_TYPE_ENTRY            long(0)           // 开仓
#define ORDER_TYPE_EXIT             long(-1)          // 平仓
#define ORDER_TYPE_EXITTODAY        long(-2)          // 平今仓

//交易请求结构 (CTPTradeRequest)
struct CTPTradeRequest
  {
   int                           action;           // 交易操作类型
   long                          magic;            // EA交易 ID (幻数)
   long                          order;            // 订单号
   char                          symbol[31];       // 交易的交易品种
   double                        volume;           // 一手需求的交易量
   double                        price;            // 价格
   double                        stoplimit;        // 订单止损限价点位
   double                        sl;               // 订单止损价位点位
   double                        tp;               // 订单盈利价位点位
   long                          deviation;        // 需求价格最可能的偏差
   int                           type;             // 订单类型
   int                           type_filling;     // 订单执行类型
   int                           type_time;        // 订单执行时间
   long                          expiration;       // 订单终止期 (为 ORDER_TIME_SPECIFIED 类型订单)
   char                          comment[81];      // 订单注释
   long                          position;         // 持仓编号
   long                          position_by;      // 反向持仓编号
  };
//交易要求检查结构结果（CTPTradeCheckResult）
struct CTPTradeCheckResult
  {
   int                           retcode;             // 应答码
   double                        balance;             // 交易执行后的余额
   double                        equity;              // 交易执行后的净值
   double                        profit;              // 浮动利润
   double                        margin;              // 保证金规定额
   double                        margin_free;         // 自由保证金
   double                        margin_level;        // 保证金比例
   char                          comment[81];         // 应答码注释 (描述错误)
  };
//交易请求结果结构 （CTPTradeResult）
struct CTPTradeResult
  {
   int                           retcode;          // 操作返回代码
   long                          deal;             // 交易订单号，如果完成的话
   long                          order;            // 订单号，如果下订单的话
   double                        volume;           // 交易交易量，经纪人确认的
   double                        price;            // 交易价格，经纪人确认的
   double                        bid;              // 当前买入价
   double                        ask;              // 当前卖出价
   char                          comment[81];      // 经纪人操作注释 (默认填充操作描述)
   int                           request_id;       // 分派期间通过程序端设置Request ID
   int                           retcode_external; // 返回外部交易系统代码
  };
//交易事务结构 (CTPTradeTransaction)
struct CTPTradeTransaction
  {
   long                          deal;             // 交易单
   long                          order;            // 订单标签
   char                          symbol[31];       // 交易品种
   int                           type;             // 交易事务类型
   int                           order_type;       // 订单类型
   int                           order_state;      // 订单状态
   int                           deal_type;        // 成交类型
   int                           time_type;        // 操作期的订单类型
   long                          time_expiration;  // 订单到期时间
   double                        price;            // 价格
   double                        price_trigger;    // 限价止损订单激活价格
   double                        price_sl;         // 止损水平
   double                        price_tp;         // 获利水平
   double                        volume;           // 交易量手数
   long                          position;         // 持仓价格
   long                          position_by;      // 反向持仓价格
  };
//持仓结构
struct CTPPositions
  {
   char                          InstrumentID[31];    //合约代码
   char                          BrokerID[11];        ///经纪公司代码
   char                          InvestorID[13];      ///投资者代码
   char                          PosiDirection;       ///持仓多空方向
   char                          HedgeFlag;           ///投机套保标志
   char                          PositionDate;        ///持仓日期
   int                           YdPosition;          ///上日持仓
   int                           Position;            ///今日持仓
   int                           LongFrozen;          ///多头冻结
   int                           ShortFrozen;         ///空头冻结
   double                        LongFrozenAmount;    ///开仓冻结金额
   double                        ShortFrozenAmount;   ///开仓冻结金额
   int                           OpenVolume;          ///开仓量
   int                           CloseVolume;         ///平仓量
   double                        OpenAmount;          ///开仓金额
   double                        CloseAmount;         ///平仓金额
   double                        PositionCost;        ///持仓成本
   double                        PreMargin;           ///上次占用的保证金
   double                        UseMargin;           ///占用的保证金
   double                        FrozenMargin;        ///冻结的保证金
   double                        FrozenCash;          ///冻结的资金
   double                        FrozenCommission;    ///冻结的手续费
   double                        CashIn;              ///资金差额
   double                        Commission;          ///手续费
   double                        CloseProfit;         ///平仓盈亏
   double                        PositionProfit;      ///持仓盈亏
   double                        PreSettlementPrice;  ///上次结算价
   double                        SettlementPrice;     ///本次结算价
   char                          TradingDay[9];       ///交易日
   int                           SettlementID;        ///结算编号
   double                        OpenCost;            ///开仓成本
   double                        OpenProfit;          ///开仓浮动盈亏
   double                        ExchangeMargin;      ///交易所保证金
   int                           CombPosition;        ///组合成交形成的持仓
   int                           CombLongFrozen;      ///组合多头冻结
   int                           CombShortFrozen;     ///组合空头冻结
   double                        CloseProfitByDate;   ///逐日盯市平仓盈亏
   double                        CloseProfitByTrade;  ///逐笔对冲平仓盈亏
   int                           TodayPosition;       ///今日持仓
   double                        MarginRateByMoney;   ///保证金率
   double                        MarginRateByVolume;  ///保证金率(按手数)
   int                           StrikeFrozen;        ///执行冻结
   double                        StrikeFrozenAmount;  ///执行冻结金额
   int                           AbandonFrozen;       ///放弃执行冻结
   char                          ExchangeID[9];       ///交易所代码
   int                           YdStrikeFrozen;      ///执行冻结的昨仓
   char                          InvestUnitID[17];    ///投资单元代码
   double                        PositionCostOffset;  ///大商所持仓成本差值，只有大商所使用
   double                        StopLoss;            ///止损价
   double                        TakeProfit;          ///止赢价
   int                           TasPosition;         ///tas持仓手数
   double                        TasPositionCost;     ///tas持仓成本
  };
//工作报单结构
struct CTPOrders
  {
   char                          BrokerID[11];        ///经纪公司代码
   char                          InvestorID[13];      ///投资者代码
   char                          InstrumentID[31];    ///合约代码
   char                          OrderRef[13];        ///报单引用
   char                          UserID[16];          ///用户代码
   char                          OrderPriceType;      ///报单价格条件
   char                          Direction;           ///买卖方向
   char                          CombOffsetFlag[5];   ///组合开平标志
   char                          CombHedgeFlag[5];    ///组合投机套保标志
   double                        LimitPrice;          ///价格
   int                           VolumeTotalOriginal; ///数量
   char                          TimeCondition;       ///有效期类型
   char                          GTDDate[9];          ///GTD日期
   char                          VolumeCondition;     ///成交量类型
   int                           MinVolume;           ///最小成交量
   char                          ContingentCondition; ///触发条件
   double                        StopPrice;           ///止损价
   char                          ForceCloseReason;    ///强平原因
   int                           IsAutoSuspend;       ///自动挂起标志
   char                          BusinessUnit[21];    ///业务单元
   int                           RequestID;           ///请求编号
   char                          OrderLocalID[13];    ///本地报单编号
   char                          ExchangeID[9];       ///交易所代码
   char                          ParticipantID[11];   ///会员代码
   char                          ClientID[11];        ///客户代码
   char                          ExchangeInstID[31];  ///合约在交易所的代码
   char                          TraderID[21];        ///交易所交易员代码
   int                           InstallID;           ///安装编号
   char                          OrderSubmitStatus;   ///报单提交状态
   int                           NotifySequence;      ///报单提示序号
   char                          TradingDay[9];       ///交易日
   int                           SettlementID;        ///结算编号
   char                          OrderSysID[21];      ///报单编号
   char                          OrderSource;         ///报单来源
   char                          OrderStatus;         ///报单状态
   char                          OrderType;           ///报单类型
   int                           VolumeTraded;        ///今成交数量
   int                           VolumeTotal;         ///剩余数量
   char                          InsertDate[9];       ///报单日期
   char                          InsertTime[9];       ///委托时间
   char                          ActiveTime[9];       ///激活时间
   char                          SuspendTime[9];      ///挂起时间
   char                          UpdateTime[9];       ///最后修改时间
   char                          CancelTime[9];       ///撤销时间
   char                          ActiveTraderID[21];  ///最后修改交易所交易员代码
   char                          ClearingPartID[11];  ///结算会员编号
   int                           SequenceNo;          ///序号
   int                           FrontID;             ///前置编号
   int                           SessionID;           ///会话编号
   char                          UserProductInfo[11]; ///用户端产品信息
   char                          StatusMsg[81];       ///状态信息
   int                           UserForceClose;      ///用户强评标志
   char                          ActiveUserID[16];    ///操作用户代码
   int                           BrokerOrderSeq;      ///经纪公司报单编号
   char                          RelativeOrderSysID[21];///相关报单
   int                           ZCETotalTradedVolume;///郑商所成交数量
   int                           IsSwapOrder;         ///互换单标志
   char                          BranchID[9];         ///营业部编号
   char                          InvestUnitID[17];    ///投资单元代码
   char                          AccountID[13];       ///资金账号
   char                          CurrencyID[4];       ///币种代码
   char                          IPAddress[16];       ///IP地址
   char                          MacAddress[21];      ///Mac地址
   double                        StopLoss;            ///止损价
   double                        TakeProfit;          ///止赢价
  };
//历史报单结构
struct CTPHistoryOrders
  {
   char                          BrokerID[11];        ///经纪公司代码
   char                          InvestorID[13];      ///投资者代码
   char                          InstrumentID[31];    ///合约代码
   char                          OrderRef[13];        ///报单引用
   char                          UserID[16];          ///用户代码
   char                          OrderPriceType;      ///报单价格条件
   char                          Direction;           ///买卖方向
   char                          CombOffsetFlag[5];   ///组合开平标志
   char                          CombHedgeFlag[5];    ///组合投机套保标志
   double                        LimitPrice;          ///价格
   int                           VolumeTotalOriginal; ///数量
   char                          TimeCondition;       ///有效期类型
   char                          GTDDate[9];          ///GTD日期
   char                          VolumeCondition;     ///成交量类型
   int                           MinVolume;           ///最小成交量
   char                          ContingentCondition; ///触发条件
   double                        StopPrice;           ///止损价
   char                          ForceCloseReason;    ///强平原因
   int                           IsAutoSuspend;       ///自动挂起标志
   char                          BusinessUnit[21];    ///业务单元
   int                           RequestID;           ///请求编号
   char                          OrderLocalID[13];    ///本地报单编号
   char                          ExchangeID[9];       ///交易所代码
   char                          ParticipantID[11];   ///会员代码
   char                          ClientID[11];        ///客户代码
   char                          ExchangeInstID[31];  ///合约在交易所的代码
   char                          TraderID[21];        ///交易所交易员代码
   int                           InstallID;           ///安装编号
   char                          OrderSubmitStatus;   ///报单提交状态
   int                           NotifySequence;      ///报单提示序号
   char                          TradingDay[9];       ///交易日
   int                           SettlementID;        ///结算编号
   char                          OrderSysID[21];      ///报单编号
   char                          OrderSource;         ///报单来源
   char                          OrderStatus;         ///报单状态
   char                          OrderType;           ///报单类型
   int                           VolumeTraded;        ///今成交数量
   int                           VolumeTotal;         ///剩余数量
   char                          InsertDate[9];       ///报单日期
   char                          InsertTime[9];       ///委托时间
   char                          ActiveTime[9];       ///激活时间
   char                          SuspendTime[9];      ///挂起时间
   char                          UpdateTime[9];       ///最后修改时间
   char                          CancelTime[9];       ///撤销时间
   char                          ActiveTraderID[21];  ///最后修改交易所交易员代码
   char                          ClearingPartID[11];  ///结算会员编号
   int                           SequenceNo;          ///序号
   int                           FrontID;             ///前置编号
   int                           SessionID;           ///会话编号
   char                          UserProductInfo[11]; ///用户端产品信息
   char                          StatusMsg[81];       ///状态信息
   int                           UserForceClose;      ///用户强评标志
   char                          ActiveUserID[16];    ///操作用户代码
   int                           BrokerOrderSeq;      ///经纪公司报单编号
   char                          RelativeOrderSysID[21];///相关报单
   int                           ZCETotalTradedVolume;///郑商所成交数量
   int                           IsSwapOrder;         ///互换单标志
   char                          BranchID[9];         ///营业部编号
   char                          InvestUnitID[17];    ///投资单元代码
   char                          AccountID[13];       ///资金账号
   char                          CurrencyID[4];       ///币种代码
   char                          IPAddress[16];       ///IP地址
   char                          MacAddress[21];      ///Mac地址
   double                        StopLoss;            ///止损价
   double                        TakeProfit;          ///止赢价
   int                           ErrorID;             ///错误代码
   char                          ErrorMsg[81];        ///错误信息
  };
//历史成交结构
struct CTPHistoryDeals
  {
   char                          BrokerID[11];        ///经纪公司代码
   char                          InvestorID[13];      ///投资者代码
   char                          InstrumentID[31];    ///合约代码
   char                          OrderRef[13];        ///报单引用
   char                          UserID[16];          ///用户代码
   char                          ExchangeID[9];       ///交易所代码
   char                          TradeID[21];         ///成交编号
   char                          Direction;           ///买卖方向
   char                          OrderSysID[21];      ///报单编号
   char                          ParticipantID[11];   ///会员代码
   char                          ClientID[11];        ///客户代码
   char                          TradingRole;         ///交易角色
   char                          ExchangeInstID[31];  ///合约在交易所的代码
   char                          OffsetFlag;          ///开平标志
   char                          HedgeFlag;           ///投机套保标志
   double                        Price;               ///价格
   int                           Volume;              ///数量
   char                          TradeDate[9];        ///成交时期
   char                          TradeTime[9];        ///成交时间
   char                          TradeType;           ///成交类型
   char                          PriceSource;         ///成交价来源
   char                          TraderID[21];        ///交易所交易员代码
   char                          OrderLocalID[13];    ///本地报单编号
   char                          ClearingPartID[11];  ///结算会员编号
   char                          BusinessUnit[21];    ///业务单元
   int                           SequenceNo;          ///序号
   char                          TradingDay[9];       ///交易日
   int                           SettlementID;        ///结算编号
   int                           BrokerOrderSeq;      ///经纪公司报单编号
   char                          TradeSource;         ///成交来源
   char                          InvestUnitID[17];    ///投资单元代码
  };
//mt5ctp成交/持仓结构
struct MT5CTPOrders
  {
   char                          symbol[31];           // 交易品种
   ulong                         ticket;               // 订单号
   ulong                         date;                 // 交易日
   ulong                         time;                 // 报单/成交时间
   int                           type;                 // 报单/持仓类型
   double                        volume;               // 数量
   double                        price;                // 报单/持仓价格
   double                        sl;                   // 止损
   double                        tp;                   // 止盈
   double                        profit;               // 盈亏
   long                          magic;                // 幻数
   char                          comment[81];          // 操作注释
   ulong                         position;             // 持仓id
   char                          ordersysid[21];       // 交易所报单编号
  };

//+------------------------------------------------------------------+
//| namespace ctp                                                    |
//+------------------------------------------------------------------+
namespace CTP
{
//+------------------------------------------------------------------+
//选中数据结构体
//持仓数据
CTPPositions CTP_Position = {0};
//工作报单
CTPOrders CTP_Order = {0};
//历史报单
CTPHistoryOrders CTP_HistoryOrder = {0};
//历史成交
CTPHistoryDeals CTP_HistoryDeal = {0};
//+------------------------------------------------------------------+
// 交易所信息
enum ENUM_EXCHANGE
  {
   EXCHANGE_SHFE = 1,
   EXCHANGE_CZCE,
   EXCHANGE_DCE,
   EXCHANGE_CFFEX,
   EXCHANGE_INE,
  };
// 合约信息 INTEGER
enum ENUM_SYMBOL_INFO_INTEGER
  {
///产品类型
   SYMBOL_ProductClass = 0,
///交割年份
   SYMBOL_DeliveryYear,
///交割月
   SYMBOL_DeliveryMonth,
///市价单最大下单量
   SYMBOL_MaxMarketOrderVolume,
///市价单最小下单量
   SYMBOL_MinMarketOrderVolume,
///限价单最大下单量
   SYMBOL_MaxLimitOrderVolume,
///限价单最小下单量
   SYMBOL_MinLimitOrderVolume,
///合约数量乘数
   SYMBOL_VolumeMultiple,
///合约小数点位数
   SYMBOL_Digit,
///合约是否已订阅行情
   SYMBOL_SubMarket,
///合约生命周期状态
   SYMBOL_InstLifePhase,
///当前是否交易
   SYMBOL_IsTrading,
///是否主力合约
   SYMBOL_IsMain,
///持仓类型
   SYMBOL_PositionType,
///持仓日期类型
   SYMBOL_PositionDateType,
///平仓处理类型
   SYMBOL_CloseDealType,
///质押资金可用范围
   SYMBOL_MortgageFundUseRange,
///是否使用大额单边保证金算法
   SYMBOL_MaxMarginSideAlgorithm,
///期权类型
   SYMBOL_OptionsType,
///组合类型
   SYMBOL_CombinationType,
///合约交易状态
   SYMBOL_InstrumentStatus,
///交易阶段编号
   SYMBOL_TradingSegmentSN,
///进入本状态原因
   SYMBOL_EnterReason,
///数量
   SYMBOL_Volume,
///最后修改毫秒
   SYMBOL_UpdateMillisec,
///申买量一
   SYMBOL_BidVolume1,
///申卖量一
   SYMBOL_AskVolume1,
///申买量二
   SYMBOL_BidVolume2,
///申卖量二
   SYMBOL_AskVolume2,
///申买量三
   SYMBOL_BidVolume3,
///申卖量三
   SYMBOL_AskVolume3,
///申买量四
   SYMBOL_BidVolume4,
///申卖量四
   SYMBOL_AskVolume4,
///申买量五
   SYMBOL_BidVolume5,
///申卖量五
   SYMBOL_AskVolume5,
  };
// 合约信息 DOUBLE
enum ENUM_SYMBOL_INFO_DOUBLE
  {
///最小变动价位
   SYMBOL_PriceTick = 0,
///合约基础商品乘数
   SYMBOL_UnderlyingMultiple,
///多头保证金率
   SYMBOL_LongMarginRatio,
///空头保证金率
   SYMBOL_ShortMarginRatio,
///执行价
   SYMBOL_StrikePrice,
///最新价
   SYMBOL_LastPrice,
///上次结算价
   SYMBOL_PreSettlementPrice,
///昨收盘
   SYMBOL_PreClosePrice,
///今开盘
   SYMBOL_OpenPrice,
///最高价
   SYMBOL_HighestPrice,
///最低价
   SYMBOL_LowestPrice,
///今收盘
   SYMBOL_ClosePrice,
///本次结算价
   SYMBOL_SettlementPrice,
///涨停板价
   SYMBOL_UpperLimitPrice,
///跌停板价
   SYMBOL_LowerLimitPrice,
///申买价一
   SYMBOL_BidPrice1,
///申卖价一
   SYMBOL_AskPrice1,
///申买价二
   SYMBOL_BidPrice2,
///申卖价二
   SYMBOL_AskPrice2,
///申买价三
   SYMBOL_BidPrice3,
///申卖价三
   SYMBOL_AskPrice3,
///申买价四
   SYMBOL_BidPrice4,
///申卖价四
   SYMBOL_AskPrice4,
///申买价五
   SYMBOL_BidPrice5,
///申卖价五
   SYMBOL_AskPrice5,
///当日均价
   SYMBOL_AveragePrice,
///成交金额
   SYMBOL_Turnover,
///昨持仓量
   SYMBOL_PreOpenInterest,
///持仓量
   SYMBOL_OpenInterest,
///昨虚实度
   SYMBOL_PreDelta,
///今虚实度
   SYMBOL_CurrDelta,
///多头保证金率
   SYMBOL_LongMarginRatioByMoney,
///多头保证金费
   SYMBOL_LongMarginRatioByVolume,
///空头保证金率
   SYMBOL_ShortMarginRatioByMoney,
///空头保证金费
   SYMBOL_ShortMarginRatioByVolume,
///交易所多头保证金率
   SYMBOL_ExchangeLongMarginRatioByMoney,
///交易所多头保证金费
   SYMBOL_ExchangeLongMarginRatioByVolume,
///交易所空头保证金率
   SYMBOL_ExchangeShortMarginRatioByMoney,
///交易所空头保证金费
   SYMBOL_ExchangeShortMarginRatioByVolume,
///开仓手续费率
   SYMBOL_OpenRatioByMoney,
///开仓手续费
   SYMBOL_OpenRatioByVolume,
///平仓手续费率
   SYMBOL_CloseRatioByMoney,
///平仓手续费
   SYMBOL_CloseRatioByVolume,
///平今手续费率
   SYMBOL_CloseTodayRatioByMoney,
///平今手续费
   SYMBOL_CloseTodayRatioByVolume,
///报单手续费
   SYMBOL_OrderCommByVolume,
///撤单手续费
   SYMBOL_OrderActionCommByVolume,
///执行手续费率
   SYMBOL_StrikeRatioByMoney,
///执行手续费
   SYMBOL_StrikeRatioByVolume,
///期权合约保证金不变部分
   SYMBOL_FixedMargin,
///期权合约最小保证金
   SYMBOL_MiniMargin,
///期权合约权利金
   SYMBOL_Royalty,
///交易所期权合约保证金不变部分
   SYMBOL_ExchFixedMargin,
///交易所期权合约最小保证金
   SYMBOL_ExchMiniMargin,
  };
// 合约信息 STRING
enum ENUM_SYMBOL_INFO_STRING
  {
///交易所代码
   SYMBOL_ExchangeID = 0,
///合约名称
   SYMBOL_InstrumentName,
///合约在交易所的代码
   SYMBOL_ExchangeInstID,
///产品代码
   SYMBOL_ProductID,
///产品名称
   SYMBOL_ProductName,
///交易所产品代码
   SYMBOL_ExchangeProductID,
///基础商品代码
   SYMBOL_UnderlyingInstrID,
///交易币种类型
   SYMBOL_TradeCurrencyID,
///创建日
   SYMBOL_CreateDate,
///上市日
   SYMBOL_OpenDate,
///到期日
   SYMBOL_ExpireDate,
///开始交割日
   SYMBOL_StartDelivDate,
///结束交割日
   SYMBOL_EndDelivDate,
///进入本状态时间
   SYMBOL_EnterTime,
///交易日
   SYMBOL_TradingDay,
///业务日期
   SYMBOL_ActionDay,
///最后修改时间
   SYMBOL_UpdateTime,
  };
//+------------------------------------------------------------------+
/// 账户信息 INTEGER
enum ENUM_ACCOUNT_INFO_INTEGER
  {
///前置编号
   ACCOUNT_FrontID = 0,
///会话编号
   ACCOUNT_SessionID,
///结算编号
   ACCOUNT_SettlementID,
///业务类型
   ACCOUNT_BizType,
///用户状态
   ACCOUNT_UserStatus,
///保证金价格类型
   ACCOUNT_MarginPriceType,
///盈亏算法
   ACCOUNT_Algorithm,
///可用是否包含平仓盈利
   ACCOUNT_AvailIncludeCloseProfit,
///期权权利金价格类型
   ACCOUNT_OptionRoyaltyPriceType,
  };
/// 账户信息 DOUBLE
enum ENUM_ACCOUNT_INFO_DOUBLE
  {
///上次质押金额
   ACCOUNT_PreMortgage = 0,
///上次信用额度
   ACCOUNT_PreCredit,
///上次存款额
   ACCOUNT_PreDeposit,
///上次结算准备金
   ACCOUNT_PreBalance,
///上次占用的保证金
   ACCOUNT_PreMargin,
///利息基数
   ACCOUNT_InterestBase,
///利息收入
   ACCOUNT_Interest,
///入金金额
   ACCOUNT_Deposit,
///出金金额
   ACCOUNT_Withdraw,
///冻结的保证金
   ACCOUNT_FrozenMargin,
///冻结的资金
   ACCOUNT_FrozenCash,
///冻结的手续费
   ACCOUNT_FrozenCommission,
///当前保证金总额
   ACCOUNT_CurrMargin,
///资金差额
   ACCOUNT_CashIn,
///手续费
   ACCOUNT_Commission,
///平仓盈亏
   ACCOUNT_CloseProfit,
///持仓盈亏
   ACCOUNT_PositionProfit,
///期货结算准备金
   ACCOUNT_Balance,
///可用资金
   ACCOUNT_Available,
///可取资金
   ACCOUNT_WithdrawQuota,
///基本准备金
   ACCOUNT_Reserve,
///信用额度
   ACCOUNT_Credit,
///质押金额
   ACCOUNT_Mortgage,
///交易所保证金
   ACCOUNT_ExchangeMargin,
///投资者交割保证金
   ACCOUNT_DeliveryMargin,
///交易所交割保证金
   ACCOUNT_ExchangeDeliveryMargin,
///保底期货结算准备金
   ACCOUNT_ReserveBalance,
///上次货币质入金额
   ACCOUNT_PreFundMortgageIn,
///上次货币质出金额
   ACCOUNT_PreFundMortgageOut,
///货币质入金额
   ACCOUNT_FundMortgageIn,
///货币质出金额
   ACCOUNT_FundMortgageOut,
///货币质押余额
   ACCOUNT_FundMortgageAvailable,
///可质押货币金额
   ACCOUNT_MortgageableFund,
///特殊产品占用保证金
   ACCOUNT_SpecProductMargin,
///特殊产品冻结保证金
   ACCOUNT_SpecProductFrozenMargin,
///特殊产品手续费
   ACCOUNT_SpecProductCommission,
///特殊产品冻结手续费
   ACCOUNT_SpecProductFrozenCommission,
///特殊产品持仓盈亏
   ACCOUNT_SpecProductPositionProfit,
///特殊产品平仓盈亏
   ACCOUNT_SpecProductCloseProfit,
///根据持仓盈亏算法计算的特殊产品持仓盈亏
   ACCOUNT_SpecProductPositionProfitByAlg,
///特殊产品交易所保证金
   ACCOUNT_SpecProductExchangeMargin,
///延时换汇冻结金额
   ACCOUNT_FrozenSwap,
///剩余换汇额度
   ACCOUNT_RemainSwap,
  };
/// 账户信息 STRING
enum ENUM_ACCOUNT_INFO_STRING
  {
///交易日
   ACCOUNT_TradingDay = 0,
///登录成功时间
   ACCOUNT_LoginTime,
///经纪公司代码
   ACCOUNT_BrokerID,
///用户代码
   ACCOUNT_UserID,
///投资者帐号
   ACCOUNT_AccountID,
///交易系统名称
   ACCOUNT_SystemName,
///最大报单引用
   ACCOUNT_MaxOrderRef,
///币种代码
   ACCOUNT_CurrencyID,
  };
//+------------------------------------------------------------------+
/// 持仓信息 INTEGER
enum ENUM_POSITION_PROPERTY_INTEGER
  {
///持仓多空方向
   POSITION_PosiDirection = 0,
///投机套保标志
   POSITION_HedgeFlag,
///持仓日期
   POSITION_PositionDate,
///上日持仓
   POSITION_YdPosition,
///今日持仓
   POSITION_Position,
///多头冻结
   POSITION_LongFrozen,
///空头冻结
   POSITION_ShortFrozen,
///开仓量
   POSITION_OpenVolume,
///平仓量
   POSITION_CloseVolume,
///结算编号
   POSITION_SettlementID,
///组合成交形成的持仓
   POSITION_CombPosition,
///组合多头冻结
   POSITION_CombLongFrozen,
///组合空头冻结
   POSITION_CombShortFrozen,
///今日持仓
   POSITION_TodayPosition,
///执行冻结
   POSITION_StrikeFrozen,
///放弃执行冻结
   POSITION_AbandonFrozen,
///执行冻结的昨仓
   POSITION_YdStrikeFrozen,
///tas持仓手数
   POSITION_TasPosition,
  };
/// 持仓信息 DOUBLE
enum ENUM_POSITION_PROPERTY_DOUBLE
  {
///开仓冻结金额
   POSITION_LongFrozenAmount = 0,
///开仓冻结金额
   POSITION_ShortFrozenAmount,
///开仓金额
   POSITION_OpenAmount,
///平仓金额
   POSITION_CloseAmount,
///持仓成本
   POSITION_PositionCost,
///上次占用的保证金
   POSITION_PreMargin,
///占用的保证金
   POSITION_UseMargin,
///冻结的保证金
   POSITION_FrozenMargin,
///冻结的资金
   POSITION_FrozenCash,
///冻结的手续费
   POSITION_FrozenCommission,
///资金差额
   POSITION_CashIn,
///手续费
   POSITION_Commission,
///平仓盈亏
   POSITION_CloseProfit,
///持仓盈亏
   POSITION_PositionProfit,
///上次结算价
   POSITION_PreSettlementPrice,
///本次结算价
   POSITION_SettlementPrice,
///开仓成本
   POSITION_OpenCost,
///开仓浮动盈亏
   POSITION_OpenProfit,
///交易所保证金
   POSITION_ExchangeMargin,
///逐日盯市平仓盈亏
   POSITION_CloseProfitByDate,
///逐笔对冲平仓盈亏
   POSITION_CloseProfitByTrade,
///保证金率
   POSITION_MarginRateByMoney,
///保证金率(按手数)
   POSITION_MarginRateByVolume,
///执行冻结金额
   POSITION_StrikeFrozenAmount,
///大商所持仓成本差值，只有大商所使用
   POSITION_PositionCostOffset,
///止损价
   POSITION_StopLoss,
///止赢价
   POSITION_TakeProfit,
///tas持仓成本
   POSITION_TasPositionCost,
  };
/// 持仓信息 STRING
enum ENUM_POSITION_PROPERTY_STRING
  {
///合约代码
   POSITION_InstrumentID = 0,
///经纪公司代码
   POSITION_BrokerID,
///投资者代码
   POSITION_InvestorID,
///交易日
   POSITION_TradingDay,
///交易所代码
   POSITION_ExchangeID,
///投资单元代码
   POSITION_InvestUnitID,
  };
//+------------------------------------------------------------------+
/// 报单信息 INTEGER
enum ENUM_ORDER_PROPERTY_INTEGER
  {
///报单价格条件
   ORDER_OrderPriceType = 0,
///买卖方向
   ORDER_Direction,
///数量
   ORDER_VolumeTotalOriginal,
///有效期类型
   ORDER_TimeCondition,
///成交量类型
   ORDER_VolumeCondition,
///最小成交量
   ORDER_MinVolume,
///触发条件
   ORDER_ContingentCondition,
///强平原因
   ORDER_ForceCloseReason,
///自动挂起标志
   ORDER_IsAutoSuspend,
///请求编号
   ORDER_RequestID,
///安装编号
   ORDER_InstallID,
///报单提交状态
   ORDER_OrderSubmitStatus,
///报单提示序号
   ORDER_NotifySequence,
///结算编号
   ORDER_SettlementID,
///报单来源
   ORDER_OrderSource,
///报单状态
   ORDER_OrderStatus,
///报单类型
   ORDER_OrderType,
///今成交数量
   ORDER_VolumeTraded,
///剩余数量
   ORDER_VolumeTotal,
///序号
   ORDER_SequenceNo,
///前置编号
   ORDER_FrontID,
///会话编号
   ORDER_SessionID,
///用户强评标志
   ORDER_UserForceClose,
///经纪公司报单编号
   ORDER_BrokerOrderSeq,
///郑商所成交数量
   ORDER_ZCETotalTradedVolume,
///互换单标志
   ORDER_IsSwapOrder,
///错误代码
   ORDER_ErrorID,
  };
/// 报单信息 DOUBLE
enum ENUM_ORDER_PROPERTY_DOUBLE
  {
///价格
   ORDER_LimitPrice = 0,
///止损价
   ORDER_StopPrice,
///止损价
   ORDER_StopLoss,
///止赢价
   ORDER_TakeProfit,
  };
/// 报单信息 STRING
enum ENUM_ORDER_PROPERTY_STRING
  {
///经纪公司代码
   ORDER_BrokerID = 0,
///投资者代码
   ORDER_InvestorID,
///合约代码
   ORDER_InstrumentID,
///报单引用
   ORDER_OrderRef,
///用户代码
   ORDER_UserID,
///组合开平标志
   ORDER_CombOffsetFlag,
///组合投机套保标志
   ORDER_CombHedgeFlag,
///GTD日期
   ORDER_GTDDate,
///业务单元
   ORDER_BusinessUnit,
///本地报单编号
   ORDER_OrderLocalID,
///交易所代码
   ORDER_ExchangeID,
///会员代码
   ORDER_ParticipantID,
///客户代码
   ORDER_ClientID,
///合约在交易所的代码
   ORDER_ExchangeInstID,
///交易所交易员代码
   ORDER_TraderID,
///交易日
   ORDER_TradingDay,
///报单编号
   ORDER_OrderSysID,
///报单日期
   ORDER_InsertDate,
///委托时间
   ORDER_InsertTime,
///激活时间
   ORDER_ActiveTime,
///挂起时间
   ORDER_SuspendTime,
///最后修改时间
   ORDER_UpdateTime,
///撤销时间
   ORDER_CancelTime,
///最后修改交易所交易员代码
   ORDER_ActiveTraderID,
///结算会员编号
   ORDER_ClearingPartID,
///用户端产品信息
   ORDER_UserProductInfo,
///状态信息
   ORDER_StatusMsg,
///操作用户代码
   ORDER_ActiveUserID,
///相关报单
   ORDER_RelativeOrderSysID,
///营业部编号
   ORDER_BranchID,
///投资单元代码
   ORDER_InvestUnitID,
///资金账号
   ORDER_AccountID,
///币种代码
   ORDER_CurrencyID,
///IP地址
   ORDER_IPAddress,
///Mac地址
   ORDER_MacAddress,
///错误信息
   ORDER_ErrorMsg,
  };
//+------------------------------------------------------------------+
/// 成交信息 INTEGER
enum ENUM_DEAL_PROPERTY_INTEGER
  {
///买卖方向
   DEAL_Direction = 0,
///交易角色
   DEAL_TradingRole,
///开平标志
   DEAL_OffsetFlag,
///投机套保标志
   DEAL_HedgeFlag,
///数量
   DEAL_Volume,
///成交类型
   DEAL_TradeType,
///成交价来源
   DEAL_PriceSource,
///序号
   DEAL_SequenceNo,
///结算编号
   DEAL_SettlementID,
///经纪公司报单编号
   DEAL_BrokerOrderSeq,
///成交来源
   DEAL_TradeSource,
  };
/// 成交信息 DOUBLE
enum ENUM_DEAL_PROPERTY_DOUBLE
  {
///价格
   DEAL_Price = 0,
  };
/// 成交信息 STRING
enum ENUM_DEAL_PROPERTY_STRING
  {
///经纪公司代码
   DEAL_BrokerID = 0,
///投资者代码
   DEAL_InvestorID,
///合约代码
   DEAL_InstrumentID,
///报单引用
   DEAL_OrderRef,
///用户代码
   DEAL_UserID,
///交易所代码
   DEAL_ExchangeID,
///成交编号
   DEAL_TradeID,
///报单编号
   DEAL_OrderSysID,
///会员代码
   DEAL_ParticipantID,
///客户代码
   DEAL_ClientID,
///合约在交易所的代码
   DEAL_ExchangeInstID,
///成交日期
   DEAL_TradeDate,
///成交时间
   DEAL_TradeTime,
///交易所交易员代码
   DEAL_TraderID,
///本地报单编号
   DEAL_OrderLocalID,
///结算会员编号
   DEAL_ClearingPartID,
///业务单元
   DEAL_BusinessUnit,
///交易日
   DEAL_TradingDay,
///投资单元代码
   DEAL_InvestUnitID,
  };

//持仓类型
enum ENUM_POSITION_TYPE
  {
   POSITION_TYPE_BUY = '2',
   POSITION_TYPE_SELL = '3',
  };

//买卖方向类型
enum ENUM_ORDER_TYPE
  {
   ORDER_TYPE_BUY = '0',
   ORDER_TYPE_SELL = '1',
  };

//产品类型
enum ENUM_PRODUCT_CLASS
  {
///期货
   THOST_FTDC_PC_Futures = '1',
///期货期权
   THOST_FTDC_PC_Options = '2',
///组合
   THOST_FTDC_PC_Combination = '3',
///即期
   THOST_FTDC_PC_Spot = '4',
///期转现
   THOST_FTDC_PC_EFP = '5',
///现货期权
   THOST_FTDC_PC_SpotOption = '6',
///TAS合约
   THOST_FTDC_PC_TAS = '7',
///金属指数
   THOST_FTDC_PC_MI = 'I',
  };
//产品/合约交易状态
enum ENUM_PRODUCT_STATUS
  {
///开盘前
   THOST_FTDC_IS_BeforeTrading = '0',
///非交易
   THOST_FTDC_IS_NoTrading = '1',
///连续交易
   THOST_FTDC_IS_Continous = '2',
///集合竞价报单
   THOST_FTDC_IS_AuctionOrdering = '3',
///集合竞价价格平衡
   THOST_FTDC_IS_AuctionBalance = '4',
///集合竞价撮合
   THOST_FTDC_IS_AuctionMatch = '5',
///收盘
   THOST_FTDC_IS_Closed = '6',
  };
// 期权权利
enum ENUM_SYMBOL_OPTION_RIGHT
  {
   SYMBOL_OPTION_RIGHT_CALL = '1',
   SYMBOL_OPTION_RIGHT_PUT  = '2',
  };
// 期权类型
enum ENUM_SYMBOL_OPTION_MODE
  {
   SYMBOL_OPTION_MODE_EUROPEAN = '0',
   SYMBOL_OPTION_MODE_AMERICAN = '1',
   SYMBOL_OPTION_MODE_BERMUDA = '2',
  };
//+------------------------------------------------------------------+
// 交易所信息
string ExchangeID(CTP::ENUM_EXCHANGE id)
  {
   string exchange_id;
   switch(id)
     {
      case EXCHANGE_SHFE:
         exchange_id = "SHFE";
         break;
      case EXCHANGE_CZCE:
         exchange_id = "CZCE";
         break;
      case EXCHANGE_DCE:
         exchange_id = "DCE";
         break;
      case EXCHANGE_CFFEX:
         exchange_id = "CFFEX";
         break;
      case EXCHANGE_INE:
         exchange_id = "INE";
         break;
      default:
         exchange_id = "";
         break;
     }
   return exchange_id;
  }
// 账户函数
bool AccountExists()
  {
   long long_var = -1;
   if(!mt5ctp::AccountInfoInteger(CTP::ACCOUNT_UserStatus,long_var))
     {
      return false;
     }
   return(long_var>0);
  }

double  AccountInfoDouble(CTP::ENUM_ACCOUNT_INFO_DOUBLE  property_id)
  {
   double double_var = WRONG_VALUE;
   mt5ctp::AccountInfoDouble(property_id,double_var);
   return double_var;
  }
long  AccountInfoInteger(CTP::ENUM_ACCOUNT_INFO_INTEGER  property_id)
  {
   long long_var = WRONG_VALUE;
   mt5ctp::AccountInfoInteger(property_id,long_var);
   return long_var;
  }
string  AccountInfoString(CTP::ENUM_ACCOUNT_INFO_STRING  property_id)
  {
   string string_var;
   ::StringInit(string_var,MT5CTP_CHAR_BUFFER_SIZE);
   mt5ctp::AccountInfoString(property_id,string_var);
   return string_var;
  }
//+------------------------------------------------------------------+
// 合约函数
bool SymbolExists(string name)
  {
   if(name==NULL || ::StringLen(name)==0)
     {
      return false;
     }
   long is_trading = -1;
   if(!mt5ctp::SymbolInfoInteger(name,CTP::SYMBOL_IsTrading,is_trading))
     {
      return false;
     }
   long this_status = -1;
   if(!mt5ctp::SymbolInfoInteger(name,CTP::SYMBOL_InstrumentStatus,this_status))
     {
      return false;
     }
   bool result = is_trading>0 && (this_status == CTP::THOST_FTDC_IS_Continous || this_status == CTP::THOST_FTDC_IS_AuctionOrdering);
   return(result);
  }

double  SymbolInfoDouble(string name,CTP::ENUM_SYMBOL_INFO_DOUBLE  property_id)
  {
   double double_var = WRONG_VALUE;
   if(!(name==NULL || ::StringLen(name)==0))
     {
      mt5ctp::SymbolInfoDouble(name,property_id,double_var);
     }
   return double_var;
  }

long  SymbolInfoInteger(string name,CTP::ENUM_SYMBOL_INFO_INTEGER  property_id)
  {
   long long_var = WRONG_VALUE;
   if(!(name==NULL || ::StringLen(name)==0))
     {
      mt5ctp::SymbolInfoInteger(name,property_id,long_var);
     }
   return long_var;
  }

string  SymbolInfoString(string name,CTP::ENUM_SYMBOL_INFO_STRING  property_id)
  {
   string string_var;
   if(!(name==NULL || ::StringLen(name)==0))
     {
      ::StringInit(string_var,MT5CTP_CHAR_BUFFER_SIZE);
      mt5ctp::SymbolInfoString(name,property_id,string_var);
     }
   return string_var;
  }
//+------------------------------------------------------------------+
// 交易函数/数据结构转换
bool OrderCheck(MqlTradeRequest& request, MqlTradeCheckResult& result)
  {
   CTPTradeRequest ctp_request = {0};
   CTPTradeCheckResult ctp_result = {0};
//-----------------------------------------------------
   ::StringToCharArray(request.symbol,ctp_request.symbol);
   ::StringToCharArray(request.comment,ctp_request.comment);
   ctp_request.action = int(request.action);
   ctp_request.magic = long(request.magic);
   ctp_request.order = long(request.order);
   ctp_request.volume = request.volume;
   ctp_request.price = request.price;
   ctp_request.stoplimit = request.stoplimit;
   ctp_request.sl = request.sl;
   ctp_request.tp = request.tp;
   ctp_request.deviation = long(request.deviation);
   ctp_request.type = int(request.type);
   ctp_request.type_filling = int(request.type_filling);
   ctp_request.type_time = int(request.type_time);
   ctp_request.expiration = long(request.expiration);
   ctp_request.position = long(request.position);
   ctp_request.position_by = long(request.position_by);
//-----------------------------------------------------
   bool bool_var = mt5ctp::OrderCheck(ctp_request,ctp_result);
//-----------------------------------------------------
   request.comment = ::CharArrayToString(ctp_request.comment);
   result.retcode = uint(ctp_result.retcode);
   result.balance = ctp_result.balance;
   result.equity = ctp_result.equity;
   result.profit = ctp_result.profit;
   result.margin = ctp_result.margin;
   result.margin_free = ctp_result.margin_free;
   result.margin_level = ctp_result.margin_level;
   result.comment = ::CharArrayToString(ctp_result.comment);
//-----------------------------------------------------
   return bool_var;
  }
bool OrderSend(MqlTradeRequest& request, MqlTradeResult& result)
  {
   CTPTradeRequest ctp_request = {0};
   CTPTradeResult ctp_result = {0};
//-----------------------------------------------------
   ::StringToCharArray(request.symbol,ctp_request.symbol);
   ::StringToCharArray(request.comment,ctp_request.comment);
   ctp_request.action = int(request.action);
   ctp_request.magic = long(request.magic);
   ctp_request.order = long(request.order);
   ctp_request.volume = request.volume;
   ctp_request.price = request.price;
   ctp_request.stoplimit = request.stoplimit;
   ctp_request.sl = request.sl;
   ctp_request.tp = request.tp;
   ctp_request.deviation = long(request.deviation);
   ctp_request.type = int(request.type);
   ctp_request.type_filling = int(request.type_filling);
   ctp_request.type_time = int(request.type_time);
   ctp_request.expiration = long(request.expiration);
   ctp_request.position = long(request.position);
   ctp_request.position_by = long(request.position_by);
//-----------------------------------------------------
   bool bool_var = mt5ctp::OrderSend(ctp_request,ctp_result);
//-----------------------------------------------------
   request.comment = ::CharArrayToString(ctp_request.comment);
   result.retcode = uint(ctp_result.retcode);
   result.deal = ulong(ctp_result.deal);
   result.order = ulong(ctp_result.order);
   result.volume = ctp_result.volume;
   result.price = ctp_result.price;
   result.bid = ctp_result.bid;
   result.ask = ctp_result.ask;
   result.comment = ::CharArrayToString(ctp_result.comment);
   result.request_id = uint(ctp_result.request_id);
   result.retcode_external = ctp_result.retcode_external;
//-----------------------------------------------------
   return bool_var;
  }
//+------------------------------------------------------------------+
// 持仓函数
int PositionsTotal()
  {
   return mt5ctp::PositionsTotal();
  }
bool PositionSelect(int index)
  {
   ::ZeroMemory(CTP::CTP_Position);
   return mt5ctp::PositionSelect(index,CTP::CTP_Position);
  }
bool PositionSelectByTicket(string ticket)
  {
   ::ZeroMemory(CTP::CTP_Position);
   return mt5ctp::PositionSelectByTicket(ticket,CTP::CTP_Position);
  }
string PositionGetTicket(int index)
  {
   string string_var;
   ::StringInit(string_var,MT5CTP_CHAR_BUFFER_SIZE);
   mt5ctp::PositionGetTicket(index,string_var);
   return string_var;
  }
int PositionGetIndex(const string ticket)
  {
   int index = mt5ctp::PositionsTotal();
   string string_var;
   ::StringInit(string_var,MT5CTP_CHAR_BUFFER_SIZE);
   for(; index>0; index--)
     {
      mt5ctp::PositionGetTicket(index,string_var);
      if(string_var==ticket)
        {
         break;
        }
     }
   return(index<=0?WRONG_VALUE:index);
  }
string PositionGetSymbol()
  {
   return ::CharArrayToString(CTP::CTP_Position.InstrumentID);
  }
double PositionGetDouble(CTP::ENUM_POSITION_PROPERTY_DOUBLE  property_id)
  {
   double double_var = WRONG_VALUE;
   switch(property_id)
     {
      case CTP::POSITION_LongFrozenAmount:
         double_var = CTP::CTP_Position.LongFrozenAmount;
         break;
      case CTP::POSITION_ShortFrozenAmount:
         double_var = CTP::CTP_Position.ShortFrozenAmount;
         break;
      case CTP::POSITION_OpenAmount:
         double_var = CTP::CTP_Position.OpenAmount;
         break;
      case CTP::POSITION_CloseAmount:
         double_var = CTP::CTP_Position.CloseAmount;
         break;
      case CTP::POSITION_PositionCost:
         double_var = CTP::CTP_Position.PositionCost;
         break;
      case CTP::POSITION_PreMargin:
         double_var = CTP::CTP_Position.PreMargin;
         break;
      case CTP::POSITION_UseMargin:
         double_var = CTP::CTP_Position.UseMargin;
         break;
      case CTP::POSITION_FrozenMargin:
         double_var = CTP::CTP_Position.FrozenMargin;
         break;
      case CTP::POSITION_FrozenCash:
         double_var = CTP::CTP_Position.FrozenCash;
         break;
      case CTP::POSITION_FrozenCommission:
         double_var = CTP::CTP_Position.FrozenCommission;
         break;
      case CTP::POSITION_CashIn:
         double_var = CTP::CTP_Position.CashIn;
         break;
      case CTP::POSITION_Commission:
         double_var = CTP::CTP_Position.Commission;
         break;
      case CTP::POSITION_CloseProfit:
         double_var = CTP::CTP_Position.CloseProfit;
         break;
      case CTP::POSITION_PositionProfit:
         double_var = CTP::CTP_Position.PositionProfit;
         break;
      case CTP::POSITION_PreSettlementPrice:
         double_var = CTP::CTP_Position.PreSettlementPrice;
         break;
      case CTP::POSITION_SettlementPrice:
         double_var = CTP::CTP_Position.SettlementPrice;
         break;
      case CTP::POSITION_OpenCost:
         double_var = CTP::CTP_Position.OpenCost;
         break;
      case CTP::POSITION_OpenProfit:
         double_var = CTP::CTP_Position.OpenProfit;
         break;
      case CTP::POSITION_ExchangeMargin:
         double_var = CTP::CTP_Position.ExchangeMargin;
         break;
      case CTP::POSITION_CloseProfitByDate:
         double_var = CTP::CTP_Position.CloseProfitByDate;
         break;
      case CTP::POSITION_CloseProfitByTrade:
         double_var = CTP::CTP_Position.CloseProfitByTrade;
         break;
      case CTP::POSITION_MarginRateByMoney:
         double_var = CTP::CTP_Position.MarginRateByMoney;
         break;
      case CTP::POSITION_MarginRateByVolume:
         double_var = CTP::CTP_Position.MarginRateByVolume;
         break;
      case CTP::POSITION_StrikeFrozenAmount:
         double_var = CTP::CTP_Position.StrikeFrozenAmount;
         break;
      case CTP::POSITION_PositionCostOffset:
         double_var = CTP::CTP_Position.PositionCostOffset;
         break;
      case CTP::POSITION_StopLoss:
         double_var = CTP::CTP_Position.StopLoss;
         break;
      case CTP::POSITION_TakeProfit:
         double_var = CTP::CTP_Position.TakeProfit;
         break;
      case CTP::POSITION_TasPositionCost:
         double_var = CTP::CTP_Position.TasPositionCost;
         break;
      default:
         break;
     }
   return double_var;
  }

bool PositionGetDouble(CTP::ENUM_POSITION_PROPERTY_DOUBLE  property_id, double& double_var)
  {
   double_var = CTP::PositionGetDouble(property_id);
   return !((long)double_var==WRONG_VALUE);
  }
long  PositionGetInteger(CTP::ENUM_POSITION_PROPERTY_INTEGER  property_id)
  {
   long long_var = WRONG_VALUE;
   switch(property_id)
     {
      case CTP::POSITION_PosiDirection:
         long_var = (long)CTP::CTP_Position.PosiDirection;
         break;
      case CTP::POSITION_HedgeFlag:
         long_var = (long)CTP::CTP_Position.HedgeFlag;
         break;
      case CTP::POSITION_PositionDate:
         long_var = (long)CTP::CTP_Position.PositionDate;
         break;
      case CTP::POSITION_YdPosition:
         long_var = (long)CTP::CTP_Position.YdPosition;
         break;
      case CTP::POSITION_Position:
         long_var = (long)CTP::CTP_Position.Position;
         break;
      case CTP::POSITION_LongFrozen:
         long_var = (long)CTP::CTP_Position.LongFrozen;
         break;
      case CTP::POSITION_ShortFrozen:
         long_var = (long)CTP::CTP_Position.ShortFrozen;
         break;
      case CTP::POSITION_OpenVolume:
         long_var = (long)CTP::CTP_Position.OpenVolume;
         break;
      case CTP::POSITION_CloseVolume:
         long_var = (long)CTP::CTP_Position.CloseVolume;
         break;
      case CTP::POSITION_SettlementID:
         long_var = (long)CTP::CTP_Position.SettlementID;
         break;
      case CTP::POSITION_CombPosition:
         long_var = (long)CTP::CTP_Position.CombPosition;
         break;
      case CTP::POSITION_CombLongFrozen:
         long_var = (long)CTP::CTP_Position.CombLongFrozen;
         break;
      case CTP::POSITION_CombShortFrozen:
         long_var = (long)CTP::CTP_Position.CombShortFrozen;
         break;
      case CTP::POSITION_TodayPosition:
         long_var = (long)CTP::CTP_Position.TodayPosition;
         break;
      case CTP::POSITION_StrikeFrozen:
         long_var = (long)CTP::CTP_Position.StrikeFrozen;
         break;
      case CTP::POSITION_AbandonFrozen:
         long_var = (long)CTP::CTP_Position.AbandonFrozen;
         break;
      case CTP::POSITION_YdStrikeFrozen:
         long_var = (long)CTP::CTP_Position.YdStrikeFrozen;
         break;
      case CTP::POSITION_TasPosition:
         long_var = (long)CTP::CTP_Position.TasPosition;
         break;
      default:
         break;
     }
   return long_var;
  }

bool  PositionGetInteger(CTP::ENUM_POSITION_PROPERTY_INTEGER  property_id, long& long_var)
  {
   long_var = CTP::PositionGetInteger(property_id);
   return (long_var!=WRONG_VALUE);
  }
string  PositionGetString(CTP::ENUM_POSITION_PROPERTY_STRING  property_id)
  {
   string string_var;
   switch(property_id)
     {
      case CTP::POSITION_InstrumentID:
         string_var = ::CharArrayToString(CTP::CTP_Position.InstrumentID);
         break;
      case CTP::POSITION_BrokerID:
         string_var = ::CharArrayToString(CTP::CTP_Position.BrokerID);
         break;
      case CTP::POSITION_InvestorID:
         string_var = ::CharArrayToString(CTP::CTP_Position.InvestorID);
         break;
      case CTP::POSITION_TradingDay:
         string_var = ::CharArrayToString(CTP::CTP_Position.TradingDay);
         break;
      case CTP::POSITION_ExchangeID:
         string_var = ::CharArrayToString(CTP::CTP_Position.ExchangeID);
         break;
      case CTP::POSITION_InvestUnitID:
         string_var = ::CharArrayToString(CTP::CTP_Position.InvestUnitID);
         break;
      default:
         break;
     }
   return string_var;
  }
bool  PositionGetString(CTP::ENUM_POSITION_PROPERTY_STRING  property_id, string& string_var)
  {
   string_var = CTP::PositionGetString(property_id);
   return (::StringLen(string_var) > 0);
  }
//+------------------------------------------------------------------+
// 报单函数
int  OrdersTotal()
  {
   return mt5ctp::OrdersTotal();
  }
bool OrderSelect(int index)
  {
   ::ZeroMemory(CTP::CTP_Order);
   return mt5ctp::OrderSelect(index,CTP::CTP_Order);
  }
bool OrderSelectByTicket(string ticket)
  {
   ::ZeroMemory(CTP::CTP_Order);
   return mt5ctp::OrderSelectByTicket(ticket,CTP::CTP_Order);
  }
string OrderGetTicket(int index)
  {
   string string_var;
   ::StringInit(string_var,MT5CTP_CHAR_BUFFER_SIZE);
   mt5ctp::OrderGetTicket(index,string_var);
   return string_var;
  }
int OrderGetIndex(const string ticket)
  {
   int index = mt5ctp::OrdersTotal();
   string string_var;
   ::StringInit(string_var,MT5CTP_CHAR_BUFFER_SIZE);
   for(; index>0; index--)
     {
      mt5ctp::OrderGetTicket(index,string_var);
      if(string_var==ticket)
        {
         break;
        }
     }
   return(index<=0?WRONG_VALUE:index);
  }
double OrderGetDouble(CTP::ENUM_ORDER_PROPERTY_DOUBLE  property_id)
  {
   double double_var = WRONG_VALUE;
   switch(property_id)
     {
      case CTP::ORDER_LimitPrice:
         double_var = CTP::CTP_Order.LimitPrice;
         break;
      case CTP::ORDER_StopPrice:
         double_var = CTP::CTP_Order.StopPrice;
         break;
      case CTP::ORDER_StopLoss:
         double_var = CTP::CTP_Order.StopLoss;
         break;
      case CTP::ORDER_TakeProfit:
         double_var = CTP::CTP_Order.TakeProfit;
         break;
      default:
         break;
     }
   return double_var;
  }
bool  OrderGetDouble(CTP::ENUM_ORDER_PROPERTY_DOUBLE  property_id, double& double_var)
  {
   double_var = CTP::OrderGetDouble(property_id);
   return !((long)double_var==WRONG_VALUE);
  }
long  OrderGetInteger(CTP::ENUM_ORDER_PROPERTY_INTEGER  property_id)
  {
   long long_var = WRONG_VALUE;
   switch(property_id)
     {
      case CTP::ORDER_OrderPriceType:
         long_var = (long)CTP::CTP_Order.OrderPriceType;
         break;
      case CTP::ORDER_Direction:
         long_var = (long)CTP::CTP_Order.Direction;
         break;
      case CTP::ORDER_VolumeTotalOriginal:
         long_var = (long)CTP::CTP_Order.VolumeTotalOriginal;
         break;
      case CTP::ORDER_TimeCondition:
         long_var = (long)CTP::CTP_Order.TimeCondition;
         break;
      case CTP::ORDER_VolumeCondition:
         long_var = (long)CTP::CTP_Order.VolumeCondition;
         break;
      case CTP::ORDER_MinVolume:
         long_var = (long)CTP::CTP_Order.MinVolume;
         break;
      case CTP::ORDER_ContingentCondition:
         long_var = (long)CTP::CTP_Order.ContingentCondition;
         break;
      case CTP::ORDER_ForceCloseReason:
         long_var = (long)CTP::CTP_Order.ForceCloseReason;
         break;
      case CTP::ORDER_IsAutoSuspend:
         long_var = (long)CTP::CTP_Order.IsAutoSuspend;
         break;
      case CTP::ORDER_RequestID:
         long_var = (long)CTP::CTP_Order.RequestID;
         break;
      case CTP::ORDER_InstallID:
         long_var = (long)CTP::CTP_Order.InstallID;
         break;
      case CTP::ORDER_OrderSubmitStatus:
         long_var = (long)CTP::CTP_Order.OrderSubmitStatus;
         break;
      case CTP::ORDER_NotifySequence:
         long_var = (long)CTP::CTP_Order.NotifySequence;
         break;
      case CTP::ORDER_SettlementID:
         long_var = (long)CTP::CTP_Order.SettlementID;
         break;
      case CTP::ORDER_OrderSource:
         long_var = (long)CTP::CTP_Order.OrderSource;
         break;
      case CTP::ORDER_OrderStatus:
         long_var = (long)CTP::CTP_Order.OrderStatus;
         break;
      case CTP::ORDER_OrderType:
         long_var = (long)CTP::CTP_Order.OrderType;
         break;
      case CTP::ORDER_VolumeTraded:
         long_var = (long)CTP::CTP_Order.VolumeTraded;
         break;
      case CTP::ORDER_VolumeTotal:
         long_var = (long)CTP::CTP_Order.VolumeTotal;
         break;
      case CTP::ORDER_SequenceNo:
         long_var = (long)CTP::CTP_Order.SequenceNo;
         break;
      case CTP::ORDER_FrontID:
         long_var = (long)CTP::CTP_Order.FrontID;
         break;
      case CTP::ORDER_SessionID:
         long_var = (long)CTP::CTP_Order.SessionID;
         break;
      case CTP::ORDER_UserForceClose:
         long_var = (long)CTP::CTP_Order.UserForceClose;
         break;
      case CTP::ORDER_BrokerOrderSeq:
         long_var = (long)CTP::CTP_Order.BrokerOrderSeq;
         break;
      case CTP::ORDER_ZCETotalTradedVolume:
         long_var = (long)CTP::CTP_Order.ZCETotalTradedVolume;
         break;
      case CTP::ORDER_IsSwapOrder:
         long_var = (long)CTP::CTP_Order.IsSwapOrder;
         break;
      default:
         break;
     }
   return long_var;
  }
bool  OrderGetInteger(CTP::ENUM_ORDER_PROPERTY_INTEGER  property_id, long& long_var)
  {
   long_var = CTP::OrderGetInteger(property_id);
   return !(long_var==WRONG_VALUE);
  }
string  OrderGetString(CTP::ENUM_ORDER_PROPERTY_STRING  property_id)
  {
   string string_var;
   switch(property_id)
     {
      case CTP::ORDER_BrokerID:
         string_var = ::CharArrayToString(CTP::CTP_Order.BrokerID);
         break;
      case CTP::ORDER_InvestorID:
         string_var = ::CharArrayToString(CTP::CTP_Order.InvestorID);
         break;
      case CTP::ORDER_InstrumentID:
         string_var = ::CharArrayToString(CTP::CTP_Order.InstrumentID);
         break;
      case CTP::ORDER_OrderRef:
         string_var = ::CharArrayToString(CTP::CTP_Order.OrderRef);
         break;
      case CTP::ORDER_UserID:
         string_var = ::CharArrayToString(CTP::CTP_Order.UserID);
         break;
      case CTP::ORDER_CombOffsetFlag:
         string_var = ::CharArrayToString(CTP::CTP_Order.CombOffsetFlag);
         break;
      case CTP::ORDER_CombHedgeFlag:
         string_var = ::CharArrayToString(CTP::CTP_Order.CombHedgeFlag);
         break;
      case CTP::ORDER_GTDDate:
         string_var = ::CharArrayToString(CTP::CTP_Order.GTDDate);
         break;
      case CTP::ORDER_BusinessUnit:
         string_var = ::CharArrayToString(CTP::CTP_Order.BusinessUnit);
         break;
      case CTP::ORDER_OrderLocalID:
         string_var = ::CharArrayToString(CTP::CTP_Order.OrderLocalID);
         break;
      case CTP::ORDER_ExchangeID:
         string_var = ::CharArrayToString(CTP::CTP_Order.ExchangeID);
         break;
      case CTP::ORDER_ParticipantID:
         string_var = ::CharArrayToString(CTP::CTP_Order.ParticipantID);
         break;
      case CTP::ORDER_ClientID:
         string_var = ::CharArrayToString(CTP::CTP_Order.ClientID);
         break;
      case CTP::ORDER_ExchangeInstID:
         string_var = ::CharArrayToString(CTP::CTP_Order.ExchangeInstID);
         break;
      case CTP::ORDER_TraderID:
         string_var = ::CharArrayToString(CTP::CTP_Order.TraderID);
         break;
      case CTP::ORDER_TradingDay:
         string_var = ::CharArrayToString(CTP::CTP_Order.TradingDay);
         break;
      case CTP::ORDER_OrderSysID:
         string_var = ::CharArrayToString(CTP::CTP_Order.OrderSysID);
         break;
      case CTP::ORDER_InsertDate:
         string_var = ::CharArrayToString(CTP::CTP_Order.InsertDate);
         break;
      case CTP::ORDER_InsertTime:
         string_var = ::CharArrayToString(CTP::CTP_Order.InsertTime);
         break;
      case CTP::ORDER_ActiveTime:
         string_var = ::CharArrayToString(CTP::CTP_Order.ActiveTime);
         break;
      case CTP::ORDER_SuspendTime:
         string_var = ::CharArrayToString(CTP::CTP_Order.SuspendTime);
         break;
      case CTP::ORDER_UpdateTime:
         string_var = ::CharArrayToString(CTP::CTP_Order.UpdateTime);
         break;
      case CTP::ORDER_CancelTime:
         string_var = ::CharArrayToString(CTP::CTP_Order.CancelTime);
         break;
      case CTP::ORDER_ActiveTraderID:
         string_var = ::CharArrayToString(CTP::CTP_Order.ActiveTraderID);
         break;
      case CTP::ORDER_ClearingPartID:
         string_var = ::CharArrayToString(CTP::CTP_Order.ClearingPartID);
         break;
      case CTP::ORDER_UserProductInfo:
         string_var = ::CharArrayToString(CTP::CTP_Order.UserProductInfo);
         break;
      case CTP::ORDER_StatusMsg:
         string_var = ::CharArrayToString(CTP::CTP_Order.StatusMsg);
         break;
      case CTP::ORDER_ActiveUserID:
         string_var = ::CharArrayToString(CTP::CTP_Order.ActiveUserID);
         break;
      case CTP::ORDER_RelativeOrderSysID:
         string_var = ::CharArrayToString(CTP::CTP_Order.RelativeOrderSysID);
         break;
      case CTP::ORDER_BranchID:
         string_var = ::CharArrayToString(CTP::CTP_Order.BranchID);
         break;
      case CTP::ORDER_InvestUnitID:
         string_var = ::CharArrayToString(CTP::CTP_Order.InvestUnitID);
         break;
      case CTP::ORDER_AccountID:
         string_var = ::CharArrayToString(CTP::CTP_Order.AccountID);
         break;
      case CTP::ORDER_CurrencyID:
         string_var = ::CharArrayToString(CTP::CTP_Order.CurrencyID);
         break;
      case CTP::ORDER_IPAddress:
         string_var = ::CharArrayToString(CTP::CTP_Order.IPAddress);
         break;
      case CTP::ORDER_MacAddress:
         string_var = ::CharArrayToString(CTP::CTP_Order.MacAddress);
         break;
      default:
         break;
     }
   return string_var;
  }
bool  OrderGetString(CTP::ENUM_ORDER_PROPERTY_STRING  property_id, string& string_var)
  {
   string_var = CTP::OrderGetString(property_id);
   return (::StringLen(string_var)>0);
  }
//+------------------------------------------------------------------+
// 报单函数
int  HistoryOrdersTotal()
  {
   return mt5ctp::HistoryOrdersTotal();
  }
bool HistoryOrderSelect(int index)
  {
   ::ZeroMemory(CTP::CTP_HistoryOrder);
   return mt5ctp::HistoryOrderSelect(index,CTP::CTP_HistoryOrder);
  }
bool HistoryOrderSelectByTicket(string ticket)
  {
   ::ZeroMemory(CTP::CTP_HistoryOrder);
   return mt5ctp::HistoryOrderSelectByTicket(ticket,CTP::CTP_HistoryOrder);
  }
string HistoryOrderGetTicket(int index)
  {
   string string_var;
   ::StringInit(string_var,MT5CTP_CHAR_BUFFER_SIZE);
   mt5ctp::HistoryOrderGetTicket(index,string_var);
   return string_var;
  }
double HistoryOrderGetDouble(CTP::ENUM_ORDER_PROPERTY_DOUBLE  property_id)
  {
   double double_var = WRONG_VALUE;
   switch(property_id)
     {
      case CTP::ORDER_LimitPrice:
         double_var = CTP::CTP_HistoryOrder.LimitPrice;
         break;
      case CTP::ORDER_StopPrice:
         double_var = CTP::CTP_HistoryOrder.StopPrice;
         break;
      case CTP::ORDER_StopLoss:
         double_var = CTP::CTP_HistoryOrder.StopLoss;
         break;
      case CTP::ORDER_TakeProfit:
         double_var = CTP::CTP_HistoryOrder.TakeProfit;
         break;
      default:
         break;
     }
   return double_var;
  }
bool  HistoryOrderGetDouble(CTP::ENUM_ORDER_PROPERTY_DOUBLE  property_id, double& double_var)
  {
   double_var = CTP::HistoryOrderGetDouble(property_id);
   return !((long)double_var==WRONG_VALUE);
  }
long  HistoryOrderGetInteger(CTP::ENUM_ORDER_PROPERTY_INTEGER  property_id)
  {
   long long_var = WRONG_VALUE;
   switch(property_id)
     {
      case CTP::ORDER_OrderPriceType:
         long_var = (long)CTP::CTP_HistoryOrder.OrderPriceType;
         break;
      case CTP::ORDER_Direction:
         long_var = (long)CTP::CTP_HistoryOrder.Direction;
         break;
      case CTP::ORDER_VolumeTotalOriginal:
         long_var = (long)CTP::CTP_HistoryOrder.VolumeTotalOriginal;
         break;
      case CTP::ORDER_TimeCondition:
         long_var = (long)CTP::CTP_HistoryOrder.TimeCondition;
         break;
      case CTP::ORDER_VolumeCondition:
         long_var = (long)CTP::CTP_HistoryOrder.VolumeCondition;
         break;
      case CTP::ORDER_MinVolume:
         long_var = (long)CTP::CTP_HistoryOrder.MinVolume;
         break;
      case CTP::ORDER_ContingentCondition:
         long_var = (long)CTP::CTP_HistoryOrder.ContingentCondition;
         break;
      case CTP::ORDER_ForceCloseReason:
         long_var = (long)CTP::CTP_HistoryOrder.ForceCloseReason;
         break;
      case CTP::ORDER_IsAutoSuspend:
         long_var = (long)CTP::CTP_HistoryOrder.IsAutoSuspend;
         break;
      case CTP::ORDER_RequestID:
         long_var = (long)CTP::CTP_HistoryOrder.RequestID;
         break;
      case CTP::ORDER_InstallID:
         long_var = (long)CTP::CTP_HistoryOrder.InstallID;
         break;
      case CTP::ORDER_OrderSubmitStatus:
         long_var = (long)CTP::CTP_HistoryOrder.OrderSubmitStatus;
         break;
      case CTP::ORDER_NotifySequence:
         long_var = (long)CTP::CTP_HistoryOrder.NotifySequence;
         break;
      case CTP::ORDER_SettlementID:
         long_var = (long)CTP::CTP_HistoryOrder.SettlementID;
         break;
      case CTP::ORDER_OrderSource:
         long_var = (long)CTP::CTP_HistoryOrder.OrderSource;
         break;
      case CTP::ORDER_OrderStatus:
         long_var = (long)CTP::CTP_HistoryOrder.OrderStatus;
         break;
      case CTP::ORDER_OrderType:
         long_var = (long)CTP::CTP_HistoryOrder.OrderType;
         break;
      case CTP::ORDER_VolumeTraded:
         long_var = (long)CTP::CTP_HistoryOrder.VolumeTraded;
         break;
      case CTP::ORDER_VolumeTotal:
         long_var = (long)CTP::CTP_HistoryOrder.VolumeTotal;
         break;
      case CTP::ORDER_SequenceNo:
         long_var = (long)CTP::CTP_HistoryOrder.SequenceNo;
         break;
      case CTP::ORDER_FrontID:
         long_var = (long)CTP::CTP_HistoryOrder.FrontID;
         break;
      case CTP::ORDER_SessionID:
         long_var = (long)CTP::CTP_HistoryOrder.SessionID;
         break;
      case CTP::ORDER_UserForceClose:
         long_var = (long)CTP::CTP_HistoryOrder.UserForceClose;
         break;
      case CTP::ORDER_BrokerOrderSeq:
         long_var = (long)CTP::CTP_HistoryOrder.BrokerOrderSeq;
         break;
      case CTP::ORDER_ZCETotalTradedVolume:
         long_var = (long)CTP::CTP_HistoryOrder.ZCETotalTradedVolume;
         break;
      case CTP::ORDER_IsSwapOrder:
         long_var = (long)CTP::CTP_HistoryOrder.IsSwapOrder;
         break;
      case CTP::ORDER_ErrorID:
         long_var = (long)CTP::CTP_HistoryOrder.ErrorID;
         break;
      default:
         break;
     }
   return long_var;
  }
bool  HistoryOrderGetInteger(CTP::ENUM_ORDER_PROPERTY_INTEGER  property_id, long& long_var)
  {
   long_var = CTP::HistoryOrderGetInteger(property_id);
   return !(long_var==WRONG_VALUE);
  }
string  HistoryOrderGetString(CTP::ENUM_ORDER_PROPERTY_STRING  property_id)
  {
   string string_var;
   switch(property_id)
     {
      case CTP::ORDER_BrokerID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.BrokerID);
         break;
      case CTP::ORDER_InvestorID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.InvestorID);
         break;
      case CTP::ORDER_InstrumentID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.InstrumentID);
         break;
      case CTP::ORDER_OrderRef:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.OrderRef);
         break;
      case CTP::ORDER_UserID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.UserID);
         break;
      case CTP::ORDER_CombOffsetFlag:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.CombOffsetFlag);
         break;
      case CTP::ORDER_CombHedgeFlag:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.CombHedgeFlag);
         break;
      case CTP::ORDER_GTDDate:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.GTDDate);
         break;
      case CTP::ORDER_BusinessUnit:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.BusinessUnit);
         break;
      case CTP::ORDER_OrderLocalID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.OrderLocalID);
         break;
      case CTP::ORDER_ExchangeID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.ExchangeID);
         break;
      case CTP::ORDER_ParticipantID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.ParticipantID);
         break;
      case CTP::ORDER_ClientID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.ClientID);
         break;
      case CTP::ORDER_ExchangeInstID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.ExchangeInstID);
         break;
      case CTP::ORDER_TraderID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.TraderID);
         break;
      case CTP::ORDER_TradingDay:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.TradingDay);
         break;
      case CTP::ORDER_OrderSysID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.OrderSysID);
         break;
      case CTP::ORDER_InsertDate:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.InsertDate);
         break;
      case CTP::ORDER_InsertTime:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.InsertTime);
         break;
      case CTP::ORDER_ActiveTime:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.ActiveTime);
         break;
      case CTP::ORDER_SuspendTime:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.SuspendTime);
         break;
      case CTP::ORDER_UpdateTime:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.UpdateTime);
         break;
      case CTP::ORDER_CancelTime:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.CancelTime);
         break;
      case CTP::ORDER_ActiveTraderID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.ActiveTraderID);
         break;
      case CTP::ORDER_ClearingPartID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.ClearingPartID);
         break;
      case CTP::ORDER_UserProductInfo:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.UserProductInfo);
         break;
      case CTP::ORDER_StatusMsg:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.StatusMsg);
         break;
      case CTP::ORDER_ActiveUserID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.ActiveUserID);
         break;
      case CTP::ORDER_RelativeOrderSysID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.RelativeOrderSysID);
         break;
      case CTP::ORDER_BranchID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.BranchID);
         break;
      case CTP::ORDER_InvestUnitID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.InvestUnitID);
         break;
      case CTP::ORDER_AccountID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.AccountID);
         break;
      case CTP::ORDER_CurrencyID:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.CurrencyID);
         break;
      case CTP::ORDER_IPAddress:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.IPAddress);
         break;
      case CTP::ORDER_MacAddress:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.MacAddress);
         break;
      case CTP::ORDER_ErrorMsg:
         string_var = ::CharArrayToString(CTP::CTP_HistoryOrder.ErrorMsg);
         break;
      default:
         break;
     }
   return string_var;
  }
bool  HistoryOrderGetString(CTP::ENUM_ORDER_PROPERTY_STRING  property_id, string& string_var)
  {
   string_var = CTP::HistoryOrderGetString(property_id);
   return (::StringLen(string_var)>0);
  }
//+------------------------------------------------------------------+
// 成交函数
int  HistoryDealsTotal()
  {
   return mt5ctp::HistoryDealsTotal();
  }
bool HistoryDealSelect(int index)
  {
   ::ZeroMemory(CTP::CTP_HistoryDeal);
   return mt5ctp::HistoryDealSelect(index,CTP::CTP_HistoryDeal);
  }
bool HistoryDealSelectByTicket(string ticket)
  {
   ::ZeroMemory(CTP::CTP_HistoryDeal);
   return mt5ctp::HistoryDealSelectByTicket(ticket,CTP::CTP_HistoryDeal);
  }
string HistoryDealGetTicket(int index)
  {
   string string_var;
   ::StringInit(string_var,MT5CTP_CHAR_BUFFER_SIZE);
   mt5ctp::HistoryDealGetTicket(index,string_var);
   return string_var;
  }
double HistoryDealGetDouble(CTP::ENUM_DEAL_PROPERTY_DOUBLE  property_id)
  {
   double double_var = WRONG_VALUE;
   switch(property_id)
     {
      case CTP::DEAL_Price:
         double_var = CTP::CTP_HistoryDeal.Price;
         break;
      default:
         break;
     }
   return double_var;
  }
bool  HistoryDealGetDouble(CTP::ENUM_DEAL_PROPERTY_DOUBLE  property_id, double& double_var)
  {
   double_var = CTP::HistoryDealGetDouble(property_id);
   return !((long)double_var==WRONG_VALUE);
  }
long  HistoryDealGetInteger(CTP::ENUM_DEAL_PROPERTY_INTEGER  property_id)
  {
   long long_var = WRONG_VALUE;
   switch(property_id)
     {
      case CTP::DEAL_Direction:
         long_var = (long)CTP::CTP_HistoryDeal.Direction;
         break;
      case CTP::DEAL_TradingRole:
         long_var = (long)CTP::CTP_HistoryDeal.TradingRole;
         break;
      case CTP::DEAL_OffsetFlag:
         long_var = (long)CTP::CTP_HistoryDeal.OffsetFlag;
         break;
      case CTP::DEAL_HedgeFlag:
         long_var = (long)CTP::CTP_HistoryDeal.HedgeFlag;
         break;
      case CTP::DEAL_Volume:
         long_var = (long)CTP::CTP_HistoryDeal.Volume;
         break;
      case CTP::DEAL_TradeType:
         long_var = (long)CTP::CTP_HistoryDeal.TradeType;
         break;
      case CTP::DEAL_PriceSource:
         long_var = (long)CTP::CTP_HistoryDeal.PriceSource;
         break;
      case CTP::DEAL_SequenceNo:
         long_var = (long)CTP::CTP_HistoryDeal.SequenceNo;
         break;
      case CTP::DEAL_SettlementID:
         long_var = (long)CTP::CTP_HistoryDeal.SettlementID;
         break;
      case CTP::DEAL_BrokerOrderSeq:
         long_var = (long)CTP::CTP_HistoryDeal.BrokerOrderSeq;
         break;
      case CTP::DEAL_TradeSource:
         long_var = (long)CTP::CTP_HistoryDeal.TradeSource;
         break;
      default:
         break;
     }
   return long_var;
  }
bool  HistoryDealGetInteger(CTP::ENUM_DEAL_PROPERTY_INTEGER  property_id, long& long_var)
  {
   long_var = CTP::HistoryDealGetInteger(property_id);
   return !(long_var==WRONG_VALUE);
  }
string  HistoryDealGetString(CTP::ENUM_DEAL_PROPERTY_STRING  property_id)
  {
   string string_var;
   switch(property_id)
     {
      case CTP::DEAL_BrokerID :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.BrokerID);
         break;
      case CTP::DEAL_InvestorID :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.InvestorID);
         break;
      case CTP::DEAL_InstrumentID :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.InstrumentID);
         break;
      case CTP::DEAL_OrderRef :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.OrderRef);
         break;
      case CTP::DEAL_UserID :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.UserID);
         break;
      case CTP::DEAL_ExchangeID :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.ExchangeID);
         break;
      case CTP::DEAL_TradeID :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.TradeID);
         break;
      case CTP::DEAL_OrderSysID :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.OrderSysID);
         break;
      case CTP::DEAL_ParticipantID :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.ParticipantID);
         break;
      case CTP::DEAL_ClientID :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.ClientID);
         break;
      case CTP::DEAL_ExchangeInstID :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.ExchangeInstID);
         break;
      case CTP::DEAL_TradeDate :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.TradeDate);
         break;
      case CTP::DEAL_TradeTime :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.TradeTime);
         break;
      case CTP::DEAL_TraderID :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.TraderID);
         break;
      case CTP::DEAL_OrderLocalID :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.OrderLocalID);
         break;
      case CTP::DEAL_ClearingPartID :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.ClearingPartID);
         break;
      case CTP::DEAL_BusinessUnit :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.BusinessUnit);
         break;
      case CTP::DEAL_TradingDay :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.TradingDay);
         break;
      case CTP::DEAL_InvestUnitID :
         string_var = ::CharArrayToString(CTP::CTP_HistoryDeal.InvestUnitID);
         break;
      default:
         break;
     }
   return string_var;
  }
bool  HistoryDealGetString(CTP::ENUM_DEAL_PROPERTY_STRING  property_id, string& string_var)
  {
   string_var = CTP::HistoryDealGetString(property_id);
   return (::StringLen(string_var)>0);
  }
//+------------------------------------------------------------------+
// 功能函数
// 交易时段总数
int SessionsTotal(string this_symbol)
  {
   int sessions = WRONG_VALUE;
   CFileTxt session;
   string session_json;
   if(session.IsExist(EXPERT_SESSION_FILE) && session.Open(EXPERT_SESSION_FILE,FILE_READ))
     {
      while(!session.IsEnding())
        {
         session_json += session.ReadString();
        }
      session.Close();
     }
   CJAVal symbol_session(NULL,jtUNDEF);
   if(symbol_session.Deserialize(session_json))
     {
      CJAVal* check = symbol_session.FindKey(::SymbolInfoString(this_symbol,SYMBOL_CATEGORY));
      if(::CheckPointer(check)!=POINTER_INVALID && check.m_type==jtARRAY)
        {
         sessions = check.Size();
        }
      delete check;
     }
   return sessions;
  }
// 交易时段开始时间/字符串
string SessionStart(string this_symbol,int index)
  {
   string time_str;
   CFileTxt session;
   string session_json;
   if(session.IsExist(EXPERT_SESSION_FILE) && session.Open(EXPERT_SESSION_FILE,FILE_READ))
     {
      while(!session.IsEnding())
        {
         session_json += session.ReadString();
        }
      session.Close();
     }
   CJAVal symbol_session(NULL,jtUNDEF);
   if(symbol_session.Deserialize(session_json))
     {
      CJAVal* check = symbol_session.FindKey(::SymbolInfoString(this_symbol,SYMBOL_CATEGORY));
      if(::CheckPointer(check)!=POINTER_INVALID && check.m_type==jtARRAY)
        {
         if(index>=0 && index<check.Size())
           {
            time_str = check[index]["Entry"].ToStr();
           }
        }
      delete check;
     }
   return time_str;
  }
// 交易时段结束时间/字符串
string SessionEnd(string this_symbol,int index)
  {
   string time_str;
   CFileTxt session;
   string session_json;
   if(session.IsExist(EXPERT_SESSION_FILE) && session.Open(EXPERT_SESSION_FILE,FILE_READ))
     {
      while(!session.IsEnding())
        {
         session_json += session.ReadString();
        }
      session.Close();
     }
   CJAVal symbol_session(NULL,jtUNDEF);
   if(symbol_session.Deserialize(session_json))
     {
      CJAVal* check = symbol_session.FindKey(::SymbolInfoString(this_symbol,SYMBOL_CATEGORY));
      if(::CheckPointer(check)!=POINTER_INVALID && check.m_type==jtARRAY)
        {
         if(index>=0 && index<check.Size())
           {
            time_str = check[index]["Exit"].ToStr();
           }
        }
      delete check;
     }
   return time_str;
  }
// 交易时段开始时间/本地时间
datetime SessionStartTime(string this_symbol,int index)
  {
   return StringToTimeLocal(SessionStart(this_symbol,index));
  }
// 交易时段结束时间/本地时间
datetime SessionEndTime(string this_symbol,int index)
  {
   return StringToTimeLocal(SessionEnd(this_symbol,index));
  }
// 时间字符串转换成本地时间/过滤日期/只对时间起作用
datetime StringToTimeLocal(string time_str)
  {
   MqlDateTime local = {0};
   MqlDateTime time = {0};
   if(::TimeToStruct(::TimeLocal(),local) && ::TimeToStruct(::StringToTime(time_str),time))
     {
      local.hour = time.hour;
      local.min = time.min;
      local.sec = time.sec;
     }
   return(::StructToTime(local));
  }
//+------------------------------------------------------------------+
} // end namespace CTP
//+------------------------------------------------------------------+
//| namespace  MT5                                                   |
//+------------------------------------------------------------------+
namespace MT5
{
//mt5明细报单/持仓//
MT5CTPOrders _MT5_CTP_Order_ = {0};
//返回持仓的数量
int PositionsTotal(void)
  {
   return(mt5ctp::MT5PositionsTotal());
  }
//该函数返回持仓列表中指定索引的持仓 ticket
ulong PositionGetTicket(int index)
  {
   ulong ticket = 0;
   mt5ctp::MT5PositionGetTicket(index,ticket);
   return(ticket);
  }
//根据持仓中指定的 ticket 选择持仓
bool PositionSelectByTicket(ulong ticket)
  {
   ::ZeroMemory(_MT5_CTP_Order_);
   return(mt5ctp::MT5PositionSelectByTicket(ticket,_MT5_CTP_Order_));
  }
// 删除持仓记录
bool PositionDelete(ulong ticket)
  {
   return(mt5ctp::MT5PositionDelete(ticket));
  }
//函数返回持仓属性:Integer
long PositionGetInteger(ENUM_POSITION_PROPERTY_INTEGER  property_id)
  {
   long long_var = WRONG_VALUE;
   switch(property_id)
     {
      case POSITION_TICKET:
         long_var = (long)_MT5_CTP_Order_.ticket;
         break;
      case POSITION_TIME:
         long_var = (datetime)_MT5_CTP_Order_.time/1000;
         break;
      case POSITION_TIME_MSC:
         long_var = (long)_MT5_CTP_Order_.time;
         break;
      case POSITION_TIME_UPDATE:
         long_var = (datetime)_MT5_CTP_Order_.time/1000;
         break;
      case POSITION_TIME_UPDATE_MSC:
         long_var = (long)_MT5_CTP_Order_.time;
         break;
      case POSITION_TYPE:
         long_var = _MT5_CTP_Order_.type;
         break;
      case POSITION_MAGIC:
         long_var = _MT5_CTP_Order_.magic;
         break;
      case POSITION_IDENTIFIER:
         long_var = (long)_MT5_CTP_Order_.ticket;
         break;
      case POSITION_REASON:
         long_var = POSITION_REASON_EXPERT;
         break;
      default:
         break;
     }
   return(long_var);
  }

bool PositionGetInteger(ENUM_POSITION_PROPERTY_INTEGER  property_id,long& long_var)
  {
   long_var = PositionGetInteger(property_id);
   return(long_var!=WRONG_VALUE);
  }
//函数返回持仓属性:String
string PositionGetString(ENUM_POSITION_PROPERTY_STRING  property_id)
  {
   string string_var;
   switch(property_id)
     {
      case POSITION_SYMBOL:
         string_var = ::CharArrayToString(_MT5_CTP_Order_.symbol);
         break;
      case POSITION_COMMENT:
         string_var = ::CharArrayToString(_MT5_CTP_Order_.comment);
         break;
      case POSITION_EXTERNAL_ID:
         string_var = ::CharArrayToString(_MT5_CTP_Order_.ordersysid);
         break;
      default:
         break;
     }
   return(string_var);
  }

bool PositionGetString(ENUM_POSITION_PROPERTY_STRING  property_id,string& string_var)
  {
   string_var = PositionGetString(property_id);
   return(::StringLen(string_var)>0);
  }
//函数返回持仓属性:Double
double PositionGetDouble(ENUM_POSITION_PROPERTY_DOUBLE  property_id)
  {
   double double_var = WRONG_VALUE;
   string _symbol_;
   switch(property_id)
     {
      case POSITION_VOLUME:
         double_var = _MT5_CTP_Order_.volume;
         break;
      case POSITION_PRICE_OPEN:
         double_var = _MT5_CTP_Order_.price;
         break;
      case POSITION_SL:
         double_var = _MT5_CTP_Order_.sl;
         break;
      case POSITION_TP:
         double_var = _MT5_CTP_Order_.tp;
         break;
      case POSITION_PRICE_CURRENT:
         _symbol_ = PositionGetString(POSITION_SYMBOL);
         double_var = _MT5_CTP_Order_.type==0?::SymbolInfoDouble(_symbol_,SYMBOL_BID):(::SymbolInfoDouble(_symbol_,SYMBOL_ASK));
         break;
      case POSITION_SWAP:
         double_var = 0;
         break;
      case POSITION_PROFIT:
         double_var = _MT5_CTP_Order_.profit;
         break;
      default:
         break;
     }
   return double_var;
  }
bool PositionGetDouble(ENUM_POSITION_PROPERTY_DOUBLE  property_id,double& double_var)
  {
   double_var = PositionGetDouble(property_id);
   return((long)double_var!=WRONG_VALUE);
  }
//返回当前工作订单数
int OrdersTotal(void)
  {
   return(mt5ctp::MT5OrdersTotal());
  }
//返回订单 ticket
ulong OrderGetTicket(int index)
  {
   ulong ticket = 0;
   mt5ctp::MT5OrderGetTicket(index,ticket);
   return(ticket);
  }
// 选择工作订单
bool OrderSelect(ulong ticket)
  {
   ::ZeroMemory(_MT5_CTP_Order_);
   return(mt5ctp::MT5OrderSelect(ticket,_MT5_CTP_Order_));
  }
// 删除工作订单记录
bool OrderDelete(ulong ticket)
  {
   return(mt5ctp::MT5OrderDelete(ticket));
  }
//返回请求的订单属性：Integer
long OrderGetInteger(ENUM_ORDER_PROPERTY_INTEGER  property_id)
  {
   long long_var = WRONG_VALUE;
   switch(property_id)
     {
      case ORDER_TICKET:
         long_var = (long)_MT5_CTP_Order_.ticket;
         break;
      case ORDER_TIME_SETUP:
         long_var = (datetime)_MT5_CTP_Order_.time/1000;
         break;
      case ORDER_TIME_SETUP_MSC:
         long_var = (long)_MT5_CTP_Order_.time;
         break;
      case ORDER_TIME_DONE:
         long_var = (datetime)_MT5_CTP_Order_.time/1000;
         break;
      case ORDER_TIME_DONE_MSC:
         long_var = (long)_MT5_CTP_Order_.time;
         break;
      case ORDER_TIME_EXPIRATION:
         long_var = (datetime)_MT5_CTP_Order_.time/1000;
         break;
      case ORDER_TYPE:
         long_var = _MT5_CTP_Order_.type;
         break;
      case ORDER_STATE:
         long_var = ORDER_STATE_PLACED;
         break;
      case ORDER_TYPE_FILLING:
         long_var = ORDER_FILLING_RETURN;
         break;
      case ORDER_TYPE_TIME:
         long_var = ORDER_TIME_DAY;
         break;
      case ORDER_MAGIC:
         long_var = _MT5_CTP_Order_.magic;
         break;
      case ORDER_REASON:
         long_var = ORDER_REASON_EXPERT;
         break;
      case ORDER_POSITION_ID:
         long_var = (long)_MT5_CTP_Order_.position;
         break;
      case ORDER_POSITION_BY_ID:
         long_var = (long)_MT5_CTP_Order_.position;
         break;
      default:
         break;
     }
   return(long_var);
  }
bool OrderGetInteger(ENUM_ORDER_PROPERTY_INTEGER  property_id,long& long_var)
  {
   long_var = OrderGetInteger(property_id);
   return(long_var!=WRONG_VALUE);
  }
//返回请求的订单属性：String
string OrderGetString(ENUM_ORDER_PROPERTY_STRING  property_id)
  {
   string string_var;
   switch(property_id)
     {
      case ORDER_SYMBOL:
         string_var = ::CharArrayToString(_MT5_CTP_Order_.symbol);
         break;
      case ORDER_COMMENT:
         string_var = ::CharArrayToString(_MT5_CTP_Order_.comment);
         break;
      default:
         break;
     }
   return(string_var);
  }

bool OrderGetString(ENUM_ORDER_PROPERTY_STRING  property_id,string& string_var)
  {
   string_var = OrderGetString(property_id);
   return(::StringLen(string_var)>0);
  }
//返回请求的订单属性：Double
double OrderGetDouble(ENUM_ORDER_PROPERTY_DOUBLE  property_id)
  {
   double double_var = WRONG_VALUE;
   string _symbol_;
   switch(property_id)
     {
      case ORDER_VOLUME_INITIAL:
         double_var = _MT5_CTP_Order_.volume;
         break;
      case ORDER_VOLUME_CURRENT:
         double_var = _MT5_CTP_Order_.volume;
         break;
      case ORDER_PRICE_OPEN:
         double_var = _MT5_CTP_Order_.price;
         break;
      case ORDER_SL:
         double_var = _MT5_CTP_Order_.sl;
         break;
      case ORDER_TP:
         double_var = _MT5_CTP_Order_.tp;
         break;
      case ORDER_PRICE_CURRENT:
         _symbol_ = OrderGetString(ORDER_SYMBOL);
         double_var = _MT5_CTP_Order_.type==0?::SymbolInfoDouble(_symbol_,SYMBOL_BID):(::SymbolInfoDouble(_symbol_,SYMBOL_ASK));
         break;
      case ORDER_PRICE_STOPLIMIT:
         break;
      default:
         break;
     }
   return(double_var);
  }
bool OrderGetDouble(ENUM_ORDER_PROPERTY_DOUBLE property_id,double& double_var)
  {
   double_var = OrderGetDouble(property_id);
   return((long)double_var==WRONG_VALUE);
  }
// 报单函数
bool OrderSend(MqlTradeRequest& request,MqlTradeResult& result)
  {
   CTPTradeRequest ctp_request = {0};
   CTPTradeResult ctp_result = {0};
//-----------------------------------------------------
   ::StringToCharArray(request.symbol,ctp_request.symbol);
   ::StringToCharArray(request.comment,ctp_request.comment);
   ctp_request.action = int(request.action);
   ctp_request.magic = long(request.magic);
   ctp_request.order = long(request.order);
   ctp_request.volume = request.volume;
   ctp_request.price = request.price;
   ctp_request.stoplimit = request.stoplimit;
   ctp_request.sl = request.sl;
   ctp_request.tp = request.tp;
   ctp_request.deviation = long(request.deviation);
   ctp_request.type = int(request.type);
   ctp_request.type_filling = int(request.type_filling);
   ctp_request.type_time = int(request.type_time);
   ctp_request.expiration = long(request.expiration);
   ctp_request.position = long(request.position);
   ctp_request.position_by = long(request.position_by);
//-----------------------------------------------------
   bool bool_var = mt5ctp::OrderSend(ctp_request,ctp_result);
//-----------------------------------------------------
   request.comment = ::CharArrayToString(ctp_request.comment);
   result.retcode = uint(ctp_result.retcode);
   result.deal = ulong(ctp_result.deal);
   result.order = ulong(ctp_result.order);
   result.volume = ctp_result.volume;
   result.price = ctp_result.price;
   result.bid = ctp_result.bid;
   result.ask = ctp_result.ask;
   result.comment = ::CharArrayToString(ctp_result.comment);
   result.request_id = uint(ctp_result.request_id);
   result.retcode_external = ctp_result.retcode_external;
//-----------------------------------------------------
   return bool_var;
  }
//+------------------------------------------------------------------+
}; // end namespace MT5
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
#import "mt5ctp.dll"
bool SymbolInfoString(const string this_symbol,CTP::ENUM_SYMBOL_INFO_STRING prop_id,string& string_var);
bool SymbolInfoDouble(const string this_symbol,CTP::ENUM_SYMBOL_INFO_DOUBLE prop_id,double& double_var);
bool SymbolInfoInteger(const string this_symbol,CTP::ENUM_SYMBOL_INFO_INTEGER prop_id,long& long_var);
//+------------------------------------------------------------------+
bool AccountInfoString(CTP::ENUM_ACCOUNT_INFO_STRING prop_id,string& string_var);
bool AccountInfoDouble(CTP::ENUM_ACCOUNT_INFO_DOUBLE prop_id,double& double_var);
bool AccountInfoInteger(CTP::ENUM_ACCOUNT_INFO_INTEGER prop_id,long& long_var);
//+------------------------------------------------------------------+
int  PositionsTotal(void);
bool PositionSelect(int index,CTPPositions &pos);
bool PositionSelectByTicket(const string ticket,CTPPositions &pos);
bool PositionGetTicket(int index, string& ticket);
//+------------------------------------------------------------------+
int  MT5PositionsTotal(void);
bool MT5PositionSelectByTicket(const ulong ticket,MT5CTPOrders &pos);
bool MT5PositionGetTicket(int index, ulong& ticket);
bool MT5PositionDelete(ulong ticket);
//+------------------------------------------------------------------+
int  OrdersTotal(void);
bool OrderSelect(int index,CTPOrders &order);
bool OrderSelectByTicket(const string ticket,CTPOrders &order);
bool OrderGetTicket(int index, string& ticket);
//+------------------------------------------------------------------+
int  MT5OrdersTotal(void);
bool MT5OrderSelect(const ulong ticket,MT5CTPOrders &pos);
bool MT5OrderGetTicket(int index, ulong& ticket);
bool MT5OrderDelete(ulong ticket);
//+------------------------------------------------------------------+
int  HistoryOrdersTotal(void);
bool HistoryOrderSelect(int index,CTPHistoryOrders &history_order);
bool HistoryOrderSelectByTicket(const string ticket,CTPHistoryOrders &history_order);
bool HistoryOrderGetTicket(int index, string& ticket);
//+------------------------------------------------------------------+
int  HistoryDealsTotal(void);
bool HistoryDealSelect(int index,CTPHistoryDeals &history_deal);
bool HistoryDealSelectByTicket(const string ticket,CTPHistoryDeals &history_deal);
bool HistoryDealGetTicket(int index, string& ticket);
//+------------------------------------------------------------------+
bool OrderCheck(CTPTradeRequest& request,CTPTradeCheckResult& result);
bool OrderSend(CTPTradeRequest& request,CTPTradeResult& result);
#import
//+------------------------------------------------------------------+