//+------------------------------------------------------------------+
//|                                                 PositionInfo.mqh |
//+------------------------------------------------------------------+
#property copyright     "Copyright 2020,Guorui Holding (Shandong) Co.,Ltd."
#property version       "1.50"
#property link          ""
#property description   "本程序仅用于量化投资爱好者测试、研究。程序使用者承诺："
#property description   "1.本人/本单位熟知本程序的适用范围，自愿承担因商业应用造成的违法、违规、侵权责任。"
#property description   "2.本人/本单位熟知金融衍生品的各种交易及代理风险，并自愿承担损失及违法、违规、违约责任。"
#property description   "3.本人/本单位熟知计算机程序的各种固有风险，并自愿承担损失及违法、违规、违约责任。"
#property description   "4.本人/本单位熟知金融监管规则，依法依规参与金融市场，并自愿承担损失及违法、违规责任。"
#property description   "5.本人/本单位认可开始使用本程序，视为同意并做出上述承诺，停用本程序后，上述承诺依然有效。"
//+------------------------------------------------------------------+
//| include 外部依赖                                                 |
//+------------------------------------------------------------------+
#include <Object.mqh>
#include "mt5ctp.mqh"
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
class CPositionInfo : public CObject
  {
protected:
   CTPPositions      m_position;          // 选中的持仓数据
   string            m_ticket;            // 选中的持仓数据key
public:
                     CPositionInfo(void);
                    ~CPositionInfo(void);
   //--- fast access methods to the string position propertyes
   // 持仓编码（mt5ctp系统编码）
   string            Ticket(void) const { return(m_ticket); }
   // 合约代码
   string            Symbol(void) const;
   // 经纪公司代码
   string            BrokerID(void) const;
   // 投资者代码
   string            InvestorID(void) const;
   // 交易日
   string            TradingDay(void) const;
   // 交易所代码
   string            ExchangeID(void) const;
   // 投资单元代码
   string            InvestUnitID(void) const;
   //--- fast access methods to the double position propertyes
   // 开仓冻结金额/多
   double            LongFrozenAmount(void) const;
   // 开仓冻结金额/空
   double            ShortFrozenAmount(void) const;
   // 开仓金额
   double            OpenAmount(void) const;
   // 平仓金额
   double            CloseAmount(void) const;
   // 持仓均价
   double            Price(void) const;
   // 持仓成本
   double            PositionCost(void) const;
   // 昨结算保证金
   double            PreMargin(void) const;
   // 保证金
   double            Margin(void) const;
   // 冻结的保证金
   double            FrozenMargin(void) const;
   // 冻结的资金
   double            FrozenCash(void) const;
   // 冻结的手续费
   double            FrozenCommission(void) const;
   // 资金差额
   double            CashIn(void) const;
   // 手续费
   double            Commission(void) const;
   // 平仓盈亏
   double            CloseProfit(void) const;
   // 持仓盈亏
   double            PositionProfit(void) const;
   // 昨结算价
   double            PreSettlementPrice(void) const;
   // 结算价
   double            SettlementPrice(void) const;
   // 开仓成本
   double            OpenCost(void) const;
   // 开仓成本
   double            OpenProfit(void) const;
   // 逐日盯市平仓盈亏
   double            CloseProfitByDate(void) const;
   // 逐笔对冲平仓盈亏
   double            CloseProfitByTrade(void) const;
   // 保证金率
   double            MarginRateByMoney(void) const;
   // 保证金率(按手数)
   double            MarginRateByVolume(void) const;
   // 执行冻结金额
   double            StrikeFrozenAmount(void) const;
   // 大商所持仓成本差值
   double            PositionCostOffset(void) const;
   // 止损价
   double            StopLoss(void) const;
   // 止赢价
   double            TakeProfit(void) const;
   // tas持仓成本
   double            TasPositionCost(void) const;
   //--- fast access methods to the integer position propertyes
   // 持仓总数
   int               ToTal(void) const;
   // 持仓序号/用于持仓操作
   int               Index(void) const;
   // 持仓多空方向
   ENUM_POSITION_TYPE   PositionType(void) const;
   char              Direction(void) const;
   string            DirectionDescription(void) const;
   // 投机套保标志
   char              Hedge(void) const;
   string            HedgeDescription(void) const;
   // 持仓日期类型
   char              PositionDate(void) const;
   string            PositionDateDescription(void) const;
   // 昨仓数量
   long              YdPosition(void) const;
   // 持仓数量
   long              Position(void) const;
   // 多头冻结数量
   long              LongFrozen(void) const;
   // 空头冻结数量
   long              ShortFrozen(void) const;
   // 开仓量
   long              OpenVolume(void) const;
   // 平仓量
   long              CloseVolume(void) const;
   // 结算编号
   long              SettlementID(void) const;
   // 组合持仓数量
   long              CombPosition(void) const;
   // 组合多头冻结数量
   long              CombLongFrozen(void) const;
   // 组合空头冻结数量
   long              CombShortFrozen(void) const;
   // 今日持仓数量
   long              TodayPosition(void) const;
   // 执行冻结数量
   long              StrikeFrozen(void) const;
   // 放弃执行冻结数量
   long              AbandonFrozen(void) const;
   // 执行冻结的昨仓数量
   long              YdStrikeFrozen(void) const;
   // tas持仓手数
   long              TasPosition(void) const;
   //--- access methods to the API MQL5 functions
   bool              InfoInteger(const CTP::ENUM_POSITION_PROPERTY_INTEGER prop_id,long &var) const;
   bool              InfoDouble(const CTP::ENUM_POSITION_PROPERTY_DOUBLE prop_id,double &var) const;
   bool              InfoString(const CTP::ENUM_POSITION_PROPERTY_STRING prop_id,string &var) const;
   //--- method for select position
   bool              Select(const string ticket);
   bool              SelectByIndex(const int index);
  };
//+------------------------------------------------------------------+
//| Constructor                                                      |
//+------------------------------------------------------------------+
CPositionInfo::CPositionInfo(void)
  {
   ::ZeroMemory(m_position);
  }
//+------------------------------------------------------------------+
//| Destructor                                                       |
//+------------------------------------------------------------------+
CPositionInfo::~CPositionInfo(void)
  {
  }
//+------------------------------------------------------------------+
//| fast access methods to the string position propertyes            |
//+------------------------------------------------------------------+
// 合约代码
string CPositionInfo::Symbol(void) const
  {
   return(::CharArrayToString(m_position.InstrumentID));
  }
// 经纪公司代码
string CPositionInfo::BrokerID(void) const
  {
   return(::CharArrayToString(m_position.BrokerID));
  }
// 投资者代码
string CPositionInfo::InvestorID(void) const
  {
   return(::CharArrayToString(m_position.InvestorID));
  }
// 交易日
string CPositionInfo::TradingDay(void) const
  {
   return(::CharArrayToString(m_position.TradingDay));
  }
// 交易所代码
string CPositionInfo::ExchangeID(void) const
  {
   return(::CharArrayToString(m_position.ExchangeID));
  }
// 投资单元代码
string CPositionInfo::InvestUnitID(void) const
  {
   return(::CharArrayToString(m_position.InvestUnitID));
  }
//+------------------------------------------------------------------+
//| fast access methods to the double position propertyes            |
//+------------------------------------------------------------------+
// 开仓冻结金额/多
double CPositionInfo::LongFrozenAmount(void) const
  {
   return(m_position.LongFrozenAmount);
  }
// 开仓冻结金额/空
double CPositionInfo::ShortFrozenAmount(void) const
  {
   return(m_position.ShortFrozenAmount);
  }
// 开仓金额
double CPositionInfo::OpenAmount(void) const
  {
   return(m_position.OpenAmount);
  }
// 平仓金额
double CPositionInfo::CloseAmount(void) const
  {
   return(m_position.CloseAmount);
  }
// 持仓均价
double CPositionInfo::Price(void) const
  {
   double pos_price = -1;
   if(m_position.Position<=0)
     {
      pos_price = 0;
      return(pos_price);
     }
   bool is_custom;
   string pos_symbol = ::CharArrayToString(m_position.InstrumentID);
   ::SymbolExist(pos_symbol,is_custom);
   if(is_custom)
     {
      long contract_size = (long)::SymbolInfoDouble(pos_symbol,SYMBOL_TRADE_CONTRACT_SIZE);
      if(contract_size<=0)
        {
         contract_size = 1;
        }
      pos_price = m_position.PositionCost/m_position.Position/contract_size;
     }
   return(pos_price);
  }
// 持仓成本
double CPositionInfo::PositionCost(void) const
  {
   return(m_position.PositionCost);
  }
// 昨结算保证金
double CPositionInfo::PreMargin(void) const
  {
   return(m_position.PreMargin);
  }
// 保证金
double CPositionInfo::Margin(void) const
  {
   return(m_position.UseMargin);
  }
// 冻结的保证金
double CPositionInfo::FrozenMargin(void) const
  {
   return(m_position.FrozenMargin);
  }
// 冻结的资金
double CPositionInfo::FrozenCash(void) const
  {
   return(m_position.FrozenCash);
  }
// 冻结的手续费
double CPositionInfo::FrozenCommission(void) const
  {
   return(m_position.FrozenCommission);
  }
// 资金差额
double CPositionInfo::CashIn(void) const
  {
   return(m_position.CashIn);
  }
// 手续费
double CPositionInfo::Commission(void) const
  {
   return(m_position.Commission);
  }
// 平仓盈亏
double CPositionInfo::CloseProfit(void) const
  {
   return(m_position.CloseProfit);
  }
// 持仓盈亏
double CPositionInfo::PositionProfit(void) const
  {
   return(m_position.PositionProfit);
  }
// 昨结算价
double CPositionInfo::PreSettlementPrice(void) const
  {
   return(m_position.PreSettlementPrice);
  }
// 结算价
double CPositionInfo::SettlementPrice(void) const
  {
   return(m_position.SettlementPrice);
  }
// 开仓成本
double CPositionInfo::OpenCost(void) const
  {
   return(m_position.OpenCost);
  }
// 浮动盈亏
double CPositionInfo::OpenProfit(void) const
  {
   return(m_position.OpenProfit);
  }
// 逐日盯市平仓盈亏
double CPositionInfo::CloseProfitByDate(void) const
  {
   return(m_position.CloseProfitByDate);
  }
// 逐笔对冲平仓盈亏
double CPositionInfo::CloseProfitByTrade(void) const
  {
   return(m_position.CloseProfitByTrade);
  }
// 保证金率
double CPositionInfo::MarginRateByMoney(void) const
  {
   return(m_position.MarginRateByMoney);
  }
// 保证金率(按手数)
double CPositionInfo::MarginRateByVolume(void) const
  {
   return(m_position.MarginRateByVolume);
  }
// 执行冻结金额
double CPositionInfo::StrikeFrozenAmount(void) const
  {
   return(m_position.StrikeFrozenAmount);
  }
// 大商所持仓成本差值
double CPositionInfo::PositionCostOffset(void) const
  {
   return(m_position.PositionCostOffset);
  }
// 止损价
double CPositionInfo::StopLoss(void) const
  {
   return(m_position.StopLoss);
  }
// 止赢价
double CPositionInfo::TakeProfit(void) const
  {
   return(m_position.TakeProfit);
  }
// tas持仓成本
double CPositionInfo::TasPositionCost(void) const
  {
   return(m_position.TasPositionCost);
  }
//+------------------------------------------------------------------+
//| fast access methods to the integer position propertyes           |
//+------------------------------------------------------------------+
// 持仓总数
int CPositionInfo::ToTal(void) const
  {
   return(mt5ctp::PositionsTotal());
  }
// 持仓序号/用于持仓操作
int CPositionInfo::Index(void) const
  {
   return(CTP::PositionGetIndex(m_ticket));
  }
// 持仓多空方向
ENUM_POSITION_TYPE CPositionInfo::PositionType(void) const
  {
   return((m_position.PosiDirection=='2')?POSITION_TYPE_BUY:POSITION_TYPE_SELL);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
char CPositionInfo::Direction(void) const
  {
   return(m_position.PosiDirection);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CPositionInfo::DirectionDescription(void) const
  {
   string str_res;
   switch(m_position.PosiDirection)
     {
      case '1':
         str_res = "THOST_FTDC_PD_Net:净头寸";
         break;
      case '2':
         str_res = "THOST_FTDC_PD_Long:多头";
         break;
      case '3':
         str_res = "THOST_FTDC_PD_Short:空头";
         break;
      default:
         str_res = "Unknown Position Direction";
         break;
     }
   return(str_res);
  }
// 投机套保标志
char CPositionInfo::Hedge(void) const
  {
   return(m_position.HedgeFlag);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CPositionInfo::HedgeDescription(void) const
  {
   string str_res;
   switch(m_position.HedgeFlag)
     {
      case '1':
         str_res = "THOST_FTDC_HF_Speculation:投机";
         break;
      case '2':
         str_res = "THOST_FTDC_HF_Arbitrage:套利";
         break;
      case '3':
         str_res = "THOST_FTDC_HF_Hedge:套保";
         break;
      case '5':
         str_res = "THOST_FTDC_HF_MarketMaker:做市商";
         break;
      case '6':
         str_res = "THOST_FTDC_HF_SpecHedge:第一腿投机第二腿套保(大商所专用)";
         break;
      case '7':
         str_res = "THOST_FTDC_HF_HedgeSpec:第一腿套保第二腿投机(大商所专用)";
         break;
      default:
         str_res = "Unknown HedgeFlag";
         break;
     }
   return(str_res);
  }
// 持仓日期类型
char CPositionInfo::PositionDate(void) const
  {
   return(m_position.PositionDate);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CPositionInfo::PositionDateDescription(void) const
  {
   string str_res;
   switch(m_position.PositionDate)
     {
      case '1':
         str_res = "THOST_FTDC_PSD_Today:今日持仓";
         break;
      case '2':
         str_res = "THOST_FTDC_PSD_History:历史持仓";
         break;
      default:
         str_res = "Unknown Position DateType";
         break;
     }
   return(str_res);
  }
// 昨仓数量
long CPositionInfo::YdPosition(void) const
  {
   return(m_position.YdPosition);
  }
// 持仓数量
long CPositionInfo::Position(void) const
  {
   return(m_position.Position);
  }
// 多头冻结数量
long CPositionInfo::LongFrozen(void) const
  {
   return(m_position.LongFrozen);
  }
// 空头冻结数量
long CPositionInfo::ShortFrozen(void) const
  {
   return(m_position.ShortFrozen);
  }
// 开仓量
long CPositionInfo::OpenVolume(void) const
  {
   return(m_position.OpenVolume);
  }
// 平仓量
long CPositionInfo::CloseVolume(void) const
  {
   return(m_position.CloseVolume);
  }
// 结算编号
long CPositionInfo::SettlementID(void) const
  {
   return(m_position.SettlementID);
  }
// 组合持仓数量
long CPositionInfo::CombPosition(void) const
  {
   return(m_position.CombPosition);
  }
// 组合多头冻结数量
long CPositionInfo::CombLongFrozen(void) const
  {
   return(m_position.CombLongFrozen);
  }
// 组合空头冻结数量
long CPositionInfo::CombShortFrozen(void) const
  {
   return(m_position.CombShortFrozen);
  }
// 今日持仓数量
long CPositionInfo::TodayPosition(void) const
  {
   return(m_position.TodayPosition);
  }
// 执行冻结数量
long CPositionInfo::StrikeFrozen(void) const
  {
   return(m_position.StrikeFrozen);
  }
// 放弃执行冻结数量
long CPositionInfo::AbandonFrozen(void) const
  {
   return(m_position.AbandonFrozen);
  }
// 执行冻结的昨仓数量
long CPositionInfo::YdStrikeFrozen(void) const
  {
   return(m_position.YdStrikeFrozen);
  }
// tas持仓手数
long CPositionInfo::TasPosition(void) const
  {
   return(m_position.TasPosition);
  }
//+------------------------------------------------------------------+
//| Access functions PositionInfoInteger(...)                        |
//+------------------------------------------------------------------+
bool CPositionInfo::InfoInteger(const CTP::ENUM_POSITION_PROPERTY_INTEGER prop_id,long &var) const
  {
   bool bool_res = false;
   switch(prop_id)
     {
      case CTP::POSITION_PosiDirection:
         var = (long)m_position.PosiDirection;
         bool_res = true;
         break;
      case CTP::POSITION_HedgeFlag:
         var = (long)m_position.HedgeFlag;
         bool_res = true;
         break;
      case CTP::POSITION_PositionDate:
         var = (long)m_position.PositionDate;
         bool_res = true;
         break;
      case CTP::POSITION_YdPosition:
         var = (long)m_position.YdPosition;
         bool_res = true;
         break;
      case CTP::POSITION_Position:
         var = (long)m_position.Position;
         bool_res = true;
         break;
      case CTP::POSITION_LongFrozen:
         var = (long)m_position.LongFrozen;
         bool_res = true;
         break;
      case CTP::POSITION_ShortFrozen:
         var = (long)m_position.ShortFrozen;
         bool_res = true;
         break;
      case CTP::POSITION_OpenVolume:
         var = (long)m_position.OpenVolume;
         bool_res = true;
         break;
      case CTP::POSITION_CloseVolume:
         var = (long)m_position.CloseVolume;
         bool_res = true;
         break;
      case CTP::POSITION_SettlementID:
         var = (long)m_position.SettlementID;
         bool_res = true;
         break;
      case CTP::POSITION_CombPosition:
         var = (long)m_position.CombPosition;
         bool_res = true;
         break;
      case CTP::POSITION_CombLongFrozen:
         var = (long)m_position.CombLongFrozen;
         bool_res = true;
         break;
      case CTP::POSITION_CombShortFrozen:
         var = (long)m_position.CombShortFrozen;
         bool_res = true;
         break;
      case CTP::POSITION_TodayPosition:
         var = (long)m_position.TodayPosition;
         bool_res = true;
         break;
      case CTP::POSITION_StrikeFrozen:
         var = (long)m_position.StrikeFrozen;
         bool_res = true;
         break;
      case CTP::POSITION_AbandonFrozen:
         var = (long)m_position.AbandonFrozen;
         bool_res = true;
         break;
      case CTP::POSITION_YdStrikeFrozen:
         var = (long)m_position.YdStrikeFrozen;
         bool_res = true;
         break;
      case CTP::POSITION_TasPosition:
         var = (long)m_position.YdStrikeFrozen;
         bool_res = true;
         break;
      default:
         break;
     }
   return(bool_res);
  }
//+------------------------------------------------------------------+
//| Access functions PositionInfoInteger(...)                        |
//+------------------------------------------------------------------+
bool CPositionInfo::InfoDouble(const CTP::ENUM_POSITION_PROPERTY_DOUBLE prop_id,double &var) const
  {
   bool bool_res = false;
   switch(prop_id)
     {
      case CTP::POSITION_LongFrozenAmount:
         var = m_position.LongFrozenAmount;
         bool_res = true;
         break;
      case CTP::POSITION_ShortFrozenAmount:
         var = m_position.ShortFrozenAmount;
         bool_res = true;
         break;
      case CTP::POSITION_OpenAmount:
         var = m_position.OpenAmount;
         bool_res = true;
         break;
      case CTP::POSITION_CloseAmount:
         var = m_position.CloseAmount;
         bool_res = true;
         break;
      case CTP::POSITION_PositionCost:
         var = m_position.PositionCost;
         bool_res = true;
         break;
      case CTP::POSITION_PreMargin:
         var = m_position.PreMargin;
         bool_res = true;
         break;
      case CTP::POSITION_UseMargin:
         var = m_position.UseMargin;
         bool_res = true;
         break;
      case CTP::POSITION_FrozenMargin:
         var = m_position.FrozenMargin;
         bool_res = true;
         break;
      case CTP::POSITION_FrozenCash:
         var = m_position.FrozenCash;
         bool_res = true;
         break;
      case CTP::POSITION_FrozenCommission:
         var = m_position.FrozenCommission;
         bool_res = true;
         break;
      case CTP::POSITION_CashIn:
         var = m_position.CashIn;
         bool_res = true;
         break;
      case CTP::POSITION_Commission:
         var = m_position.Commission;
         bool_res = true;
         break;
      case CTP::POSITION_CloseProfit:
         var = m_position.CloseProfit;
         bool_res = true;
         break;
      case CTP::POSITION_PositionProfit:
         var = m_position.PositionProfit;
         bool_res = true;
         break;
      case CTP::POSITION_PreSettlementPrice:
         var = m_position.PreSettlementPrice;
         bool_res = true;
         break;
      case CTP::POSITION_SettlementPrice:
         var = m_position.SettlementPrice;
         bool_res = true;
         break;
      case CTP::POSITION_OpenCost:
         var = m_position.OpenCost;
         bool_res = true;
         break;
      case CTP::POSITION_ExchangeMargin:
         var = m_position.ExchangeMargin;
         bool_res = true;
         break;
      case CTP::POSITION_CloseProfitByDate:
         var = m_position.CloseProfitByDate;
         bool_res = true;
         break;
      case CTP::POSITION_CloseProfitByTrade:
         var = m_position.CloseProfitByTrade;
         bool_res = true;
         break;
      case CTP::POSITION_MarginRateByMoney:
         var = m_position.MarginRateByMoney;
         bool_res = true;
         break;
      case CTP::POSITION_MarginRateByVolume:
         var = m_position.MarginRateByVolume;
         bool_res = true;
         break;
      case CTP::POSITION_StrikeFrozenAmount:
         var = m_position.StrikeFrozenAmount;
         bool_res = true;
         break;
      case CTP::POSITION_PositionCostOffset:
         var = m_position.PositionCostOffset;
         bool_res = true;
         break;
      case CTP::POSITION_StopLoss:
         var = m_position.StopLoss;
         bool_res = true;
         break;
      case CTP::POSITION_TakeProfit:
         var = m_position.TakeProfit;
         bool_res = true;
         break;
      case CTP::POSITION_TasPositionCost:
         var = m_position.TasPositionCost;
         bool_res = true;
         break;
      default:
         break;
     }
   return(bool_res);
  }
//+------------------------------------------------------------------+
//| Access functions PositionInfoInteger(...)                        |
//+------------------------------------------------------------------+
bool CPositionInfo::InfoString(const CTP::ENUM_POSITION_PROPERTY_STRING prop_id,string &var) const
  {
   bool bool_res = false;
   switch(prop_id)
     {
      case CTP::POSITION_InstrumentID:
         var = ::CharArrayToString(m_position.InstrumentID);
         bool_res = true;
         break;
      case CTP::POSITION_BrokerID:
         var = ::CharArrayToString(m_position.BrokerID);
         bool_res = true;
         break;
      case CTP::POSITION_InvestorID:
         var = ::CharArrayToString(m_position.InvestorID);
         bool_res = true;
         break;
      case CTP::POSITION_TradingDay:
         var = ::CharArrayToString(m_position.TradingDay);
         bool_res = true;
         break;
      case CTP::POSITION_ExchangeID:
         var = ::CharArrayToString(m_position.ExchangeID);
         bool_res = true;
         break;
      case CTP::POSITION_InvestUnitID:
         var = ::CharArrayToString(m_position.InvestUnitID);
         bool_res = true;
         break;
      default:
         break;
     }
   return(bool_res);
  }
//+------------------------------------------------------------------+
//| method for select position                                       |
//+------------------------------------------------------------------+
bool CPositionInfo::Select(const string ticket)
  {
   ::ZeroMemory(m_position);
   if(mt5ctp::PositionSelectByTicket(ticket,m_position))
     {
      m_ticket = ticket;
      return(true);
     }
   return(false);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPositionInfo::SelectByIndex(const int index)
  {
   string ticket = CTP::PositionGetTicket(index);
   return(Select(ticket));
  }
//+------------------------------------------------------------------+
