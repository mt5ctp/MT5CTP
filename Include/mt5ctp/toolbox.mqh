//+------------------------------------------------------------------+
//|                                                      toolbox.mqh |
//+------------------------------------------------------------------+
#resource "\\Images\\EasyAndFastGUI\\Icons\\bmp16\\start.bmp"
#resource "\\Images\\EasyAndFastGUI\\Icons\\bmp16\\stop.bmp"

#include <mt5ctp\Trade.mqh>
#include <EasyAndFastGUI\WndCreate.mqh>
#include <EasyAndFastGUI\TimeCounter.mqh>
//+------------------------------------------------------------------+
//| define                                                           |
//+------------------------------------------------------------------+
#ifndef __TRADE_CLASS__
#define __TRADE_CLASS__
#endif
#define APP_NAME                    ("mt5ctptools")
#define MENUBAR_TEXT                ({"开户","管理","帮助"})
#define MENUBAR_TEXT_WIDTH          (60)
#define STATUSBAR_TEXT              ({APP_NAME,"上期(SHFE)","中金(CFFEX)","大商(DEC)","郑商(CZCE)","能源(INE)"})
#define STATUSBAR_TEXT_WIDTH        (120)
#define ORDER_WIDTH                 (350)
#define TABS_WIDTH                  (60)
#define LABEL_TABS_INFO             ({"账户","CTP持仓","MT5持仓","挂单","报单","成交"})
#define LABEL_TABS_ORDER            ({"标准"})
#define HEADERS_ACCOUNTS            ({"资金账号","经纪公司","昨结算权益","动态权益","可用资金","平仓盈亏","持仓盈亏","手续费","保证金","保证金冻结","手续费冻结","入金","出金"})
#define HEADERS_POSITIONS           ({"持仓索引","资金账号","经纪公司","合约代码","日期类型","买/卖","数量","价格","保证金","止损价","止盈价","市场价","盯市盈亏","浮动盈亏"})
#define HEADERS_POSITIONS_MT5       ({"交易合约","订单号","时间","类型","交易量","价格","止损","止盈","最新价","盈利","幻数","注释"})
#define HEADERS_ORDERS              ({"报单索引","资金账号","经纪公司","合约代码","时间","买/卖","开/平","数量","剩余数量","价格","止损价","止盈价","触发价","触发条件","报单状态","报单信息"})
#define HEADERS_HISTORY_ORDERS      ({"报单索引","资金账号","经纪公司","合约代码","时间","买/卖","开/平","数量","剩余数量","价格","止损价","止盈价","触发价","触发条件","报单状态","报单信息","错误信息"})
#define HEADERS_TRADES              ({"成交索引","资金账号","经纪公司","合约代码","时间","买/卖","开/平","数量","价格"})
#define LABEL_BUTTON_ORDER          ({"买入(BUY)","卖出(SELL)"})
//+------------------------------------------------------------------
//| ToolBox
//| 函数：主要应用程序对话框                                |
//+------------------------------------------------------------------
class ToolBox : public CWndCreate
  {
private:
   bool              m_user_login;
   int               CheckTicket(CTable& table,const string ticket);
   string            m_recommender;    // 推荐人
protected:
   CWindow           m_window;
   CTimeCounter      m_timer;
   //--菜单栏
   CMenuBar          m_menu_bar;
   CContextMenu      m_submenu_0;
   CContextMenu      m_submenu_1;
   CContextMenu      m_submenu_2;
   //--状态栏
   CStatusBar        m_status_bar;
   //--tab组件
   CTabs             m_orders;
   CTabs             m_infos;
   //--信息组件
   CTable            m_account_table;
   CTable            m_position_table;
   CTable            m_position_mt;
   CTable            m_order_table;
   CTable            m_history_order_table;
   CTable            m_trade_table;
   //----下单交互信息组件
   CTextEdit         m_symbol_text;
   CListView         m_symbol_list;
   CButtonsGroup     m_Hedge_button;
   CTextEdit         m_price_text;
   CCheckBox         m_price_select0;
   CCheckBox         m_price_select1;
   CCheckBox         m_price_select2;
   CTextEdit         m_volume_text;
   CCheckBox         m_volume_select0;
   CCheckBox         m_volume_select1;
   CCheckBox         m_volume_select2;
   CTextEdit         m_stoploss_text;
   CTextEdit         m_takeprofit_text;
   //--分割线
   CSeparateLine     m_order_line;
   //--下单操作组件
   CButtonsGroup     m_openclose_button;
   CButton           m_buy_button;
   CButton           m_sell_button;
   //--选择组件
   CFrame            m_select_frame;
   CCheckBox         m_select_account;
   CCheckBox         m_select_exchange;
   CCheckBox         m_select_product;
   CCheckBox         m_select_symbol;
   CCheckBox         m_select_buysell;
   CCheckBox         m_select_magic;
   //----持仓快捷操作组件
   CButton           m_pos_add_button;
   CButton           m_pos_sub_button;
   CButton           m_pos_cls_button;
   CButton           m_pos_hedg_button;
   CButton           m_pos_back_button;
   CButton           m_pos_sync_button;
   CButton           m_pos_sltp_button;
   //----订单快捷操作组件
   CButton           m_order_modprice_button;
   CButton           m_order_modvolume_button;
   CButton           m_order_del_button;
   CButton           m_order_back_button;
   CButton           m_order_delall_button;
   CButton           m_order_sltp_button;
   //----订单处理对象
   CTrade            m_trade;
public:
                     ToolBox(void);
                    ~ToolBox(void);
   void              SetRecommender(string recommender);
   string            Recommender(void);
   void              OnInitEvent(void);
   void              OnDeinitEvent(const int reason);
   void              OnTimerEvent(void);
   virtual void      OnEvent(const int id,const long &lparam,const double &dparam,const string &sparam);
   bool              Create(const string text,const int x=0,const int y=0,const int x_size=200,const int y_size=200);
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ToolBox::ToolBox(void)
  {

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ToolBox::~ToolBox(void)
  {
  }
//+------------------------------------------------------------------+
//|设置/提取居间人ID                                                 |
//+------------------------------------------------------------------+
void ToolBox::SetRecommender(string recommender)
  {
   if(StringLen(recommender)>0)
     {
      m_recommender = recommender;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string ToolBox::Recommender(void)
  {
   return(m_recommender);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ToolBox::OnInitEvent(void)
  {
// 初始化登陆状态/客户端状态记录
   m_user_login = false;
// 设置定时器
   m_timer.SetParameters(16,320);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ToolBox::OnDeinitEvent(const int reason)
  {
   CWndEvents::Destroy();
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ToolBox::OnTimerEvent(void)
  {
   CWndEvents::OnTimerEvent();
   if(m_timer.CheckTimeCounter())
     {
      // 界面行情更新
      string symbol_select = m_symbol_text.GetValue();
      bool is_custom;
      SymbolExist(symbol_select,is_custom);
      if(is_custom)
        {
         // 设置最大最小值/行情中提取
         double min_price = CTP::SymbolInfoDouble(symbol_select,CTP::SYMBOL_LowerLimitPrice);
         if(MathAbs(m_price_text.MinValue()-min_price) > 0.0001)
           {
            m_price_text.MinValue(min_price);
           }
         if(MathAbs(m_stoploss_text.MinValue()-min_price) > 0.0001)
           {
            m_stoploss_text.MinValue(min_price);
           }
         if(MathAbs(m_takeprofit_text.MinValue()-min_price) > 0.0001)
           {
            m_takeprofit_text.MinValue(min_price);
           }
         //
         double max_price = CTP::SymbolInfoDouble(symbol_select,CTP::SYMBOL_UpperLimitPrice);
         if(MathAbs(m_price_text.MaxValue()-max_price) > 0.0001)
           {
            m_price_text.MaxValue(max_price);
           }
         if(MathAbs(m_stoploss_text.MaxValue()-max_price) > 0.0001)
           {
            m_stoploss_text.MaxValue(max_price);
           }
         if(MathAbs(m_takeprofit_text.MaxValue()-max_price) > 0.0001)
           {
            m_takeprofit_text.MaxValue(max_price);
           }
         // 设置当前价格/行情中提取
         double this_price = SymbolInfoDouble(symbol_select,SYMBOL_LAST);
         int this_digit = (int)SymbolInfoInteger(symbol_select,SYMBOL_DIGITS);
         bool check_select = m_price_select0.IsPressed()|| m_price_select1.IsPressed()|| m_price_select2.IsPressed();
         if(check_select)
           {
            m_price_text.SetValue(DoubleToString(this_price,this_digit),true);
            m_price_text.GetTextBoxPointer().Update(true);
           }
        }
      // 账户数据更新
      if(CTP::AccountExists())
        {
         m_user_login = true;
         string type_local = CTP::AccountInfoString(CTP::ACCOUNT_BrokerID)=="9999"?"demo":"real";
         string main_lab = StringFormat("%s-%s:%s",APP_NAME,type_local,CTP::AccountInfoString(CTP::ACCOUNT_AccountID));
         if(m_window.LabelText()!=main_lab)
           {
            m_window.LabelText(main_lab);
            m_window.Update(true);
           }
        }
      else
        {
         //清除数据
         if(m_user_login)
           {
            m_account_table.DeleteAllRows(true);
            m_position_table.DeleteAllRows(true);
            m_position_mt.DeleteAllRows(true);
            m_order_table.DeleteAllRows(true);
            m_history_order_table.DeleteAllRows(true);
            m_trade_table.DeleteAllRows(true);
            // 清除选择
            m_account_table.DeselectRow();
            m_position_table.DeselectRow();
            m_position_mt.DeselectRow();
            m_order_table.DeselectRow();
            m_history_order_table.DeselectRow();
            m_trade_table.DeselectRow();
            //
            m_user_login = false;
           }
         if(m_window.LabelText()!=APP_NAME)
           {
            m_window.LabelText(APP_NAME);
            m_window.Update(true);
           }
         return;
        }
      // 更新账户数据
      int row_account = CheckTicket(m_account_table,CTP::AccountInfoString(CTP::ACCOUNT_AccountID));
      if(row_account==WRONG_VALUE)
        {
         if(StringLen(m_account_table.GetValue(0,0))>0)
           {
            m_account_table.DeselectRow();
            m_account_table.AddRow(0,true);
           }
         row_account = 0;
        }
      m_account_table.SetValue(0,row_account,CTP::AccountInfoString(CTP::ACCOUNT_AccountID),0,true);    // 资金账号
      m_account_table.SetValue(1,row_account,CTP::AccountInfoString(CTP::ACCOUNT_BrokerID),0,true);     // 经纪公司
      m_account_table.SetValue(2,row_account,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_PreBalance),2),0,true);          // 上次结算准备金
      m_account_table.SetValue(3,row_account,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_Balance),2),0,true);             // 期货结算准备金
      m_account_table.SetValue(4,row_account,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_Available),2),0,true);           // 可用资金
      m_account_table.SetValue(5,row_account,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_CloseProfit),2),0,true);         // 平仓盈亏
      m_account_table.SetValue(6,row_account,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_PositionProfit),2),0,true);      // 持仓盈亏
      m_account_table.SetValue(7,row_account,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_Commission),2),0,true);          // 手续费
      m_account_table.SetValue(8,row_account,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_CurrMargin),2),0,true);          // 保证金
      m_account_table.SetValue(9,row_account,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_FrozenMargin),2),0,true);        // 保证金冻结
      m_account_table.SetValue(10,row_account,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_FrozenCommission),2),0,true);   // 手续费冻结
      m_account_table.SetValue(11,row_account,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_Deposit),2),0,true);            // 入金
      m_account_table.SetValue(12,row_account,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_Withdraw),2),0,true);           // 出金
      if(m_account_table.IsVisible())
        {
         m_account_table.Update();
        }
      // 更新持仓数据
      for(int pos_index=1; pos_index<=CTP::PositionsTotal(); pos_index++)
        {
         if(!CTP::PositionSelect(pos_index))
            continue;
         // 检查持仓数据对应的行位置
         string pos_ticket = CTP::PositionGetTicket(pos_index);
         int row_pos = CheckTicket(m_position_table,pos_ticket);
         // 数量检查
         int pos_volume = (int)CTP::PositionGetInteger(CTP::POSITION_Position);
         if(pos_volume<=0)
           {
            // 持仓为0/删除行
            if(row_pos!=WRONG_VALUE)
              {
               int check_rows = (int)m_position_table.RowsTotal();
               if(check_rows<2)
                 {
                  m_position_table.DeselectRow();
                  m_position_table.DeleteAllRows(true);
                 }
               else
                 {
                  m_position_table.DeselectRow();
                  m_position_table.DeleteRow(row_pos,true);
                 }
              }
            continue;
           }
         // 计算持仓均价
         string pos_symbol = CTP::PositionGetString(CTP::POSITION_InstrumentID);
         int digit_symbol = (int)::SymbolInfoInteger(pos_symbol,SYMBOL_DIGITS);
         double size_symbol = ::SymbolInfoDouble(pos_symbol,SYMBOL_TRADE_CONTRACT_SIZE);
         if(!(size_symbol>0))
            continue;
         double pos_price = CTP::PositionGetDouble(CTP::POSITION_PositionCost)/size_symbol/pos_volume;
         // 持仓方向/取得市场价
         char pos_direction = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection);
         double this_price = pos_direction==CTP::POSITION_TYPE_BUY?(::SymbolInfoDouble(pos_symbol,SYMBOL_BID)):(::SymbolInfoDouble(pos_symbol,SYMBOL_ASK));
         // 新增持仓
         if(row_pos==WRONG_VALUE)
           {
            if(::StringLen(m_position_table.GetValue(0,0))>0)
              {
               m_position_table.DeselectRow();
               m_position_table.AddRow(0,true);
              }
            row_pos = 0;
           }
         // 更新行数据
         m_position_table.SetValue(0,row_pos,CTP::PositionGetTicket(pos_index),0,true);                    // 持仓索引
         m_position_table.SetValue(1,row_pos,CTP::PositionGetString(CTP::POSITION_InvestorID),0,true);     // 资金账号
         m_position_table.SetValue(2,row_pos,CTP::PositionGetString(CTP::POSITION_BrokerID),0,true);       // 经纪公司
         m_position_table.SetValue(3,row_pos,pos_symbol,0,true);                                           // 合约代码
         m_position_table.SetValue(4,row_pos,::CharToString((uchar)CTP::PositionGetInteger(CTP::POSITION_PositionDate)),0,true);     // 日期类型
         m_position_table.SetValue(5,row_pos,pos_direction==CTP::POSITION_TYPE_BUY?"BUY":"SELL",0,true);                      // 买/卖
         m_position_table.SetValue(6,row_pos,::IntegerToString(pos_volume),0,true);                        // 数量
         m_position_table.SetValue(7,row_pos,::DoubleToString(pos_price,digit_symbol),0,true);               // 价格
         m_position_table.SetValue(8,row_pos,::DoubleToString(CTP::PositionGetDouble(CTP::POSITION_UseMargin),2),0,true);              // 保证金
         m_position_table.SetValue(9,row_pos,::DoubleToString(CTP::PositionGetDouble(CTP::POSITION_StopLoss),digit_symbol),0,true);    // 止损价
         m_position_table.SetValue(10,row_pos,::DoubleToString(CTP::PositionGetDouble(CTP::POSITION_TakeProfit),digit_symbol),0,true); // 止盈价
         m_position_table.SetValue(11,row_pos,::DoubleToString(this_price,digit_symbol),0,true);                                       // 市场价
         m_position_table.SetValue(12,row_pos,::DoubleToString(CTP::PositionGetDouble(CTP::POSITION_PositionProfit),2),0,true);        // 盈利
         m_position_table.SetValue(13,row_pos,::DoubleToString(CTP::PositionGetDouble(CTP::POSITION_OpenProfit),2),0,true);        // 浮动盈亏
        }
      //
      if(m_position_table.IsVisible())
        {
         m_position_table.Update();
        }
      // 循环删除持仓数据
      int pos_row = (int)m_position_mt.RowsTotal()-1;
      for(; pos_row>=0; pos_row--)
        {
         ulong pos_ticket = StringToInteger(m_position_mt.GetValue(1,pos_row));
         if(pos_ticket>0 && !MT5::PositionSelectByTicket(pos_ticket))
           {
            int check_rows = (int)m_position_mt.RowsTotal();
            if(check_rows<2)
              {
               m_position_mt.DeselectRow();
               m_position_mt.DeleteAllRows(true);
              }
            else
              {
               m_position_mt.DeselectRow();
               m_position_mt.DeleteRow(pos_row,true);
              }
           }
        }//
      // 更新持仓数据
      for(int pos_index=0; pos_index<mt5ctp::MT5PositionsTotal(); pos_index++)
        {
         ulong ticket = 0;
         mt5ctp::MT5PositionGetTicket(pos_index,ticket);
         MT5CTPOrders order_mt5 = {0};
         if(!mt5ctp::MT5PositionSelectByTicket(ticket,order_mt5))
            continue;
         // 检查持仓数据对应的行位置
         int row_pos = (int)m_position_mt.RowsTotal()-1;
         for(int row = row_pos; row >= 0; row_pos = --row)
           {
            string table_str = m_position_mt.GetValue(1,row);
            if(::StringToInteger(table_str) == ticket)
              {
               break;
              }
           }
         // 数量检查
         if(order_mt5.volume<=0)
           {
            // 持仓为0/删除行
            if(row_pos!=WRONG_VALUE)
              {
               int check_rows = (int)m_position_mt.RowsTotal();
               if(check_rows<2)
                 {
                  m_position_mt.DeselectRow();
                  m_position_mt.DeleteAllRows(true);
                 }
               else
                 {
                  m_position_mt.DeselectRow();
                  m_position_mt.DeleteRow(row_pos,true);
                 }
              }
            continue;
           }
         // 取得市场价
         string pos_symbol = ::CharArrayToString(order_mt5.symbol);
         int digit_symbol = (int)::SymbolInfoInteger(pos_symbol,SYMBOL_DIGITS);
         double this_price = order_mt5.type==0?(::SymbolInfoDouble(pos_symbol,SYMBOL_BID)):(::SymbolInfoDouble(pos_symbol,SYMBOL_ASK));
         // 新增持仓
         if(row_pos==WRONG_VALUE)
           {
            if(::StringLen(m_position_mt.GetValue(0,0))>0)
              {
               m_position_mt.DeselectRow();
               m_position_mt.AddRow(0,true);
              }
            row_pos = 0;
           }
         // 更新行数据
         m_position_mt.SetValue(0,row_pos,pos_symbol,0,true);                    // 合约代码
         m_position_mt.SetValue(1,row_pos,::IntegerToString(ticket),0,true);                    // 持仓序号
         m_position_mt.SetValue(2,row_pos,::StringFormat("%s.%03d",TimeToString((datetime)order_mt5.time/1000,TIME_DATE|TIME_SECONDS),order_mt5.time%1000),0,true);                      // 成交时间
         m_position_mt.SetValue(3,row_pos,order_mt5.type==0?"BUY":"SELL",0,true);                                           // 合约代码
         m_position_mt.SetValue(4,row_pos,::DoubleToString(order_mt5.volume,0),0,true);     // 日期类型
         m_position_mt.SetValue(5,row_pos,::DoubleToString(order_mt5.price,digit_symbol),0,true);                      // 买/卖
         m_position_mt.SetValue(6,row_pos,::DoubleToString(order_mt5.sl,digit_symbol),0,true);                        // 数量
         m_position_mt.SetValue(7,row_pos,::DoubleToString(order_mt5.tp,digit_symbol),0,true);
         m_position_mt.SetValue(8,row_pos,::DoubleToString(this_price,digit_symbol),0,true);
         m_position_mt.SetValue(9,row_pos,::DoubleToString(order_mt5.profit,2),0,true);                                       // 市场价
         m_position_mt.SetValue(10,row_pos,::IntegerToString(order_mt5.magic),0,true);        // 盈利
         m_position_mt.SetValue(11,row_pos,::CharArrayToString(order_mt5.comment),0,true);        // 浮动盈亏
        }
      //
      if(m_position_mt.IsVisible())
        {
         m_position_mt.Update();
        }
      // 更新工作订单数据/两个循环
      // 循环删除表数据
      int order_row = (int)m_order_table.RowsTotal()-1;
      for(; order_row>=0; order_row--)
        {
         string order_ticket = m_order_table.GetValue(0,order_row);
         if(::StringLen(order_ticket)>0 && !CTP::OrderSelectByTicket(order_ticket))
           {
            int check_rows = (int)m_order_table.RowsTotal();
            if(check_rows<2)
              {
               m_order_table.DeselectRow();
               m_order_table.DeleteAllRows(true);
              }
            else
              {
               m_order_table.DeselectRow();
               m_order_table.DeleteRow(order_row,true);
              }
           }
        }
      // 循环添加表数据
      for(int order_index=1; order_index<=CTP::OrdersTotal(); order_index++)
        {
         if(!CTP::OrderSelect(order_index))
            continue;
         // 检查据对应的行位置
         string order_ticket = CTP::OrderGetTicket(order_index);
         order_row = CheckTicket(m_order_table,order_ticket);
         // 报单合约
         string order_symbol = CTP::OrderGetString(CTP::ORDER_InstrumentID);
         int digit_symbol = (int)::SymbolInfoInteger(order_symbol,SYMBOL_DIGITS);
         // 报单日期时间
         string order_datetime = CTP::OrderGetString(CTP::ORDER_InsertDate);
         order_datetime += " ";
         order_datetime += CTP::OrderGetString(CTP::ORDER_InsertTime);
         datetime order_time = ::StringToTime(order_datetime);
         // 新增数据行
         if(order_row==WRONG_VALUE)
           {
            if(::StringLen(m_order_table.GetValue(0,0))>0)
              {
               m_order_table.DeselectRow();
               m_order_table.AddRow(0,true);
              }
            order_row = 0;
           }
         //添加/更新数据
         m_order_table.SetValue(0,order_row,CTP::OrderGetTicket(order_index),0,true);                  // 报单索引
         m_order_table.SetValue(1,order_row,CTP::OrderGetString(CTP::ORDER_InvestorID),0,true);        // 资金账号
         m_order_table.SetValue(2,order_row,CTP::OrderGetString(CTP::ORDER_BrokerID),0,true);          // 经纪公司
         m_order_table.SetValue(3,order_row,order_symbol,0,true);                                      // 合约代码
         m_order_table.SetValue(4,order_row,::TimeToString(order_time,TIME_DATE|TIME_SECONDS),0,true);                        // 时间
         m_order_table.SetValue(5,order_row,(char)CTP::OrderGetInteger(CTP::ORDER_Direction)==CTP::ORDER_TYPE_BUY?"BUY":"SELL",0,true);   // 买/卖
         m_order_table.SetValue(6,order_row,CTP::OrderGetString(CTP::ORDER_CombOffsetFlag),0,true);    // 开/平
         m_order_table.SetValue(7,order_row,::IntegerToString(CTP::OrderGetInteger(CTP::ORDER_VolumeTotalOriginal)),0,true);      // "数量",
         m_order_table.SetValue(8,order_row,::IntegerToString(CTP::OrderGetInteger(CTP::ORDER_VolumeTotal)),0,true);                // "剩余数量",
         m_order_table.SetValue(9,order_row,::DoubleToString(CTP::OrderGetDouble(CTP::ORDER_LimitPrice),digit_symbol),0,true);      // "价格",
         m_order_table.SetValue(10,order_row,::DoubleToString(CTP::OrderGetDouble(CTP::ORDER_StopLoss),digit_symbol),0,true);      // "止损价",
         m_order_table.SetValue(11,order_row,::DoubleToString(CTP::OrderGetDouble(CTP::ORDER_TakeProfit),digit_symbol),0,true);      // "止盈价",
         m_order_table.SetValue(12,order_row,::DoubleToString(CTP::OrderGetDouble(CTP::ORDER_StopPrice),digit_symbol),0,true);      // "触发价",
         m_order_table.SetValue(13,order_row,::CharToString((uchar)CTP::OrderGetInteger(CTP::ORDER_ContingentCondition)),0,true);      // "触发条件",
         m_order_table.SetValue(14,order_row,::CharToString((uchar)CTP::OrderGetInteger(CTP::ORDER_OrderStatus)),0,true);      // "报单状态",
         m_order_table.SetValue(15,order_row,CTP::OrderGetString(CTP::ORDER_StatusMsg),0,true);                                // "报单信息"
        }
      //
      if(m_order_table.IsVisible())
        {
         m_order_table.Update();
        }
      //---历史订单数据
      for(int order_index=1; order_index<=CTP::HistoryOrdersTotal(); order_index++)
        {
         if(!CTP::HistoryOrderSelect(order_index))
            continue;
         // 检查据对应的行位置
         string order_ticket = CTP::HistoryOrderGetTicket(order_index);
         order_row = CheckTicket(m_history_order_table,order_ticket);
         // 报单合约
         string order_symbol = CTP::HistoryOrderGetString(CTP::ORDER_InstrumentID);
         int digit_symbol = (int)::SymbolInfoInteger(order_symbol,SYMBOL_DIGITS);
         // 报单日期时间
         string order_datetime = CTP::HistoryOrderGetString(CTP::ORDER_InsertDate);
         order_datetime += " ";
         order_datetime += CTP::HistoryOrderGetString(CTP::ORDER_InsertTime);
         datetime order_time = ::StringToTime(order_datetime);
         // 新增数据行
         if(order_row==WRONG_VALUE)
           {
            if(::StringLen(m_history_order_table.GetValue(0,0))>0)
              {
               m_history_order_table.DeselectRow();
               m_history_order_table.AddRow(0,true);
              }
            order_row = 0;
           }
         if(order_row==0)
           {
            //添加/更新数据
            m_history_order_table.SetValue(0,order_row,CTP::HistoryOrderGetTicket(order_index),0,true);                  // 报单索引
            m_history_order_table.SetValue(1,order_row,CTP::HistoryOrderGetString(CTP::ORDER_InvestorID),0,true);        // 资金账号
            m_history_order_table.SetValue(2,order_row,CTP::HistoryOrderGetString(CTP::ORDER_BrokerID),0,true);          // 经纪公司
            m_history_order_table.SetValue(3,order_row,order_symbol,0,true);                                             // 合约代码
            m_history_order_table.SetValue(4,order_row,::TimeToString(order_time,TIME_DATE|TIME_SECONDS),0,true);                               // 时间
            m_history_order_table.SetValue(5,order_row,(char)CTP::HistoryOrderGetInteger(CTP::ORDER_Direction)==CTP::ORDER_TYPE_BUY?"BUY":"SELL",0,true);   // 买/卖
            m_history_order_table.SetValue(6,order_row,CTP::HistoryOrderGetString(CTP::ORDER_CombOffsetFlag),0,true);    // 开/平
            m_history_order_table.SetValue(7,order_row,::IntegerToString(CTP::HistoryOrderGetInteger(CTP::ORDER_VolumeTotalOriginal)),0,true);      // "数量",
            m_history_order_table.SetValue(8,order_row,::IntegerToString(CTP::HistoryOrderGetInteger(CTP::ORDER_VolumeTotal)),0,true);                // "剩余数量",
            m_history_order_table.SetValue(9,order_row,::DoubleToString(CTP::HistoryOrderGetDouble(CTP::ORDER_LimitPrice),digit_symbol),0,true);      // "价格",
            m_history_order_table.SetValue(10,order_row,::DoubleToString(CTP::HistoryOrderGetDouble(CTP::ORDER_StopLoss),digit_symbol),0,true);      // "止损价",
            m_history_order_table.SetValue(11,order_row,::DoubleToString(CTP::HistoryOrderGetDouble(CTP::ORDER_TakeProfit),digit_symbol),0,true);      // "止盈价",
            m_history_order_table.SetValue(12,order_row,::DoubleToString(CTP::HistoryOrderGetDouble(CTP::ORDER_StopPrice),digit_symbol),0,true);      // "触发价",
            m_history_order_table.SetValue(13,order_row,::CharToString((uchar)CTP::HistoryOrderGetInteger(CTP::ORDER_ContingentCondition)),0,true);      // "触发条件",
            m_history_order_table.SetValue(14,order_row,::CharToString((uchar)CTP::HistoryOrderGetInteger(CTP::ORDER_OrderStatus)),0,true);      // "报单状态",
            m_history_order_table.SetValue(15,order_row,CTP::HistoryOrderGetString(CTP::ORDER_StatusMsg),0,true);                                // "报单信息"
            m_history_order_table.SetValue(16,order_row,CTP::HistoryOrderGetString(CTP::ORDER_ErrorMsg),0,true);                                 // "错误信息"
           }
        }
      //
      if(m_history_order_table.IsVisible())
        {
         m_history_order_table.Update();
        }
      //--成交数据
      int deals_total = (int)CTP::HistoryDealsTotal();
      int trade_total = (int)m_trade_table.RowsTotal();
      // 新成交
      for(int deal_index=1; deal_index<=deals_total; deal_index++)
        {
         if(!CTP::HistoryDealSelect(deal_index))
            continue;
         // 检查据对应的行位置
         string deal_ticket = CTP::HistoryDealGetTicket(deal_index);
         int deal_row = CheckTicket(m_trade_table,deal_ticket);
         if(deal_row!=WRONG_VALUE)
           {
            continue;
           }
         // 合约
         string deal_symbol = CTP::HistoryDealGetString(CTP::DEAL_InstrumentID);
         int digit_symbol = (int)::SymbolInfoInteger(deal_symbol,SYMBOL_DIGITS);
         // 日期时间
         string deal_datetime = CTP::HistoryDealGetString(CTP::DEAL_TradeDate);
         deal_datetime += " ";
         deal_datetime += CTP::HistoryDealGetString(CTP::DEAL_TradeTime);
         datetime deal_time = ::StringToTime(deal_datetime);
         // 新增数据行
         if(deal_row==WRONG_VALUE)
           {
            if(::StringLen(m_trade_table.GetValue(0,0))>0)
              {
               m_trade_table.DeselectRow();
               m_trade_table.AddRow(0,true);
              }
            deal_row = 0;
           }
         if(deal_row==0)
           {
            m_trade_table.SetValue(0,deal_row,CTP::HistoryDealGetTicket(deal_index),0,true);                  //"成交索引",
            m_trade_table.SetValue(1,deal_row,CTP::HistoryDealGetString(CTP::DEAL_InvestorID),0,true);        // 资金账号
            m_trade_table.SetValue(2,deal_row,CTP::HistoryDealGetString(CTP::DEAL_BrokerID),0,true);          // 经纪公司
            m_trade_table.SetValue(3,deal_row,deal_symbol,0,true);                                            // 合约代码
            m_trade_table.SetValue(4,deal_row,::TimeToString(deal_time,TIME_DATE|TIME_SECONDS),0,true);                              // "时间",
            m_trade_table.SetValue(5,deal_row,(char)CTP::HistoryDealGetInteger(CTP::DEAL_Direction)==CTP::ORDER_TYPE_BUY?"BUY":"SELL",0,true);  // "买/卖",
            m_trade_table.SetValue(6,deal_row,::CharToString((uchar)CTP::HistoryDealGetInteger(CTP::DEAL_Direction)),0,true);             // "开/平",
            m_trade_table.SetValue(7,deal_row,::IntegerToString(CTP::HistoryDealGetInteger(CTP::DEAL_Volume)),0,true);                   // "数量",
            m_trade_table.SetValue(8,deal_row,::DoubleToString(CTP::HistoryDealGetDouble(CTP::DEAL_Price),digit_symbol),0,true);         //"价格"
           }
        }
      //
      if(m_trade_table.IsVisible())
        {
         m_trade_table.Update();
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool ToolBox::Create(const string text,const int x=0,const int y=0,const int x_size=200,const int y_size=200)
  {
   if(!CWndCreate::CreateWindow(m_window,text,x,y,x_size,y_size,true,false,false,true))
     {
      return(false);
     }
// 禁止移动
   m_window.IsMovable(false);
// 自动满屏
   m_window.AutoXResizeMode(true);
   m_window.AutoYResizeMode(true);
// 菜单
   string menu_items[] = MENUBAR_TEXT;
   int width_menu_items[ArraySize(menu_items)];
   for(int i = 0; i<ArraySize(menu_items); i++)
     {
      width_menu_items[i] = MENUBAR_TEXT_WIDTH;
     }
   if(!CWndCreate::CreateMenuBar(m_menu_bar,m_window,1,20,22,menu_items,width_menu_items))
     {
      return(false);
     }
   else
     {
      //
      m_submenu_0.MainPointer(m_window);
      m_submenu_0.PrevNodePointer(m_menu_bar.GetItemPointer(0));
      m_menu_bar.AddContextMenuPointer(0,m_submenu_0);
      m_submenu_0.XSize(160);
      m_submenu_0.FixSide(FIX_BOTTOM);
      // 添加菜单项
      m_submenu_0.AddItem("预约开户","","",MI_SIMPLE);
      m_submenu_0.AddItem("退出","","",MI_SIMPLE);
      // 添加分割线
      m_submenu_0.AddSeparateLine(0);
      if(!m_submenu_0.CreateContextMenu(3,42))
        {
         return(false);
        }
      CWndContainer::AddToElementsArray(0,m_submenu_0);
      //
      m_submenu_1.MainPointer(m_window);
      m_submenu_1.PrevNodePointer(m_menu_bar.GetItemPointer(1));
      m_menu_bar.AddContextMenuPointer(1,m_submenu_1);
      m_submenu_1.XSize(160);
      m_submenu_1.FixSide(FIX_BOTTOM);
      // 添加菜单项
      m_submenu_1.AddItem("修改交易密码","","",MI_SIMPLE);
      m_submenu_1.AddItem("修改资金密码","","",MI_SIMPLE);
      m_submenu_1.AddItem("入金","","",MI_SIMPLE);
      m_submenu_1.AddItem("出金","","",MI_SIMPLE);
      // 添加分割线
      m_submenu_1.AddSeparateLine(1);
      if(!m_submenu_1.CreateContextMenu(62,42))
        {
         return(false);
        }
      CWndContainer::AddToElementsArray(0,m_submenu_1);      //
      //
      m_submenu_2.MainPointer(m_window);
      m_submenu_2.PrevNodePointer(m_menu_bar.GetItemPointer(2));
      m_menu_bar.AddContextMenuPointer(2,m_submenu_2);
      m_submenu_2.XSize(160);
      m_submenu_2.FixSide(FIX_BOTTOM);
      // 添加菜单项
      m_submenu_2.AddItem("关于","","",MI_SIMPLE);
      if(!m_submenu_2.CreateContextMenu(122,42))
        {
         return(false);
        }
      CWndContainer::AddToElementsArray(0,m_submenu_2);
     }
// 状态栏
   string text_items[] = STATUSBAR_TEXT;
   int width_items[ArraySize(text_items)];
   for(int i = 0; i<ArraySize(text_items); i++)
     {
      if(i == 0)
        {
         width_items[i] = i;
        }
      else
        {
         width_items[i] = STATUSBAR_TEXT_WIDTH;
        }
     }
   if(!CWndCreate::CreateStatusBar(m_status_bar,m_window,1,23,22,text_items,width_items))
     {
      return(false);
     }
   else
     {
      for(int item_index = 1; item_index<ArraySize(text_items); item_index++)
        {
         m_status_bar.GetItemPointer(item_index).LabelXGap(25);
         m_status_bar.GetItemPointer(item_index).AddImagesGroup(3,0);
         m_status_bar.GetItemPointer(item_index).AddImage(0,"Images\\EasyAndFastGUI\\Icons\\bmp16\\start.bmp");
         m_status_bar.GetItemPointer(item_index).AddImage(0,"Images\\EasyAndFastGUI\\Icons\\bmp16\\stop.bmp");
        }
     }
// info tabs
   string infos_text[] = LABEL_TABS_INFO;
   int infos_width[ArraySize(infos_text)];
   for(int i = 0; i<ArraySize(infos_text); i++)
     {
      infos_width[i] = TABS_WIDTH;
     }
   if(!CWndCreate::CreateTabs(m_infos,m_window,0,2,44,0,0,infos_text,infos_width,TABS_BOTTOM,true,true,ORDER_WIDTH+5,45))
     {
      return(false);
     }
// order tabs
   string orders_text[] = LABEL_TABS_ORDER;
   int orders_width[ArraySize(orders_text)];
   for(int i = 0; i<ArraySize(orders_text); i++)
     {
      orders_width[i] = TABS_WIDTH;
     }
   if(!CWndCreate::CreateTabs(m_orders,m_window,0,m_window.XSize()-ORDER_WIDTH-3,44,ORDER_WIDTH,0,orders_text,orders_width,TABS_BOTTOM,true,true,3,45))
     {
      return(false);
     }
// 账户表
   string account_headers[] = HEADERS_ACCOUNTS;
   if(!CWndCreate::CreateTable(m_account_table,m_infos,0,m_infos,0,ArraySize(account_headers),0,account_headers,1,1,0,0,true,true))
     {
      return(false);
     }
   else
     {
      m_account_table.CellYSize(24);
      m_account_table.IsZebraFormatRows(clrWhiteSmoke);
      m_account_table.ColumnResizeMode(true);
      // 初始化/填充数据
      if(CTP::AccountExists())
        {
         if(StringLen(m_account_table.GetValue(0,0))>0)
           {
            m_account_table.AddRow(0);
           }
         m_account_table.SetValue(0,0,CTP::AccountInfoString(CTP::ACCOUNT_AccountID),0,true);    // 资金账号
         m_account_table.SetValue(1,0,CTP::AccountInfoString(CTP::ACCOUNT_BrokerID),0,true);     // 经纪公司
         m_account_table.SetValue(2,0,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_PreBalance),2),0,true);          // 上次结算准备金
         m_account_table.SetValue(3,0,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_Balance),2),0,true);             // 期货结算准备金
         m_account_table.SetValue(4,0,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_Available),2),0,true);           // 可用资金
         m_account_table.SetValue(5,0,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_CloseProfit),2),0,true);         // 平仓盈亏
         m_account_table.SetValue(6,0,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_PositionProfit),2),0,true);      // 持仓盈亏
         m_account_table.SetValue(7,0,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_Commission),2),0,true);          // 手续费
         m_account_table.SetValue(8,0,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_CurrMargin),2),0,true);          // 保证金
         m_account_table.SetValue(9,0,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_FrozenMargin),2),0,true);        // 保证金冻结
         m_account_table.SetValue(10,0,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_FrozenCommission),2),0,true);   // 手续费冻结
         m_account_table.SetValue(11,0,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_Deposit),2),0,true);            // 入金
         m_account_table.SetValue(12,0,::DoubleToString(CTP::AccountInfoDouble(CTP::ACCOUNT_Withdraw),2),0,true);           // 出金
        }
     }
// 持仓表
   string positiom_headers[] = HEADERS_POSITIONS;
   if(!CWndCreate::CreateTable(m_position_table,m_infos,0,m_infos,1,ArraySize(positiom_headers),0,positiom_headers,1,1,0,0,true,true))
     {
      return(false);
     }
   else
     {
      m_position_table.CellYSize(24);
      m_position_table.IsZebraFormatRows(clrWhiteSmoke);
      m_position_table.ColumnResizeMode(true);
      // 初始化/填充数据
      if(CTP::AccountExists())
        {
         for(int pos_index=1; pos_index<=CTP::PositionsTotal(); pos_index++)
           {
            if(!CTP::PositionSelect(pos_index))
               continue;
            // 数量检查
            int pos_volume = (int)CTP::PositionGetInteger(CTP::POSITION_Position);
            if(pos_volume<=0)
               continue;
            // 计算持仓均价
            string pos_symbol = CTP::PositionGetString(CTP::POSITION_InstrumentID);
            int digit_symbol = (int)::SymbolInfoInteger(pos_symbol,SYMBOL_DIGITS);
            double size_symbol = ::SymbolInfoDouble(pos_symbol,SYMBOL_TRADE_CONTRACT_SIZE);
            if(!(size_symbol>0))
               continue;
            double pos_price = CTP::PositionGetDouble(CTP::POSITION_PositionCost)/size_symbol/pos_volume;
            // 持仓方向/取得市场价
            char pos_direction = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection);
            double this_price = pos_direction==CTP::POSITION_TYPE_BUY?(::SymbolInfoDouble(pos_symbol,SYMBOL_BID)):(::SymbolInfoDouble(pos_symbol,SYMBOL_ASK));

            if(::StringLen(m_position_table.GetValue(0,0))>0)
              {
               m_position_table.AddRow(0);
              }
            //
            m_position_table.SetValue(0,0,CTP::PositionGetTicket(pos_index),0,true);                    // 持仓索引
            m_position_table.SetValue(1,0,CTP::PositionGetString(CTP::POSITION_InvestorID),0,true);     // 资金账号
            m_position_table.SetValue(2,0,CTP::PositionGetString(CTP::POSITION_BrokerID),0,true);       // 经纪公司
            m_position_table.SetValue(3,0,pos_symbol,0,true);                                           // 合约代码
            m_position_table.SetValue(4,0,::CharToString((uchar)CTP::PositionGetInteger(CTP::POSITION_PositionDate)),0,true);     // 日期类型
            m_position_table.SetValue(5,0,pos_direction==CTP::POSITION_TYPE_BUY?"BUY":"SELL",0,true);                      // 买/卖
            m_position_table.SetValue(6,0,::IntegerToString(pos_volume),0,true);                        // 数量
            m_position_table.SetValue(7,0,::DoubleToString(pos_price,digit_symbol),0,true);               // 价格
            m_position_table.SetValue(8,0,::DoubleToString(CTP::PositionGetDouble(CTP::POSITION_UseMargin),2),0,true);              // 保证金
            m_position_table.SetValue(9,0,::DoubleToString(CTP::PositionGetDouble(CTP::POSITION_StopLoss),digit_symbol),0,true);    // 止损价
            m_position_table.SetValue(10,0,::DoubleToString(CTP::PositionGetDouble(CTP::POSITION_TakeProfit),digit_symbol),0,true); // 止盈价
            m_position_table.SetValue(11,0,::DoubleToString(this_price,digit_symbol),0,true);                                       // 市场价
            m_position_table.SetValue(12,0,::DoubleToString(CTP::PositionGetDouble(CTP::POSITION_PositionProfit),2),0,true);        // 盈利
            m_position_table.SetValue(13,0,::DoubleToString(CTP::PositionGetDouble(CTP::POSITION_OpenProfit),2),0,true);        // 浮动盈亏
           }
        }
     }
// 持仓表明细
   string positiom_headers_mt[] = HEADERS_POSITIONS_MT5;
   if(!CWndCreate::CreateTable(m_position_mt,m_infos,0,m_infos,2,ArraySize(positiom_headers_mt),0,positiom_headers_mt,1,1,0,0,true,true))
     {
      return(false);
     }
   else
     {
      m_position_mt.CellYSize(24);
      m_position_mt.IsZebraFormatRows(clrWhiteSmoke);
      m_position_mt.ColumnResizeMode(true);
      // 初始化/填充数据
      if(CTP::AccountExists())
        {
         for(int pos_index=0; pos_index<mt5ctp::MT5PositionsTotal(); pos_index++)
           {
            ulong ticket = 0;
            mt5ctp::MT5PositionGetTicket(pos_index,ticket);
            MT5CTPOrders order_mt5 = {0};
            if(!mt5ctp::MT5PositionSelectByTicket(ticket,order_mt5))
               continue;
            // 取得市场价
            string pos_symbol = ::CharArrayToString(order_mt5.symbol);
            int digit_symbol = (int)::SymbolInfoInteger(pos_symbol,SYMBOL_DIGITS);
            double this_price = order_mt5.type==0?(::SymbolInfoDouble(pos_symbol,SYMBOL_BID)):(::SymbolInfoDouble(pos_symbol,SYMBOL_ASK));

            if(::StringLen(m_position_mt.GetValue(0,0))>0)
              {
               m_position_mt.AddRow(0);
              }
            //
            m_position_mt.SetValue(0,0,pos_symbol,0,true);                    // 合约代码
            m_position_mt.SetValue(1,0,::IntegerToString(order_mt5.ticket),0,true);                    // 持仓序号
            m_position_mt.SetValue(2,0,::StringFormat("%s.%03d",TimeToString((datetime)order_mt5.time/1000,TIME_DATE|TIME_SECONDS),order_mt5.time%1000),0,true);       // 成交时间
            m_position_mt.SetValue(3,0,order_mt5.type==0?"BUY":"SELL",0,true);                                           // 买/卖
            m_position_mt.SetValue(4,0,::DoubleToString(order_mt5.volume,0),0,true);     // 数量
            m_position_mt.SetValue(5,0,::DoubleToString(order_mt5.price,digit_symbol),0,true);                      // 价格
            m_position_mt.SetValue(6,0,::DoubleToString(order_mt5.sl,digit_symbol),0,true);                        // 止损
            m_position_mt.SetValue(7,0,::DoubleToString(order_mt5.tp,digit_symbol),0,true);                        // 止盈
            m_position_mt.SetValue(8,0,::DoubleToString(this_price,digit_symbol),0,true);                         // 市场价
            m_position_mt.SetValue(9,0,::DoubleToString(order_mt5.profit,2),0,true);                                       // 盈利
            m_position_mt.SetValue(10,0,::IntegerToString(order_mt5.magic),0,true);        // 幻数
            m_position_mt.SetValue(11,0,::CharArrayToString(order_mt5.comment),0,true);        // 注释
           }
        }
     }
// 报单表
   string order_headers[] = HEADERS_ORDERS;
   if(!CWndCreate::CreateTable(m_order_table,m_infos,0,m_infos,3,::ArraySize(order_headers),0,order_headers,1,1,0,0,true,true))
     {
      return(false);
     }
   else
     {
      m_order_table.CellYSize(24);
      m_order_table.IsZebraFormatRows(clrWhiteSmoke);
      m_order_table.ColumnResizeMode(true);
      // 初始化/填充数据
      if(CTP::AccountExists())
        {
         for(int order_index=1; order_index<=CTP::OrdersTotal(); order_index++)
           {
            if(!CTP::OrderSelect(order_index))
               continue;
            // 报单合约
            string order_symbol = CTP::OrderGetString(CTP::ORDER_InstrumentID);
            int digit_symbol = (int)::SymbolInfoInteger(order_symbol,SYMBOL_DIGITS);
            // 报单日期时间
            string order_datetime = CTP::OrderGetString(CTP::ORDER_InsertDate);
            order_datetime += " ";
            order_datetime += CTP::OrderGetString(CTP::ORDER_InsertTime);
            datetime order_time = ::StringToTime(order_datetime);
            if(::StringLen(m_order_table.GetValue(0,0))>0)
              {
               m_order_table.AddRow(0);
              }
            //
            m_order_table.SetValue(0,0,CTP::OrderGetTicket(order_index),0,true);                  // 报单索引
            m_order_table.SetValue(1,0,CTP::OrderGetString(CTP::ORDER_InvestorID),0,true);        // 资金账号
            m_order_table.SetValue(2,0,CTP::OrderGetString(CTP::ORDER_BrokerID),0,true);          // 经纪公司
            m_order_table.SetValue(3,0,order_symbol,0,true);                                      // 合约代码
            m_order_table.SetValue(4,0,::TimeToString(order_time,TIME_DATE|TIME_SECONDS),0,true);                        // 时间
            m_order_table.SetValue(5,0,(char)CTP::OrderGetInteger(CTP::ORDER_Direction)==CTP::ORDER_TYPE_BUY?"BUY":"SELL",0,true);   // 买/卖
            m_order_table.SetValue(6,0,CTP::OrderGetString(CTP::ORDER_CombOffsetFlag),0,true);    // 开/平
            m_order_table.SetValue(7,0,::IntegerToString(CTP::OrderGetInteger(CTP::ORDER_VolumeTotalOriginal)),0,true);      // "数量",
            m_order_table.SetValue(8,0,::IntegerToString(CTP::OrderGetInteger(CTP::ORDER_VolumeTotal)),0,true);                // "剩余数量",
            m_order_table.SetValue(9,0,::DoubleToString(CTP::OrderGetDouble(CTP::ORDER_LimitPrice),digit_symbol),0,true);      // "价格",
            m_order_table.SetValue(10,0,::DoubleToString(CTP::OrderGetDouble(CTP::ORDER_StopLoss),digit_symbol),0,true);      // "止损价",
            m_order_table.SetValue(11,0,::DoubleToString(CTP::OrderGetDouble(CTP::ORDER_TakeProfit),digit_symbol),0,true);      // "止盈价",
            m_order_table.SetValue(12,0,::DoubleToString(CTP::OrderGetDouble(CTP::ORDER_StopPrice),digit_symbol),0,true);      // "触发价",
            m_order_table.SetValue(13,0,::CharToString((uchar)CTP::OrderGetInteger(CTP::ORDER_ContingentCondition)),0,true);      // "触发条件",
            m_order_table.SetValue(14,0,::CharToString((uchar)CTP::OrderGetInteger(CTP::ORDER_OrderStatus)),0,true);      // "报单状态",
            m_order_table.SetValue(15,0,CTP::OrderGetString(CTP::ORDER_StatusMsg),0,true);                                // "报单信息"
           }
        }
     }
// 历史报单表
   string history_order_headers[] = HEADERS_HISTORY_ORDERS;
   if(!CWndCreate::CreateTable(m_history_order_table,m_infos,0,m_infos,4,ArraySize(history_order_headers),0,history_order_headers,1,1,0,0,true,true))
     {
      return(false);
     }
   else
     {
      m_history_order_table.CellYSize(24);
      m_history_order_table.IsZebraFormatRows(clrWhiteSmoke);
      m_history_order_table.ColumnResizeMode(true);
      // 初始化/填充数据
      if(CTP::AccountExists())
        {
         for(int order_index=1; order_index<=CTP::HistoryOrdersTotal(); order_index++)
           {
            if(!CTP::HistoryOrderSelect(order_index))
               continue;
            // 报单合约
            string order_symbol = CTP::HistoryOrderGetString(CTP::ORDER_InstrumentID);
            int digit_symbol = (int)::SymbolInfoInteger(order_symbol,SYMBOL_DIGITS);
            // 报单日期时间
            string order_datetime = CTP::HistoryOrderGetString(CTP::ORDER_InsertDate);
            order_datetime += " ";
            order_datetime += CTP::HistoryOrderGetString(CTP::ORDER_InsertTime);
            datetime order_time = ::StringToTime(order_datetime);
            if(::StringLen(m_history_order_table.GetValue(0,0))>0)
              {
               m_history_order_table.AddRow(0);
              }
            m_history_order_table.SetValue(0,0,CTP::HistoryOrderGetTicket(order_index),0,true);                  // 报单索引
            m_history_order_table.SetValue(1,0,CTP::HistoryOrderGetString(CTP::ORDER_InvestorID),0,true);        // 资金账号
            m_history_order_table.SetValue(2,0,CTP::HistoryOrderGetString(CTP::ORDER_BrokerID),0,true);          // 经纪公司
            m_history_order_table.SetValue(3,0,order_symbol,0,true);                                             // 合约代码
            m_history_order_table.SetValue(4,0,::TimeToString(order_time,TIME_DATE|TIME_SECONDS),0,true);                               // 时间
            m_history_order_table.SetValue(5,0,(char)CTP::HistoryOrderGetInteger(CTP::ORDER_Direction)==CTP::ORDER_TYPE_BUY?"BUY":"SELL",0,true);   // 买/卖
            m_history_order_table.SetValue(6,0,CTP::HistoryOrderGetString(CTP::ORDER_CombOffsetFlag),0,true);    // 开/平
            m_history_order_table.SetValue(7,0,::IntegerToString(CTP::HistoryOrderGetInteger(CTP::ORDER_VolumeTotalOriginal)),0,true);      // "数量",
            m_history_order_table.SetValue(8,0,::IntegerToString(CTP::HistoryOrderGetInteger(CTP::ORDER_VolumeTotal)),0,true);                // "剩余数量",
            m_history_order_table.SetValue(9,0,::DoubleToString(CTP::HistoryOrderGetDouble(CTP::ORDER_LimitPrice),digit_symbol),0,true);      // "价格",
            m_history_order_table.SetValue(10,0,::DoubleToString(CTP::HistoryOrderGetDouble(CTP::ORDER_StopLoss),digit_symbol),0,true);      // "止损价",
            m_history_order_table.SetValue(11,0,::DoubleToString(CTP::HistoryOrderGetDouble(CTP::ORDER_TakeProfit),digit_symbol),0,true);      // "止盈价",
            m_history_order_table.SetValue(12,0,::DoubleToString(CTP::HistoryOrderGetDouble(CTP::ORDER_StopPrice),digit_symbol),0,true);      // "触发价",
            m_history_order_table.SetValue(13,0,::CharToString((uchar)CTP::HistoryOrderGetInteger(CTP::ORDER_ContingentCondition)),0,true);      // "触发条件",
            m_history_order_table.SetValue(14,0,::CharToString((uchar)CTP::HistoryOrderGetInteger(CTP::ORDER_OrderStatus)),0,true);      // "报单状态",
            m_history_order_table.SetValue(15,0,CTP::HistoryOrderGetString(CTP::ORDER_StatusMsg),0,true);                                // "报单信息"
            m_history_order_table.SetValue(16,0,CTP::HistoryOrderGetString(CTP::ORDER_ErrorMsg),0,true);                                 // "错误信息"
           }
        }
     }
// 成交表
   string trade_headers[] = HEADERS_TRADES;
   if(!CWndCreate::CreateTable(m_trade_table,m_infos,0,m_infos,5,ArraySize(trade_headers),0,trade_headers,1,1,0,0,true,true))
     {
      return(false);
     }
   else
     {
      m_trade_table.CellYSize(24);
      m_trade_table.IsZebraFormatRows(clrWhiteSmoke);
      m_trade_table.ColumnResizeMode(true);
      // 初始化/填充数据
      if(CTP::AccountExists())
        {
         for(int deal_index=1; deal_index<=CTP::HistoryDealsTotal(); deal_index++)
           {
            if(!CTP::HistoryDealSelect(deal_index))
               continue;
            // 合约
            string deal_symbol = CTP::HistoryDealGetString(CTP::DEAL_InstrumentID);
            int digit_symbol = (int)::SymbolInfoInteger(deal_symbol,SYMBOL_DIGITS);
            // 日期时间
            string deal_datetime = CTP::HistoryDealGetString(CTP::DEAL_TradeDate);
            deal_datetime += " ";
            deal_datetime += CTP::HistoryDealGetString(CTP::DEAL_TradeTime);
            datetime deal_time = ::StringToTime(deal_datetime);
            if(::StringLen(m_trade_table.GetValue(0,0))>0)
              {
               m_trade_table.AddRow(0);
              }
            m_trade_table.SetValue(0,0,CTP::HistoryDealGetTicket(deal_index),0,true);                  //"成交索引",
            m_trade_table.SetValue(1,0,CTP::HistoryDealGetString(CTP::DEAL_InvestorID),0,true);        // 资金账号
            m_trade_table.SetValue(2,0,CTP::HistoryDealGetString(CTP::DEAL_BrokerID),0,true);          // 经纪公司
            m_trade_table.SetValue(3,0,deal_symbol,0,true);                                            // 合约代码
            m_trade_table.SetValue(4,0,::TimeToString(deal_time,TIME_DATE|TIME_SECONDS),0,true);                              // "时间",
            m_trade_table.SetValue(5,0,(char)CTP::HistoryDealGetInteger(CTP::DEAL_Direction)==CTP::ORDER_TYPE_BUY?"BUY":"SELL",0,true);  // "买/卖",
            m_trade_table.SetValue(6,0,::CharToString((uchar)CTP::HistoryDealGetInteger(CTP::DEAL_Direction)),0,true);             // "开/平",
            m_trade_table.SetValue(7,0,::IntegerToString(CTP::HistoryDealGetInteger(CTP::DEAL_Volume)),0,true);                   // "数量",
            m_trade_table.SetValue(8,0,::DoubleToString(CTP::HistoryDealGetDouble(CTP::DEAL_Price),digit_symbol),0,true);         //"价格"
           }
        }
     }
// order------------------------------
//
   if(!CWndCreate::CreateTextEdit(m_symbol_text,"合约代码",m_orders,0,m_orders,0,false,10,7,160,100,"",""))
     {
      return(false);
     }
//
   string Hedge_label[] = {"投机","套利","套保"};
   int Hedge_x_offset[] = {180,235,290};
   int Hedge_y_offset[] = {12,12,12};
   int Hedge_width[] = {50,50,50};
   if(!CWndCreate::CreateRadioButtons(m_Hedge_button,m_orders,0,m_orders,0,0,0,Hedge_x_offset,Hedge_y_offset,Hedge_label,Hedge_width))
     {
      return(false);
     }
//
   double price_uplimit = 0;
   double price_dwlimit = 0;
   double price_tick = 0;
   int price_digit = 0;
   if(!CWndCreate::CreateTextEdit(m_price_text,"价格",m_orders,0,m_orders,0,false,10,35,160,100,price_uplimit,price_dwlimit,price_tick,price_digit))
     {
      return(false);
     }
//
   if(!CWndCreate::CreateCheckbox(m_price_select0,"成交",m_orders,0,m_orders,0,180,40,50))
     {
      return(false);
     }
   if(!CWndCreate::CreateCheckbox(m_price_select1,"对手",m_orders,0,m_orders,0,235,40,50))
     {
      return(false);
     }
   if(!CWndCreate::CreateCheckbox(m_price_select2,"停板",m_orders,0,m_orders,0,290,40,50))
     {
      return(false);
     }
//
   if(!CWndCreate::CreateTextEdit(m_volume_text,"数量",m_orders,0,m_orders,0,false,10,63,160,100,price_uplimit,price_dwlimit,price_tick,price_digit))
     {
      return(false);
     }
//
   if(!CWndCreate::CreateCheckbox(m_volume_select0,"1/3",m_orders,0,m_orders,0,180,68,50))
     {
      return(false);
     }
   if(!CWndCreate::CreateCheckbox(m_volume_select1,"1/2",m_orders,0,m_orders,0,235,68,50))
     {
      return(false);
     }
   if(!CWndCreate::CreateCheckbox(m_volume_select2,"2/3",m_orders,0,m_orders,0,290,68,50))
     {
      return(false);
     }
//
   if(!CWndCreate::CreateTextEdit(m_stoploss_text,"止损价",m_orders,0,m_orders,0,true,10,91,160,100,price_uplimit,price_dwlimit,price_tick,price_digit))
     {
      return(false);
     }
   if(!CWndCreate::CreateTextEdit(m_takeprofit_text,"止盈价",m_orders,0,m_orders,0,true,180,91,160,100,price_uplimit,price_dwlimit,price_tick,price_digit))
     {
      return(false);
     }
// m_order_line
   if(!CWndCreate::CreateSepLine(m_order_line,m_orders,0,m_orders,0,10,120,330,2,C'150,150,150',clrWhite,H_SEP_LINE))
     {
      return(false);
     }
//
   string openclose_label[] = {"开仓","平仓","平今仓","自动开平"};
   int buysell_x_offset[] = {30,100,170,240};
   int buysell_y_offset[] = {130,130,130,130};
   int buysell_width[] = {70,70,70,70};
   if(!CWndCreate::CreateRadioButtons(m_openclose_button,m_orders,0,m_orders,0,0,0,buysell_x_offset,buysell_y_offset,openclose_label,buysell_width))
     {
      return(false);
     }
//
   string buysell_button[] = LABEL_BUTTON_ORDER;
   if(!CWndCreate::CreateButton(m_buy_button,buysell_button[0],m_orders,0,m_orders,0,10,155,160,40,45,8,10,10,""))
     {
      return(false);
     }
   else
     {
      m_buy_button.Font("微软雅黑");
      m_buy_button.FontSize(12);
     }
   if(!CWndCreate::CreateButton(m_sell_button,buysell_button[1],m_orders,0,m_orders,0,180,155,160,40,45,8,10,10,""))
     {
      return(false);
     }
   else
     {
      m_sell_button.Font("微软雅黑");
      m_sell_button.FontSize(12);
     }
// 快捷操作区
   if(!CWndCreate::CreateFrame(m_select_frame,"快捷操作",m_orders,0,m_orders,0,2,210,345,112,58))
     {
      return(false);
     }
   else
     {
      m_select_frame.Hide();
     }
//
   string select_text[] = {"同账户","同交易所","同品种","同合约","同方向","同幻数"};
   if(!CWndCreate::CreateCheckbox(m_select_account,select_text[0],m_orders,0,m_orders,0,10,228,60))
     {
      return(false);
     }
   else
     {
      m_select_account.Hide();
     }
   if(!CWndCreate::CreateCheckbox(m_select_exchange,select_text[1],m_orders,0,m_orders,0,75,228,70))
     {
      return(false);
     }
   else
     {
      m_select_exchange.Hide();
     }
   if(!CWndCreate::CreateCheckbox(m_select_product,select_text[2],m_orders,0,m_orders,0,150,228,60))
     {
      return(false);
     }
   else
     {
      m_select_product.Hide();
     }
   if(!CWndCreate::CreateCheckbox(m_select_symbol,select_text[3],m_orders,0,m_orders,0,215,228,60))
     {
      return(false);
     }
   else
     {
      m_select_symbol.Hide();
     }
   if(!CWndCreate::CreateCheckbox(m_select_buysell,select_text[4],m_orders,0,m_orders,0,280,228,60))
     {
      return(false);
     }
   else
     {
      m_select_buysell.Hide();
     }
   if(!CWndCreate::CreateCheckbox(m_select_magic,select_text[5],m_orders,0,m_orders,0,10,228,60))
     {
      return(false);
     }
   else
     {
      m_select_magic.Hide();
     }
// 持仓操作
   string pos_action[] = {"加倍","减半","平仓","锁仓","反手","止损止盈","同步"};
   if(!CWndCreate::CreateButton(m_pos_add_button,pos_action[0],m_orders,0,m_orders,0,9,250,105,30,40,5,10,10,""))
     {
      return(false);
     }
   else
     {
      m_pos_add_button.Font("微软雅黑");
      m_pos_add_button.FontSize(10);
      m_pos_add_button.Hide();
     }
   if(!CWndCreate::CreateButton(m_pos_sub_button,pos_action[1],m_orders,0,m_orders,0,123,250,105,30,40,5,10,10,""))
     {
      return(false);
     }
   else
     {
      m_pos_sub_button.Font("微软雅黑");
      m_pos_sub_button.FontSize(10);
      m_pos_sub_button.Hide();
     }
   if(!CWndCreate::CreateButton(m_pos_cls_button,pos_action[2],m_orders,0,m_orders,0,237,250,105,30,40,5,10,10,""))
     {
      return(false);
     }
   else
     {
      m_pos_cls_button.Font("微软雅黑");
      m_pos_cls_button.FontSize(10);
      m_pos_cls_button.Hide();
     }
   if(!CWndCreate::CreateButton(m_pos_hedg_button,pos_action[3],m_orders,0,m_orders,0,9,285,105,30,40,5,10,10,""))
     {
      return(false);
     }
   else
     {
      m_pos_hedg_button.Font("微软雅黑");
      m_pos_hedg_button.FontSize(10);
      m_pos_hedg_button.Hide();
     }
   if(!CWndCreate::CreateButton(m_pos_back_button,pos_action[4],m_orders,0,m_orders,0,123,285,105,30,40,5,10,10,""))
     {
      return(false);
     }
   else
     {
      m_pos_back_button.Font("微软雅黑");
      m_pos_back_button.FontSize(10);
      m_pos_back_button.Hide();
     }
   if(!CWndCreate::CreateButton(m_pos_sync_button,pos_action[6],m_orders,0,m_orders,0,123,285,105,30,40,5,10,10,""))
     {
      return(false);
     }
   else
     {
      m_pos_sync_button.Font("微软雅黑");
      m_pos_sync_button.FontSize(10);
      m_pos_sync_button.Hide();
     }
   if(!CWndCreate::CreateButton(m_pos_sltp_button,pos_action[5],m_orders,0,m_orders,0,237,285,105,30,30,5,10,10,""))
     {
      return(false);
     }
   else
     {
      m_pos_sltp_button.Font("微软雅黑");
      m_pos_sltp_button.FontSize(10);
      m_pos_sltp_button.Hide();
     }
// 订单操作
   string order_action[] = {"改价","改量","撤单","镜像","全撤","止损止盈"};
   if(!CWndCreate::CreateButton(m_order_modprice_button,order_action[0],m_orders,0,m_orders,0,9,250,105,30,40,5,10,10,""))
     {
      return(false);
     }
   else
     {
      m_order_modprice_button.Font("微软雅黑");
      m_order_modprice_button.FontSize(10);
      m_order_modprice_button.Hide();
     }
   if(!CWndCreate::CreateButton(m_order_modvolume_button,order_action[1],m_orders,0,m_orders,0,123,250,105,30,40,5,10,10,""))
     {
      return(false);
     }
   else
     {
      m_order_modvolume_button.Font("微软雅黑");
      m_order_modvolume_button.FontSize(10);
      m_order_modvolume_button.Hide();
     }
   if(!CWndCreate::CreateButton(m_order_del_button,order_action[2],m_orders,0,m_orders,0,237,250,105,30,40,5,10,10,""))
     {
      return(false);
     }
   else
     {
      m_order_del_button.Font("微软雅黑");
      m_order_del_button.FontSize(10);
      m_order_del_button.Hide();
     }
   if(!CWndCreate::CreateButton(m_order_back_button,order_action[3],m_orders,0,m_orders,0,9,285,105,30,40,5,10,10,""))
     {
      return(false);
     }
   else
     {
      m_order_back_button.Font("微软雅黑");
      m_order_back_button.FontSize(10);
      m_order_back_button.Hide();
     }
   if(!CWndCreate::CreateButton(m_order_delall_button,order_action[4],m_orders,0,m_orders,0,123,285,105,30,40,5,10,10,""))
     {
      return(false);
     }
   else
     {
      m_order_delall_button.Font("微软雅黑");
      m_order_delall_button.FontSize(10);
      m_order_delall_button.Hide();
     }
   if(!CWndCreate::CreateButton(m_order_sltp_button,order_action[5],m_orders,0,m_orders,0,237,285,105,30,30,5,10,10,""))
     {
      return(false);
     }
   else
     {
      m_order_sltp_button.Font("微软雅黑");
      m_order_sltp_button.FontSize(10);
      m_order_sltp_button.Hide();
     }
// 窗口创建完成
   CWndEvents::CompletedGUI();
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ToolBox::OnEvent(const int id,const long &lparam,const double &dparam,const string &sparam)
  {
// 交易事件处理
   m_trade.OnChartEvent(id,lparam,dparam,sparam);
// 界面重绘
   if(id == CHARTEVENT_CHART_CHANGE)
     {
      // 下单面板
      m_orders.XGap(m_window.XSize()-ORDER_WIDTH-3);
      m_orders.Moving();
      return;
     }
// 元素选择
   if(id==CHARTEVENT_CUSTOM+ON_CLICK_LIST_ITEM)
     {
      if(lparam==m_position_table.Id())
        {
         int row_pos = m_position_table.SelectedItem();
         if(row_pos!=WRONG_VALUE)
           {
            string order_symbol = m_position_table.GetValue(3,row_pos);
            if(StringLen(order_symbol)>0)
              {
               m_symbol_text.SetValue(order_symbol);
               m_symbol_text.GetTextBoxPointer().Update(true);
               // 属性设置
               double tick_size = SymbolInfoDouble(order_symbol,SYMBOL_TRADE_TICK_SIZE);
               int this_digit = (int)SymbolInfoInteger(order_symbol,SYMBOL_DIGITS);
               int lots_limit = (int)CTP::SymbolInfoInteger(order_symbol,CTP::SYMBOL_MaxLimitOrderVolume);
               //
               m_volume_text.StepValue(1);
               m_volume_text.SetDigits(0);
               m_volume_text.MinValue(1);
               m_volume_text.MaxValue(lots_limit);
               //
               m_price_text.StepValue(tick_size);
               m_price_text.SetDigits(this_digit);
               //
               m_stoploss_text.StepValue(tick_size);
               m_stoploss_text.SetDigits(this_digit);
               // 填充数量
               m_volume_text.SetValue(m_position_table.GetValue(6,row_pos));
               m_volume_text.GetTextBoxPointer().Update(true);
               // 填充开平仓
               string order_exchange = SymbolInfoString(order_symbol,SYMBOL_EXCHANGE);
               if((order_exchange=="SHFE" || order_exchange=="INE")&& (m_position_table.GetValue(4,row_pos))[0]=='1')
                 {
                  m_openclose_button.SelectButton(2);
                 }
               else
                 {
                  m_openclose_button.SelectButton(1);
                 }
               m_openclose_button.Update(true);
               // 填充价格选择
               if(!(m_price_select0.IsPressed() || m_price_select1.IsPressed() || m_price_select2.IsPressed()))
                 {
                  m_price_select1.IsPressed(true);
                  m_price_select1.Update(true);
                 }
               // 填充数量选择
               if(m_volume_select0.IsPressed())
                 {
                  m_volume_select0.IsPressed(false);
                  m_volume_select0.Update(true);
                 }
               if(m_volume_select1.IsPressed())
                 {
                  m_volume_select1.IsPressed(false);
                  m_volume_select1.Update(true);
                 }
               if(m_volume_select2.IsPressed())
                 {
                  m_volume_select2.IsPressed(false);
                  m_volume_select2.Update(true);
                 }
               // 填充止损价
               string s_stoploss = m_position_table.GetValue(9,row_pos);
               if(StringToDouble(s_stoploss)>0)
                 {
                  m_stoploss_text.IsPressed(true);
                  m_stoploss_text.SetValue(s_stoploss);
                  m_stoploss_text.GetTextBoxPointer().Update(true);
                  m_stoploss_text.Update(true);
                 }
               else
                 {
                  m_stoploss_text.IsPressed(false);
                  m_stoploss_text.Update(true);
                 }
               // 填充止盈价
               string s_takeprofit = m_position_table.GetValue(10,row_pos);
               if(StringToDouble(s_takeprofit)>0)
                 {
                  m_takeprofit_text.IsPressed(true);
                  m_takeprofit_text.SetValue(s_takeprofit);
                  m_takeprofit_text.GetTextBoxPointer().Update(true);
                  m_takeprofit_text.Update(true);
                 }
               else
                 {
                  m_takeprofit_text.IsPressed(false);
                  m_takeprofit_text.Update(true);
                 }
              }
           }
         return;
        }
      if(lparam==m_position_mt.Id())
        {
         int row_pos = m_position_mt.SelectedItem();
         if(row_pos!=WRONG_VALUE)
           {
            string order_symbol = m_position_mt.GetValue(0,row_pos);
            if(::StringLen(order_symbol)>0)
              {
               m_symbol_text.SetValue(order_symbol);
               m_symbol_text.GetTextBoxPointer().Update(true);
               // 属性设置
               double tick_size = SymbolInfoDouble(order_symbol,SYMBOL_TRADE_TICK_SIZE);
               int this_digit = (int)SymbolInfoInteger(order_symbol,SYMBOL_DIGITS);
               int lots_limit = (int)CTP::SymbolInfoInteger(order_symbol,CTP::SYMBOL_MaxLimitOrderVolume);
               //
               m_volume_text.StepValue(1);
               m_volume_text.SetDigits(0);
               m_volume_text.MinValue(1);
               m_volume_text.MaxValue(lots_limit);
               //
               m_price_text.StepValue(tick_size);
               m_price_text.SetDigits(this_digit);
               //
               m_stoploss_text.StepValue(tick_size);
               m_stoploss_text.SetDigits(this_digit);
               // 填充数量
               m_volume_text.SetValue(m_position_mt.GetValue(4,row_pos));
               m_volume_text.GetTextBoxPointer().Update(true);
               // 填充价格选择
               if(!(m_price_select0.IsPressed() || m_price_select1.IsPressed() || m_price_select2.IsPressed()))
                 {
                  m_price_select1.IsPressed(true);
                  m_price_select1.Update(true);
                 }
               // 填充数量选择
               if(m_volume_select0.IsPressed())
                 {
                  m_volume_select0.IsPressed(false);
                  m_volume_select0.Update(true);
                 }
               if(m_volume_select1.IsPressed())
                 {
                  m_volume_select1.IsPressed(false);
                  m_volume_select1.Update(true);
                 }
               if(m_volume_select2.IsPressed())
                 {
                  m_volume_select2.IsPressed(false);
                  m_volume_select2.Update(true);
                 }
               // 填充止损价
               string s_stoploss = m_position_mt.GetValue(6,row_pos);
               if(StringToDouble(s_stoploss)>0)
                 {
                  m_stoploss_text.IsPressed(true);
                  m_stoploss_text.SetValue(s_stoploss);
                  m_stoploss_text.GetTextBoxPointer().Update(true);
                  m_stoploss_text.Update(true);
                 }
               else
                 {
                  m_stoploss_text.IsPressed(false);
                  m_stoploss_text.Update(true);
                 }
               // 填充止盈价
               string s_takeprofit = m_position_mt.GetValue(7,row_pos);
               if(StringToDouble(s_takeprofit)>0)
                 {
                  m_takeprofit_text.IsPressed(true);
                  m_takeprofit_text.SetValue(s_takeprofit);
                  m_takeprofit_text.GetTextBoxPointer().Update(true);
                  m_takeprofit_text.Update(true);
                 }
               else
                 {
                  m_takeprofit_text.IsPressed(false);
                  m_takeprofit_text.Update(true);
                 }
              }
           }
         return;
        }
      if(lparam==m_order_table.Id())
        {
         int row_pos = m_order_table.SelectedItem();
         if(row_pos!=WRONG_VALUE)
           {
            string order_symbol = m_order_table.GetValue(3,row_pos);
            if(StringLen(order_symbol)>0)
              {
               m_symbol_text.SetValue(order_symbol);
               m_symbol_text.GetTextBoxPointer().Update(true);
               // 属性设置
               double tick_size = SymbolInfoDouble(order_symbol,SYMBOL_TRADE_TICK_SIZE);
               int this_digit = (int)SymbolInfoInteger(order_symbol,SYMBOL_DIGITS);
               int lots_limit = (int)CTP::SymbolInfoInteger(order_symbol,CTP::SYMBOL_MaxLimitOrderVolume);
               //
               m_volume_text.StepValue(1);
               m_volume_text.SetDigits(0);
               m_volume_text.MinValue(1);
               m_volume_text.MaxValue(lots_limit);
               //
               m_price_text.StepValue(tick_size);
               m_price_text.SetDigits(this_digit);
               //
               m_stoploss_text.StepValue(tick_size);
               m_stoploss_text.SetDigits(this_digit);
               // 填充价格
               string str_prcie = m_order_table.GetValue(12,row_pos);
               str_prcie = StringToDouble(str_prcie)>0?str_prcie:m_order_table.GetValue(9,row_pos);
               m_price_text.SetValue(str_prcie);
               m_price_text.GetTextBoxPointer().Update(true);
               // 填充数量
               m_volume_text.SetValue(m_order_table.GetValue(7,row_pos));
               m_volume_text.GetTextBoxPointer().Update(true);
               // 填充开平仓
               string order_openclose = m_order_table.GetValue(6,row_pos);
               if(order_openclose[0]=='0')
                 {
                  m_openclose_button.SelectButton(0);
                 }
               else
                  if(order_openclose[0]=='3')
                    {
                     m_openclose_button.SelectButton(2);
                    }
                  else
                    {
                     m_openclose_button.SelectButton(1);
                    }
               m_openclose_button.Update(true);
               // 填充价格选择
               if(m_price_select0.IsPressed())
                 {
                  m_price_select0.IsPressed(false);
                  m_price_select0.Update(true);
                 }
               if(m_price_select1.IsPressed())
                 {
                  m_price_select1.IsPressed(false);
                  m_price_select1.Update(true);
                 }
               if(m_price_select2.IsPressed())
                 {
                  m_price_select2.IsPressed(false);
                  m_price_select2.Update(true);
                 }
               // 填充数量选择
               if(m_volume_select0.IsPressed())
                 {
                  m_volume_select0.IsPressed(false);
                  m_volume_select0.Update(true);
                 }
               if(m_volume_select1.IsPressed())
                 {
                  m_volume_select1.IsPressed(false);
                  m_volume_select1.Update(true);
                 }
               if(m_volume_select2.IsPressed())
                 {
                  m_volume_select2.IsPressed(false);
                  m_volume_select2.Update(true);
                 }
               // 填充止损价
               string s_stoploss = m_order_table.GetValue(10,row_pos);
               if(StringToDouble(s_stoploss)>0)
                 {
                  m_stoploss_text.IsPressed(true);
                  m_stoploss_text.SetValue(s_stoploss);
                  m_stoploss_text.GetTextBoxPointer().Update(true);
                  m_stoploss_text.Update(true);
                 }
               else
                 {
                  m_stoploss_text.IsPressed(false);
                  m_stoploss_text.Update(true);
                 }
               // 填充止盈价
               string s_takeprofit = m_order_table.GetValue(11,row_pos);
               if(StringToDouble(s_takeprofit)>0)
                 {
                  m_takeprofit_text.IsPressed(true);
                  m_takeprofit_text.SetValue(s_takeprofit);
                  m_takeprofit_text.GetTextBoxPointer().Update(true);
                  m_takeprofit_text.Update(true);
                 }
               else
                 {
                  m_takeprofit_text.IsPressed(false);
                  m_takeprofit_text.Update(true);
                 }
              }
           }
         return;
        }
      return;
     }
//
   if(id==CHARTEVENT_CUSTOM+ON_CLICK_TAB)
     {
      if(lparam==m_infos.Id())
        {
         if(m_select_frame.IsVisible())
           {
            m_select_frame.Hide();
            m_select_account.Hide();
            m_select_magic.Hide();
            m_select_exchange.Hide();
            m_select_product.Hide();
            m_select_symbol.Hide();
            m_select_buysell.Hide();
            //
            m_pos_add_button.Hide();
            m_pos_sub_button.Hide();
            m_pos_cls_button.Hide();
            m_pos_hedg_button.Hide();
            m_pos_back_button.Hide();
            m_pos_sync_button.Hide();
            m_pos_sltp_button.Hide();
            //
            m_order_modprice_button.Hide();
            m_order_modvolume_button.Hide();
            m_order_del_button.Hide();
            m_order_back_button.Hide();
            m_order_delall_button.Hide();
            m_order_sltp_button.Hide();
           }
         if(!m_openclose_button.IsVisible())
           {
            m_openclose_button.Reset();
           }
         if((int)dparam == 1 && m_position_table.IsVisible())
           {
            if(!m_select_frame.IsVisible())
              {
               m_select_frame.Reset();
               m_select_account.Reset();
               m_select_exchange.Reset();
               m_select_product.Reset();
               m_select_symbol.Reset();
               m_select_buysell.Reset();
               m_pos_add_button.Reset();
               m_pos_sub_button.Reset();
               m_pos_cls_button.Reset();
               m_pos_hedg_button.Reset();
               m_pos_back_button.Reset();
               m_pos_sltp_button.Reset();
              }
            return;
           }
         if((int)dparam == 2 && m_position_mt.IsVisible())
           {
            if(!m_select_frame.IsVisible())
              {
               m_select_frame.Reset();
               m_select_magic.Reset();
               m_select_exchange.Reset();
               m_select_product.Reset();
               m_select_symbol.Reset();
               m_select_buysell.Reset();
               m_pos_add_button.Reset();
               m_pos_sub_button.Reset();
               m_pos_cls_button.Reset();
               m_pos_hedg_button.Reset();
               m_pos_sync_button.Reset();
               m_pos_sltp_button.Reset();
              }
            if(m_openclose_button.IsVisible())
              {
               m_openclose_button.Hide();
              }
            return;
           }
         if((int)dparam == 3 && m_order_table.IsVisible())
           {
            if(!m_select_frame.IsVisible())
              {
               m_select_frame.Reset();
               m_select_account.Reset();
               m_select_exchange.Reset();
               m_select_product.Reset();
               m_select_symbol.Reset();
               m_select_buysell.Reset();
               m_order_modprice_button.Reset();
               m_order_modvolume_button.Reset();
               m_order_del_button.Reset();
               m_order_back_button.Reset();
               m_order_delall_button.Reset();
               m_order_sltp_button.Reset();
              }
            return;
           }
        }
      return;
     }
//
   if(id==CHARTEVENT_CUSTOM+ON_CLICK_CHECKBOX)
     {
      // 价格选择
      if(lparam==m_price_select0.Id())
        {
         if(m_price_select0.IsPressed())
           {
            if(m_price_select1.IsPressed())
              {
               m_price_select1.IsPressed(false);
               m_price_select1.Update(true);
              }
            if(m_price_select2.IsPressed())
              {
               m_price_select2.IsPressed(false);
               m_price_select2.Update(true);
              }
           }
         return;
        }
      if(lparam==m_price_select1.Id())
        {
         if(m_price_select1.IsPressed())
           {
            if(m_price_select0.IsPressed())
              {
               m_price_select0.IsPressed(false);
               m_price_select0.Update(true);
              }
            if(m_price_select2.IsPressed())
              {
               m_price_select2.IsPressed(false);
               m_price_select2.Update(true);
              }
           }
         return;
        }
      if(lparam==m_price_select2.Id())
        {
         if(m_price_select2.IsPressed())
           {
            if(m_price_select0.IsPressed())
              {
               m_price_select0.IsPressed(false);
               m_price_select0.Update(true);
              }
            if(m_price_select1.IsPressed())
              {
               m_price_select1.IsPressed(false);
               m_price_select1.Update(true);
              }
           }
         return;
        }
      // 数量选择
      if(lparam==m_volume_select0.Id())
        {
         if(m_volume_select0.IsPressed())
           {
            if(m_volume_select1.IsPressed())
              {
               m_volume_select1.IsPressed(false);
               m_volume_select1.Update(true);
              }
            if(m_volume_select2.IsPressed())
              {
               m_volume_select2.IsPressed(false);
               m_volume_select2.Update(true);
              }
            // 数量计算
            if(CTP::AccountExists() && m_openclose_button.SelectedButtonIndex()==0)
              {
               string order_symbol = m_symbol_text.GetValue();
               double symbol_price = SymbolInfoDouble(order_symbol,SYMBOL_LAST);
               double symbol_size = SymbolInfoDouble(order_symbol,SYMBOL_TRADE_CONTRACT_SIZE);
               double symbol_ratio = CTP::SymbolInfoDouble(order_symbol,CTP::SYMBOL_ExchangeLongMarginRatioByMoney);
               double money_avl = CTP::AccountInfoDouble(CTP::ACCOUNT_Available);
               double volume = money_avl/(symbol_price*symbol_size*symbol_ratio);
               m_volume_text.SetValue(DoubleToString(volume/3,0),true);
               m_volume_text.GetTextBoxPointer().Update(true);
              }
           }
         else
           {
            double volume = 1.0;
            m_volume_text.SetValue(DoubleToString(volume,0),true);
            m_volume_text.GetTextBoxPointer().Update(true);
           }
         return;
        }
      if(lparam==m_volume_select1.Id())
        {
         if(m_volume_select1.IsPressed())
           {
            if(m_volume_select0.IsPressed())
              {
               m_volume_select0.IsPressed(false);
               m_volume_select0.Update(true);
              }
            if(m_volume_select2.IsPressed())
              {
               m_volume_select2.IsPressed(false);
               m_volume_select2.Update(true);
              }
            // 数量计算
            if(CTP::AccountExists() && m_openclose_button.SelectedButtonIndex()==0)
              {
               string order_symbol = m_symbol_text.GetValue();
               double symbol_price = SymbolInfoDouble(order_symbol,SYMBOL_LAST);
               double symbol_size = SymbolInfoDouble(order_symbol,SYMBOL_TRADE_CONTRACT_SIZE);
               double symbol_ratio = CTP::SymbolInfoDouble(order_symbol,CTP::SYMBOL_ExchangeLongMarginRatioByMoney);
               double money_avl = CTP::AccountInfoDouble(CTP::ACCOUNT_Available);
               double volume = money_avl/(symbol_price*symbol_size*symbol_ratio);
               m_volume_text.SetValue(DoubleToString(volume/2,0),true);
               m_volume_text.GetTextBoxPointer().Update(true);
              }
           }
         else
           {
            double volume = 1.0;
            m_volume_text.SetValue(DoubleToString(volume,0),true);
            m_volume_text.GetTextBoxPointer().Update(true);
           }
         return;
        }
      if(lparam==m_volume_select2.Id())
        {
         if(m_volume_select2.IsPressed())
           {
            if(m_volume_select0.IsPressed())
              {
               m_volume_select0.IsPressed(false);
               m_volume_select0.Update(true);
              }
            if(m_volume_select1.IsPressed())
              {
               m_volume_select1.IsPressed(false);
               m_volume_select1.Update(true);
              }
            // 数量计算
            if(CTP::AccountExists() && m_openclose_button.SelectedButtonIndex()==0)
              {
               string order_symbol = m_symbol_text.GetValue();
               double symbol_price = SymbolInfoDouble(order_symbol,SYMBOL_LAST);
               double symbol_size = SymbolInfoDouble(order_symbol,SYMBOL_TRADE_CONTRACT_SIZE);
               double symbol_ratio = CTP::SymbolInfoDouble(order_symbol,CTP::SYMBOL_ExchangeLongMarginRatioByMoney);
               double money_avl = CTP::AccountInfoDouble(CTP::ACCOUNT_Available);
               double volume = money_avl/(symbol_price*symbol_size*symbol_ratio);
               m_volume_text.SetValue(DoubleToString(volume*2/3,0),true);
               m_volume_text.GetTextBoxPointer().Update(true);
              }
           }
         else
           {
            double volume = 1.0;
            m_volume_text.SetValue(DoubleToString(volume,0),true);
            m_volume_text.GetTextBoxPointer().Update(true);
           }
         return;
        }
      // 止损
      if(lparam==m_stoploss_text.Id())
        {
         if(m_stoploss_text.IsPressed())
           {
            string symbol_select = m_symbol_text.GetValue();
            bool is_custom;
            SymbolExist(symbol_select,is_custom);
            if(is_custom)
              {
               // 设置当前价格/行情中提取
               double this_price = SymbolInfoDouble(symbol_select,SYMBOL_LAST);
               int this_digit = (int)SymbolInfoInteger(symbol_select,SYMBOL_DIGITS);
               m_stoploss_text.SetValue(DoubleToString(this_price,this_digit),true);
               m_stoploss_text.GetTextBoxPointer().Update(true);
              }
           }
         else
           {
            m_stoploss_text.SetValue("",true);
            m_stoploss_text.GetTextBoxPointer().Update(true);
           }
         return;
        }
      // 止盈
      if(lparam==m_takeprofit_text.Id())
        {
         if(m_takeprofit_text.IsPressed())
           {
            string symbol_select = m_symbol_text.GetValue();
            bool is_custom;
            SymbolExist(symbol_select,is_custom);
            if(is_custom)
              {
               // 设置当前价格/行情中提取
               double this_price = SymbolInfoDouble(symbol_select,SYMBOL_LAST);
               int this_digit = (int)SymbolInfoInteger(symbol_select,SYMBOL_DIGITS);
               m_takeprofit_text.SetValue(DoubleToString(this_price,this_digit),true);
               m_takeprofit_text.GetTextBoxPointer().Update(true);
              }
           }
         else
           {
            m_takeprofit_text.SetValue("",true);
            m_takeprofit_text.GetTextBoxPointer().Update(true);
           }
         return;
        }
      //
      return;
     }
// 输入完成操作
   if(id==CHARTEVENT_CUSTOM+ON_END_EDIT)
     {
      if(lparam==m_symbol_text.Id())
        {
         //合约大小写自适应/矫正
         string symbol_select = m_symbol_text.GetValue();
         ::StringToLower(symbol_select);
         bool is_custom;
         ::SymbolExist(symbol_select,is_custom);
         if(!is_custom)
           {
            StringToUpper(symbol_select);
            ::SymbolExist(symbol_select,is_custom);
            if(!is_custom)
              {
               return;
              }
           }
         // 通过检查/CTP合约
         if(::SymbolInfoString(symbol_select,SYMBOL_PAGE)=="CTP")
           {
            //矫正后的合约代码写回/下单时调用
            m_symbol_text.SetValue(symbol_select);
            m_symbol_text.GetTextBoxPointer().Update(true);
            // 订阅合约行情
            if(!::SymbolInfoInteger(symbol_select,SYMBOL_VISIBLE))
              {
               ::SymbolSelect(symbol_select,true);
              }
            // 属性设置
            double tick_size = SymbolInfoDouble(symbol_select,SYMBOL_TRADE_TICK_SIZE);
            int this_digit = (int)SymbolInfoInteger(symbol_select,SYMBOL_DIGITS);
            int lots_limit = (int)CTP::SymbolInfoInteger(symbol_select,CTP::SYMBOL_MaxLimitOrderVolume);
            //
            m_price_text.StepValue(tick_size);
            m_price_text.SetDigits(this_digit);
            //
            m_volume_text.StepValue(1);
            m_volume_text.SetDigits(0);
            m_volume_text.MinValue(1);
            m_volume_text.MaxValue(lots_limit);
            //
            m_stoploss_text.StepValue(tick_size);
            m_stoploss_text.SetDigits(this_digit);
            if(m_stoploss_text.IsPressed())
              {
               m_stoploss_text.SetValue(DoubleToString(SymbolInfoDouble(symbol_select,SYMBOL_LAST),this_digit),true);
               m_stoploss_text.GetTextBoxPointer().Update(true);
              }
            //
            m_takeprofit_text.StepValue(tick_size);
            m_takeprofit_text.SetDigits(this_digit);
            if(m_takeprofit_text.IsPressed())
              {
               m_takeprofit_text.SetValue(DoubleToString(SymbolInfoDouble(symbol_select,SYMBOL_LAST),this_digit),true);
               m_takeprofit_text.GetTextBoxPointer().Update(true);
              }
            // 数量计算
            if(CTP::AccountExists() && m_openclose_button.SelectedButtonIndex()==0)
              {
               double symbol_price = SymbolInfoDouble(symbol_select,SYMBOL_LAST);
               double symbol_size = SymbolInfoDouble(symbol_select,SYMBOL_TRADE_CONTRACT_SIZE);
               double symbol_ratio = CTP::SymbolInfoDouble(symbol_select,CTP::SYMBOL_ExchangeLongMarginRatioByMoney);
               double money_avl = CTP::AccountInfoDouble(CTP::ACCOUNT_Available);
               double symbol_margin = symbol_price*symbol_size*symbol_ratio;
               if(symbol_margin<0.0001)
                 {
                  return;
                 }
               double volume = money_avl/symbol_margin;
               if(m_volume_select0.IsPressed())
                 {
                  m_volume_text.SetValue(DoubleToString(volume/3,0),true);
                  m_volume_text.GetTextBoxPointer().Update(true);
                 }
               else
                  if(m_volume_select1.IsPressed())
                    {
                     m_volume_text.SetValue(DoubleToString(volume/2,0),true);
                     m_volume_text.GetTextBoxPointer().Update(true);
                    }
                  else
                     if(m_volume_select2.IsPressed())
                       {
                        m_volume_text.SetValue(DoubleToString(volume*2/3,0),true);
                        m_volume_text.GetTextBoxPointer().Update(true);
                       }
              }
           }
        }
      return;
     }
//
   if(id==CHARTEVENT_CUSTOM+ON_CLICK_BUTTON)
     {
      if(lparam==m_buy_button.Id())
        {
         string order_symbol = m_symbol_text.GetValue();
         bool is_custom;
         SymbolExist(order_symbol,is_custom);
         if(is_custom && CTP::AccountExists())
           {
            if(!m_position_mt.IsVisible())
              {
               // 报单结构
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               // 报单价格/矫正
               double order_price = StringToDouble(m_price_text.GetValue());
               if(m_price_select0.IsPressed())
                 {
                  order_price = SymbolInfoDouble(order_symbol,SYMBOL_LAST);
                 }
               else
                  if(m_price_select1.IsPressed())
                    {
                     order_price = SymbolInfoDouble(order_symbol,SYMBOL_ASK);
                    }
                  else
                     if(m_price_select2.IsPressed())
                       {
                        order_price = CTP::SymbolInfoDouble(order_symbol,CTP::SYMBOL_UpperLimitPrice);
                       }
               // 报单数量
               double order_lots = StringToDouble(m_volume_text.GetValue());
               // 止损/止盈
               double order_sl = m_stoploss_text.IsPressed()?StringToDouble(m_stoploss_text.GetValue()):0;
               double order_tp = m_takeprofit_text.IsPressed()?StringToDouble(m_takeprofit_text.GetValue()):0;
               // 开平仓
               int order_pos = 0;   //----开仓\order_pos>0是平仓的持仓序号
               if(m_openclose_button.SelectedButtonIndex()==0)    //--买入开仓
                 {
                  bool order_act = false;
#ifdef __TRADE_CLASS__
                  order_act = m_trade.PositionOpen(order_symbol,ORDER_TYPE_BUY,(long)order_lots,order_price,0,order_sl,order_tp);
#else
                  ZeroMemory(order_req);
                  ZeroMemory(order_res);
                  order_req.action   = TRADE_ACTION_DEAL;
                  order_req.symbol   = order_symbol;
                  order_req.price    = order_price;
                  order_req.volume   = order_lots;
                  order_req.type     = ORDER_TYPE_BUY;
                  order_req.type_filling = ORDER_FILLING_RETURN;
                  if(order_sl>0)
                    {
                     order_req.sl    = order_sl;
                    }
                  if(order_tp>0)
                    {
                     order_req.tp    = order_tp;
                    }
                  order_act = CTP::OrderSend(order_req,order_res);
#endif
                  return;
                 }
               if(m_openclose_button.SelectedButtonIndex()==1)  //--买入平仓
                 {
                  order_pos = -1;
                  bool order_act = false;
#ifdef __TRADE_CLASS__
                  order_act = m_trade.PositionClose(order_symbol,ORDER_TYPE_BUY,(long)order_lots,order_price,order_pos,0);
#else
                  ZeroMemory(order_req);
                  ZeroMemory(order_res);
                  order_req.action   = TRADE_ACTION_DEAL;
                  order_req.symbol   = order_symbol;
                  order_req.price    = order_price;
                  order_req.volume   = order_lots;
                  order_req.type     = ORDER_TYPE_BUY;
                  order_req.position = order_pos;
                  order_req.type_filling = ORDER_FILLING_RETURN;
                  order_act = CTP::OrderSend(order_req,order_res);
#endif
                  return;
                 }
               if(m_openclose_button.SelectedButtonIndex()==2)  //--买入平今仓
                 {
                  order_pos = -2;
                  bool order_act = false;
#ifdef __TRADE_CLASS__
                  order_act = m_trade.PositionClose(order_symbol,ORDER_TYPE_BUY,(long)order_lots,order_price,order_pos,0);
#else
                  ZeroMemory(order_req);
                  ZeroMemory(order_res);
                  order_req.action   = TRADE_ACTION_DEAL;
                  order_req.symbol   = order_symbol;
                  order_req.price    = order_price;
                  order_req.volume   = order_lots;
                  order_req.type     = ORDER_TYPE_BUY;
                  order_req.position = order_pos;
                  order_req.type_filling = ORDER_FILLING_RETURN;
                  //
                  order_act = CTP::OrderSend(order_req,order_res);
#endif
                  return;
                 }
               if(m_openclose_button.SelectedButtonIndex()==3)  //--先平后开
                 {
                  //查找空头持仓数量\持仓笔数
                  long pos_volume_td = 0;  //今仓量
                  long pos_volume_yd = 0;  //昨仓量
                  int pos_total  = 0;
                  for(int index=CTP::PositionsTotal(); index>0; index--)
                    {
                     if(!CTP::PositionSelect(index))
                        continue;
                     if(CTP::PositionGetSymbol()!=order_symbol)
                        continue;
                     long check_volume = CTP::PositionGetInteger(CTP::POSITION_Position);
                     if(check_volume<=0)
                        continue;
                     if(CTP::PositionGetInteger(CTP::POSITION_PosiDirection)==CTP::POSITION_TYPE_SELL)
                       {
                        if(CTP::PositionGetInteger(CTP::POSITION_PositionDate)=='1')
                          {
                           pos_volume_td += check_volume;
                           pos_total++;
                          }
                        else
                          {
                           pos_volume_yd += check_volume;
                           pos_total++;
                          }
                       }
                    }
                  // 平仓
                  if(pos_total>0)
                    {
                     // 先平今仓
                     if(order_lots>0 && pos_volume_td>0)
                       {
                        // 本次报单量/平今仓后的余量
                        double this_order_lots = 0;
                        if(order_lots > pos_volume_td)
                          {
                           this_order_lots = (double)pos_volume_td;
                           order_lots = order_lots - pos_volume_td;
                          }
                        else
                          {
                           this_order_lots = order_lots;
                           order_lots = 0;
                          }
                        //
                        order_pos = -2;
                        bool order_act = false;
#ifdef __TRADE_CLASS__
                        order_act = m_trade.PositionClose(order_symbol,ORDER_TYPE_BUY,(long)this_order_lots,order_price,order_pos,0);
#else
                        ZeroMemory(order_req);
                        ZeroMemory(order_res);
                        order_req.action   = TRADE_ACTION_DEAL;
                        order_req.symbol   = order_symbol;
                        order_req.price    = order_price;
                        order_req.volume   = this_order_lots;
                        order_req.type     = ORDER_TYPE_BUY;
                        order_req.position = order_pos;
                        order_req.type_filling = ORDER_FILLING_RETURN;
                        order_act = CTP::OrderSend(order_req,order_res);
#endif
                        // 报单频率限制
                        Sleep(10);
                       }
                     // 再平昨仓
                     if(order_lots>0 && pos_volume_yd>0)
                       {
                        // 本次报单量/平仓后的余量
                        double this_order_lots = 0;
                        if(order_lots > pos_volume_yd)
                          {
                           this_order_lots = (double)pos_volume_yd;
                           order_lots = order_lots - pos_volume_yd;
                          }
                        else
                          {
                           this_order_lots = order_lots;
                           order_lots = 0;
                          }
                        //
                        order_pos = -1;
                        bool order_act = false;
#ifdef __TRADE_CLASS__
                        order_act = m_trade.PositionClose(order_symbol,ORDER_TYPE_BUY,(long)this_order_lots,order_price,order_pos,0);
#else
                        ZeroMemory(order_req);
                        ZeroMemory(order_res);
                        order_req.action   = TRADE_ACTION_DEAL;
                        order_req.symbol   = order_symbol;
                        order_req.price    = order_price;
                        order_req.volume   = this_order_lots;
                        order_req.type     = ORDER_TYPE_BUY;
                        order_req.position = order_pos;
                        order_req.type_filling = ORDER_FILLING_RETURN;
                        //
                        order_act = CTP::OrderSend(order_req,order_res);
#endif
                        // 报单频率限制
                        Sleep(10);
                       }
                    }
                  // 开仓
                  if(order_lots>0)
                    {
                     bool order_act = false;
#ifdef __TRADE_CLASS__
                     order_act = m_trade.PositionOpen(order_symbol,ORDER_TYPE_BUY,(long)order_lots,order_price,0,order_sl,order_tp);
#else
                     ZeroMemory(order_req);
                     ZeroMemory(order_res);
                     order_req.action   = TRADE_ACTION_DEAL;
                     order_req.symbol   = order_symbol;
                     order_req.price    = order_price;
                     order_req.volume   = order_lots;
                     order_req.type     = ORDER_TYPE_BUY;
                     order_req.type_filling = ORDER_FILLING_RETURN;
                     if(order_sl>0)
                       {
                        order_req.sl    = order_sl;
                       }
                     if(order_tp>0)
                       {
                        order_req.tp    = order_tp;
                       }
                     order_act = CTP::OrderSend(order_req,order_res);
#endif
                    }
                  return;
                 }
              }
            else
              {
               // 报单价格/矫正
               double order_price = StringToDouble(m_price_text.GetValue());
               if(m_price_select0.IsPressed())
                 {
                  order_price = SymbolInfoDouble(order_symbol,SYMBOL_LAST);
                 }
               else
                  if(m_price_select1.IsPressed())
                    {
                     order_price = SymbolInfoDouble(order_symbol,SYMBOL_ASK);
                    }
                  else
                     if(m_price_select2.IsPressed())
                       {
                        order_price = CTP::SymbolInfoDouble(order_symbol,CTP::SYMBOL_UpperLimitPrice);
                       }
               // 报单数量
               double order_lots = StringToDouble(m_volume_text.GetValue());
               // 止损/止盈
               double order_sl = m_stoploss_text.IsPressed()?StringToDouble(m_stoploss_text.GetValue()):0;
               double order_tp = m_takeprofit_text.IsPressed()?StringToDouble(m_takeprofit_text.GetValue()):0;
               // 报单结构
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               order_req.action   = TRADE_ACTION_DEAL;
               order_req.symbol   = order_symbol;
               order_req.price    = order_price;
               order_req.volume   = order_lots;
               order_req.type     = ORDER_TYPE_BUY;
               order_req.type_filling = ORDER_FILLING_RETURN;
               if(order_sl>0)
                 {
                  order_req.sl    = order_sl;
                 }
               if(order_tp>0)
                 {
                  order_req.tp    = order_tp;
                 }
               MT5::OrderSend(order_req,order_res);
               return;
              }
           }
         return;
        }
      if(lparam==m_sell_button.Id())
        {
         string order_symbol = m_symbol_text.GetValue();
         bool is_custom;
         SymbolExist(order_symbol,is_custom);
         if(is_custom && CTP::AccountExists())
           {
            if(!m_position_mt.IsVisible())
              {
               // 报单结构
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               // 报单价格/矫正
               double order_price = StringToDouble(m_price_text.GetValue());
               if(m_price_select0.IsPressed())
                 {
                  order_price = SymbolInfoDouble(order_symbol,SYMBOL_LAST);
                 }
               else
                  if(m_price_select1.IsPressed())
                    {
                     order_price = SymbolInfoDouble(order_symbol,SYMBOL_BID);
                    }
                  else
                     if(m_price_select2.IsPressed())
                       {
                        order_price = CTP::SymbolInfoDouble(order_symbol,CTP::SYMBOL_LowerLimitPrice);
                       }
               // 报单数量
               double order_lots = StringToDouble(m_volume_text.GetValue());
               // 止损/止盈
               double order_sl = m_stoploss_text.IsPressed()?StringToDouble(m_stoploss_text.GetValue()):0;
               double order_tp = m_takeprofit_text.IsPressed()?StringToDouble(m_takeprofit_text.GetValue()):0;
               // 开平仓
               int order_pos = 0;   //----开仓\order_pos>0是平仓的持仓序号
               if(m_openclose_button.SelectedButtonIndex()==0)    //--卖出开仓
                 {
                  bool order_act = false;
#ifdef __TRADE_CLASS__
                  order_act = m_trade.PositionOpen(order_symbol,ORDER_TYPE_SELL,(long)order_lots,order_price,0,order_sl,order_tp);
#else
                  ZeroMemory(order_req);
                  ZeroMemory(order_res);
                  order_req.action   = TRADE_ACTION_DEAL;
                  order_req.symbol   = order_symbol;
                  order_req.price    = order_price;
                  order_req.volume   = order_lots;
                  order_req.type     = ORDER_TYPE_SELL;
                  order_req.type_filling = ORDER_FILLING_RETURN;
                  if(order_sl>0)
                    {
                     order_req.sl    = order_sl;
                    }
                  if(order_tp>0)
                    {
                     order_req.tp    = order_tp;
                    }
                  //
                  order_act = CTP::OrderSend(order_req,order_res);
#endif
                  return;
                 }
               if(m_openclose_button.SelectedButtonIndex()==1)  //--卖出平仓
                 {
                  order_pos = -1;
                  bool order_act = false;
#ifdef __TRADE_CLASS__
                  order_act = m_trade.PositionClose(order_symbol,ORDER_TYPE_SELL,(long)order_lots,order_price,order_pos,0);
#else
                  ZeroMemory(order_req);
                  ZeroMemory(order_res);
                  order_req.action   = TRADE_ACTION_DEAL;
                  order_req.symbol   = order_symbol;
                  order_req.price    = order_price;
                  order_req.volume   = order_lots;
                  order_req.type     = ORDER_TYPE_SELL;
                  order_req.position = order_pos;
                  order_req.type_filling = ORDER_FILLING_RETURN;
                  //
                  order_act = CTP::OrderSend(order_req,order_res);
#endif
                  return;
                 }
               if(m_openclose_button.SelectedButtonIndex()==2)  //--卖出平今仓
                 {
                  order_pos = -2;
                  bool order_act = false;
#ifdef __TRADE_CLASS__
                  order_act = m_trade.PositionClose(order_symbol,ORDER_TYPE_SELL,(long)order_lots,order_price,order_pos,0);
#else
                  ZeroMemory(order_req);
                  ZeroMemory(order_res);
                  order_req.action   = TRADE_ACTION_DEAL;
                  order_req.symbol   = order_symbol;
                  order_req.price    = order_price;
                  order_req.volume   = order_lots;
                  order_req.type     = ORDER_TYPE_SELL;
                  order_req.position = order_pos;
                  order_req.type_filling = ORDER_FILLING_RETURN;
                  //
                  order_act = CTP::OrderSend(order_req,order_res);
#endif
                  return;
                 }
               if(m_openclose_button.SelectedButtonIndex()==3)  //--先平后开
                 {
                  //查找多头持仓数量\持仓笔数
                  long pos_volume_td = 0;  //今仓量
                  long pos_volume_yd = 0;  //昨仓量
                  int pos_total  = 0;
                  for(int index=CTP::PositionsTotal(); index>0; index--)
                    {
                     if(!CTP::PositionSelect(index))
                        continue;
                     if(CTP::PositionGetSymbol()!=order_symbol)
                        continue;
                     long check_volume = CTP::PositionGetInteger(CTP::POSITION_Position);
                     if(check_volume<=0)
                        continue;
                     if(CTP::PositionGetInteger(CTP::POSITION_PosiDirection)==CTP::POSITION_TYPE_BUY)
                       {
                        if(CTP::PositionGetInteger(CTP::POSITION_PositionDate)=='1')
                          {
                           pos_volume_td += check_volume;
                           pos_total++;
                          }
                        else
                          {
                           pos_volume_yd += check_volume;
                           pos_total++;
                          }
                       }
                    }
                  // 平仓
                  if(pos_total>0)
                    {
                     // 先平今仓
                     if(order_lots>0 && pos_volume_td>0)
                       {
                        // 本次报单量/平今仓后的余量
                        double this_order_lots = 0;
                        if(order_lots > pos_volume_td)
                          {
                           this_order_lots = (double)pos_volume_td;
                           order_lots = order_lots - pos_volume_td;
                          }
                        else
                          {
                           this_order_lots = order_lots;
                           order_lots = 0;
                          }
                        //
                        order_pos = -2;
                        bool order_act = false;
#ifdef __TRADE_CLASS__
                        order_act = m_trade.PositionClose(order_symbol,ORDER_TYPE_SELL,(long)this_order_lots,order_price,order_pos,0);
#else
                        ZeroMemory(order_req);
                        ZeroMemory(order_res);
                        order_req.action   = TRADE_ACTION_DEAL;
                        order_req.symbol   = order_symbol;
                        order_req.price    = order_price;
                        order_req.volume   = this_order_lots;
                        order_req.type     = ORDER_TYPE_SELL;
                        order_req.position = order_pos;
                        order_req.type_filling = ORDER_FILLING_RETURN;
                        //
                        order_act = CTP::OrderSend(order_req,order_res);
#endif
                        // 报单频率限制
                        Sleep(10);
                       }
                     // 再平昨仓
                     if(order_lots>0 && pos_volume_yd>0)
                       {
                        // 本次报单量/平仓后的余量
                        double this_order_lots = 0;
                        if(order_lots > pos_volume_yd)
                          {
                           this_order_lots = (double)pos_volume_yd;
                           order_lots = order_lots - pos_volume_yd;
                          }
                        else
                          {
                           this_order_lots = order_lots;
                           order_lots = 0;
                          }
                        //
                        order_pos = -1;
                        bool order_act = false;
#ifdef __TRADE_CLASS__
                        order_act = m_trade.PositionClose(order_symbol,ORDER_TYPE_SELL,(long)this_order_lots,order_price,order_pos,0);
#else
                        ZeroMemory(order_req);
                        ZeroMemory(order_res);
                        order_req.action   = TRADE_ACTION_DEAL;
                        order_req.symbol   = order_symbol;
                        order_req.price    = order_price;
                        order_req.volume   = this_order_lots;
                        order_req.type     = ORDER_TYPE_SELL;
                        order_req.position = order_pos;
                        order_req.type_filling = ORDER_FILLING_RETURN;
                        //
                        order_act = CTP::OrderSend(order_req,order_res);
#endif
                        // 报单频率限制
                        Sleep(10);
                       }
                    }
                  // 开仓
                  if(order_lots>0)
                    {
                     bool order_act = false;
#ifdef __TRADE_CLASS__
                     order_act = m_trade.PositionOpen(order_symbol,ORDER_TYPE_SELL,(long)order_lots,order_price,0,order_sl,order_tp);
#else
                     ZeroMemory(order_req);
                     ZeroMemory(order_res);
                     order_req.action   = TRADE_ACTION_DEAL;
                     order_req.symbol   = order_symbol;
                     order_req.price    = order_price;
                     order_req.volume   = order_lots;
                     order_req.type     = ORDER_TYPE_SELL;
                     order_req.type_filling = ORDER_FILLING_RETURN;
                     if(order_sl>0)
                       {
                        order_req.sl    = order_sl;
                       }
                     if(order_tp>0)
                       {
                        order_req.tp    = order_tp;
                       }
                     //
                     order_act = CTP::OrderSend(order_req,order_res);
#endif
                    }
                  return;
                 }
              }
            else
              {
               // 报单价格/矫正
               double order_price = StringToDouble(m_price_text.GetValue());
               if(m_price_select0.IsPressed())
                 {
                  order_price = SymbolInfoDouble(order_symbol,SYMBOL_LAST);
                 }
               else
                  if(m_price_select1.IsPressed())
                    {
                     order_price = SymbolInfoDouble(order_symbol,SYMBOL_ASK);
                    }
                  else
                     if(m_price_select2.IsPressed())
                       {
                        order_price = CTP::SymbolInfoDouble(order_symbol,CTP::SYMBOL_UpperLimitPrice);
                       }
               // 报单数量
               double order_lots = StringToDouble(m_volume_text.GetValue());
               // 止损/止盈
               double order_sl = m_stoploss_text.IsPressed()?StringToDouble(m_stoploss_text.GetValue()):0;
               double order_tp = m_takeprofit_text.IsPressed()?StringToDouble(m_takeprofit_text.GetValue()):0;
               // 报单结构
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               order_req.action   = TRADE_ACTION_DEAL;
               order_req.symbol   = order_symbol;
               order_req.price    = order_price;
               order_req.volume   = order_lots;
               order_req.type     = ORDER_TYPE_SELL;
               order_req.type_filling = ORDER_FILLING_RETURN;
               if(order_sl>0)
                 {
                  order_req.sl    = order_sl;
                 }
               if(order_tp>0)
                 {
                  order_req.tp    = order_tp;
                 }
               MT5::OrderSend(order_req,order_res);
               return;
              }
           }
         return;
        }
      if(lparam==m_pos_add_button.Id())
        {
         string order_symbol = m_symbol_text.GetValue();
         bool is_custom;
         SymbolExist(order_symbol,is_custom);
         if(is_custom && CTP::AccountExists())
           {
            if(!m_position_mt.IsVisible())
              {
               // 读取当前持仓信息
               int row_pos = m_position_table.SelectedItem();
               string pos_ticket = m_position_table.GetValue(0,row_pos);
               char   buysell = '9';
               if(CTP::PositionSelectByTicket(pos_ticket))
                 {
                  buysell = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection);
                 }
               string account = CTP::AccountInfoString(CTP::ACCOUNT_AccountID);
               string exchange = SymbolInfoString(order_symbol,SYMBOL_EXCHANGE);
               string product = CTP::SymbolInfoString(order_symbol,CTP::SYMBOL_ProductID);
               // 报单
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               // 是否持仓筛选/遍历持仓/按筛选条件执行操作
               bool cond_check = m_select_account.IsPressed()  ||
                                 m_select_exchange.IsPressed() ||
                                 m_select_product.IsPressed()  ||
                                 m_select_symbol.IsPressed()   ||
                                 m_select_buysell.IsPressed()  ;

               if(cond_check)
                 {
                  for(int index = CTP::PositionsTotal(); index>0; index--)
                    {
                     if(!CTP::PositionSelect(index))
                        continue;
                     long pos_volume = CTP::PositionGetInteger(CTP::POSITION_Position);
                     if(pos_volume<=0)
                        continue;
                     string pos_symbol = CTP::PositionGetSymbol();
                     // 账户删选
                     bool pos_check = false;
                     if(m_select_account.IsPressed())
                       {
                        bool this_check = CTP::PositionGetString(CTP::POSITION_InvestorID) == account;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 交易所删选
                     if(m_select_exchange.IsPressed())
                       {
                        bool this_check = CTP::PositionGetString(CTP::POSITION_ExchangeID) == exchange;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 品种删选
                     if(m_select_product.IsPressed())
                       {
                        bool this_check = CTP::SymbolInfoString(pos_symbol,CTP::SYMBOL_ProductID) == product;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 合约删选
                     if(m_select_symbol.IsPressed())
                       {
                        bool this_check = pos_symbol == order_symbol;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 方向筛选
                     if(m_select_buysell.IsPressed())
                       {
                        bool this_check = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection) == buysell;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 通过检查/报单
                     if(pos_check)
                       {
                        //
                        buysell = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection);
                        ENUM_ORDER_TYPE order_type = (buysell==CTP::POSITION_TYPE_BUY)?ORDER_TYPE_BUY:ORDER_TYPE_SELL;
                        double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(pos_symbol,SYMBOL_ASK):SymbolInfoDouble(pos_symbol,SYMBOL_BID);
                        double pos_sl = CTP::PositionGetDouble(CTP::POSITION_StopLoss);
                        double pos_tp = CTP::PositionGetDouble(CTP::POSITION_TakeProfit);
                        bool order_act = false;
#ifdef __TRADE_CLASS__
                        order_act = m_trade.PositionOpen(pos_symbol,order_type,pos_volume,order_price,0,pos_sl,pos_tp);
#else
                        ZeroMemory(order_req);
                        ZeroMemory(order_res);
                        order_req.action   = TRADE_ACTION_DEAL;
                        order_req.symbol   = pos_symbol;
                        order_req.price    = order_price;
                        order_req.volume   = (double)pos_volume;
                        order_req.type     = order_type;
                        order_req.type_filling = ORDER_FILLING_RETURN;
                        if(pos_sl>0)
                          {
                           order_req.sl    = pos_sl;
                          }
                        if(pos_tp>0)
                          {
                           order_req.tp    = pos_tp;
                          }
                        //
                        order_act = CTP::OrderSend(order_req,order_res);
#endif
                        // 报单频率限制
                        Sleep(10);
                       }
                    }
                 }
               else  // 选中操作
                 {
                  //
                  ENUM_ORDER_TYPE order_type = (buysell==CTP::POSITION_TYPE_BUY)?ORDER_TYPE_BUY:ORDER_TYPE_SELL;
                  double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(order_symbol,SYMBOL_ASK):SymbolInfoDouble(order_symbol,SYMBOL_BID);
                  // 止损/止盈
                  double order_sl = m_stoploss_text.IsPressed()?StringToDouble(m_stoploss_text.GetValue()):0;
                  double order_tp = m_takeprofit_text.IsPressed()?StringToDouble(m_takeprofit_text.GetValue()):0;
                  bool order_act = false;
#ifdef __TRADE_CLASS__
                  order_act = m_trade.PositionOpen(order_symbol,order_type,StringToInteger(m_volume_text.GetValue()),order_price,0,order_sl,order_tp);
#else
                  ZeroMemory(order_req);
                  ZeroMemory(order_res);
                  order_req.action   = TRADE_ACTION_DEAL;
                  order_req.symbol   = order_symbol;
                  order_req.price    = order_price;
                  order_req.volume   = StringToDouble(m_volume_text.GetValue());
                  order_req.type     = order_type;
                  order_req.type_filling = ORDER_FILLING_RETURN;
                  if(order_sl>0)
                    {
                     order_req.sl    = order_sl;
                    }
                  if(order_tp>0)
                    {
                     order_req.tp    = order_tp;
                    }
                  //
                  order_act = CTP::OrderSend(order_req,order_res);
#endif
                 }
               return;
              }
            else
              {
               // 读取当前持仓信息
               int row_pos = m_position_mt.SelectedItem();
               ulong ticket = StringToInteger(m_position_mt.GetValue(1,row_pos));
               if(!MT5::PositionSelectByTicket(ticket))
                  return;
               ulong magic_number = MT5::PositionGetInteger(POSITION_MAGIC);
               string exchange = SymbolInfoString(order_symbol,SYMBOL_EXCHANGE);
               string product = SymbolInfoString(order_symbol,SYMBOL_CATEGORY);
               ENUM_POSITION_TYPE type = (ENUM_POSITION_TYPE)MT5::PositionGetInteger(POSITION_TYPE);
               // 报单
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               // 是否持仓筛选/遍历持仓/按筛选条件执行操作
               bool cond_check = m_select_magic.IsPressed()  ||
                                 m_select_exchange.IsPressed() ||
                                 m_select_product.IsPressed()  ||
                                 m_select_symbol.IsPressed()   ||
                                 m_select_buysell.IsPressed()  ;

               if(cond_check)
                 {
                  for(int index = MT5::PositionsTotal()-1; index>=0; index--)
                    {
                     ulong pos_ticket = MT5::PositionGetTicket(index);
                     if(!MT5::PositionSelectByTicket(pos_ticket))
                        continue;
                     string pos_symbol = MT5::PositionGetString(POSITION_SYMBOL);
                     // 账户删选
                     bool pos_check = false;
                     if(m_select_magic.IsPressed())
                       {
                        bool this_check = MT5::PositionGetInteger(POSITION_MAGIC) == magic_number && magic_number!=NULL;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 交易所删选
                     if(m_select_exchange.IsPressed())
                       {
                        bool this_check = SymbolInfoString(pos_symbol,SYMBOL_EXCHANGE) == exchange;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 品种删选
                     if(m_select_product.IsPressed())
                       {
                        bool this_check = SymbolInfoString(pos_symbol,SYMBOL_CATEGORY) == product;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 合约删选
                     if(m_select_symbol.IsPressed())
                       {
                        bool this_check = pos_symbol == order_symbol;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 方向筛选
                     if(m_select_buysell.IsPressed())
                       {
                        bool this_check = MT5::PositionGetInteger(POSITION_TYPE) == type;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 通过检查/报单
                     if(pos_check)
                       {
                        ENUM_ORDER_TYPE order_type = (MT5::PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY)?ORDER_TYPE_BUY:ORDER_TYPE_SELL;
                        double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(pos_symbol,SYMBOL_ASK):SymbolInfoDouble(pos_symbol,SYMBOL_BID);
                        double pos_sl = MT5::PositionGetDouble(POSITION_SL);
                        double pos_tp = MT5::PositionGetDouble(POSITION_TP);
                        ZeroMemory(order_req);
                        ZeroMemory(order_res);
                        order_req.action   = TRADE_ACTION_DEAL;
                        order_req.symbol   = pos_symbol;
                        order_req.price    = order_price;
                        order_req.volume   = MT5::PositionGetDouble(POSITION_VOLUME);
                        order_req.type     = order_type;
                        order_req.magic    = MT5::PositionGetInteger(POSITION_MAGIC);
                        order_req.type_filling = ORDER_FILLING_RETURN;
                        order_req.sl       = pos_sl;
                        order_req.tp       = pos_tp;
                        //
                        MT5::OrderSend(order_req,order_res);
                        // 报单频率限制
                        Sleep(10);
                       }
                    }
                 }
               else  // 选中操作
                 {
                  //
                  ENUM_ORDER_TYPE order_type = (type==POSITION_TYPE_BUY)?ORDER_TYPE_BUY:ORDER_TYPE_SELL;
                  double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(order_symbol,SYMBOL_ASK):SymbolInfoDouble(order_symbol,SYMBOL_BID);
                  // 止损/止盈
                  double order_sl = MT5::PositionGetDouble(POSITION_SL);
                  double order_tp = MT5::PositionGetDouble(POSITION_TP);
                  ZeroMemory(order_req);
                  ZeroMemory(order_res);
                  order_req.action   = TRADE_ACTION_DEAL;
                  order_req.symbol   = order_symbol;
                  order_req.price    = order_price;
                  order_req.volume   = MT5::PositionGetDouble(POSITION_VOLUME);
                  order_req.type     = order_type;
                  order_req.magic    = MT5::PositionGetInteger(POSITION_MAGIC);
                  order_req.type_filling = ORDER_FILLING_RETURN;
                  order_req.sl       = order_sl;
                  order_req.tp       = order_tp;
                  //
                  MT5::OrderSend(order_req,order_res);
                 }
               return;
              }
           }
         return;
        }
      if(lparam==m_pos_sub_button.Id())
        {
         string order_symbol = m_symbol_text.GetValue();
         bool is_custom;
         SymbolExist(order_symbol,is_custom);
         if(is_custom && CTP::AccountExists())
           {
            if(!m_position_mt.IsVisible())
              {
               // 读取当前持仓信息
               int row_pos = m_position_table.SelectedItem();
               string pos_ticket = m_position_table.GetValue(0,row_pos);
               char   buysell = '9';
               if(CTP::PositionSelectByTicket(pos_ticket))
                 {
                  buysell = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection);
                 }
               string account = CTP::AccountInfoString(CTP::ACCOUNT_AccountID);
               string exchange = SymbolInfoString(order_symbol,SYMBOL_EXCHANGE);
               string product = CTP::SymbolInfoString(order_symbol,CTP::SYMBOL_ProductID);
               // 报单
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               // 是否持仓筛选/遍历持仓/按筛选条件执行操作
               bool cond_check = m_select_account.IsPressed()  ||
                                 m_select_exchange.IsPressed() ||
                                 m_select_product.IsPressed()  ||
                                 m_select_symbol.IsPressed()   ||
                                 m_select_buysell.IsPressed()  ;

               if(cond_check)
                 {
                  for(int index = CTP::PositionsTotal(); index>0; index--)
                    {
                     if(!CTP::PositionSelect(index))
                        continue;
                     long pos_volume = CTP::PositionGetInteger(CTP::POSITION_Position);
                     if(pos_volume<=0)
                        continue;
                     string pos_symbol = CTP::PositionGetSymbol();
                     // 账户删选
                     bool pos_check = false;
                     if(m_select_account.IsPressed())
                       {
                        bool this_check = CTP::PositionGetString(CTP::POSITION_InvestorID) == account;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 交易所删选
                     if(m_select_exchange.IsPressed())
                       {
                        bool this_check = CTP::PositionGetString(CTP::POSITION_ExchangeID) == exchange;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 品种删选
                     if(m_select_product.IsPressed())
                       {
                        bool this_check = CTP::SymbolInfoString(pos_symbol,CTP::SYMBOL_ProductID) == product;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 合约删选
                     if(m_select_symbol.IsPressed())
                       {
                        bool this_check = pos_symbol == order_symbol;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 方向筛选
                     if(m_select_buysell.IsPressed())
                       {
                        bool this_check = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection) == buysell;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 通过检查/报单
                     if(pos_check)
                       {
                        //
                        buysell = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection);
                        ENUM_ORDER_TYPE order_type = (buysell==CTP::POSITION_TYPE_BUY)?ORDER_TYPE_SELL:ORDER_TYPE_BUY;
                        double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(pos_symbol,SYMBOL_ASK):SymbolInfoDouble(pos_symbol,SYMBOL_BID);
                        bool order_act = false;
#ifdef __TRADE_CLASS__
                        order_act = m_trade.PositionClose(index,(long)MathFloor(pos_volume*0.5),order_price,0);
#else
                        ZeroMemory(order_req);
                        ZeroMemory(order_res);
                        order_req.action   = TRADE_ACTION_DEAL;
                        order_req.symbol   = pos_symbol;
                        order_req.price    = order_price;
                        order_req.volume   = MathFloor(pos_volume*0.5);
                        order_req.type     = order_type;
                        order_req.type_filling = ORDER_FILLING_RETURN;
                        order_req.position = index;
                        //
                        order_act = CTP::OrderSend(order_req,order_res);
#endif
                        // 报单频率限制
                        Sleep(10);
                       }
                    }
                 }
               else  // 选中操作
                 {
                  // 找到对应的index
                  int index = CTP::PositionsTotal();
                  for(; index>0; index--)
                    {
                     if(pos_ticket==CTP::PositionGetTicket(index))
                       {
                        break;
                       }
                    }
                  ENUM_ORDER_TYPE order_type = (buysell==CTP::POSITION_TYPE_BUY)?ORDER_TYPE_SELL:ORDER_TYPE_BUY;
                  double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(order_symbol,SYMBOL_ASK):SymbolInfoDouble(order_symbol,SYMBOL_BID);
                  bool order_act = false;
#ifdef __TRADE_CLASS__
                  order_act = m_trade.PositionClose(index,(long)MathFloor(StringToDouble(m_volume_text.GetValue())*0.5),order_price,0);
#else
                  ZeroMemory(order_req);
                  ZeroMemory(order_res);
                  order_req.action   = TRADE_ACTION_DEAL;
                  order_req.symbol   = order_symbol;
                  order_req.price    = order_price;
                  order_req.volume   = MathFloor(StringToDouble(m_volume_text.GetValue())*0.5);
                  order_req.type     = order_type;
                  order_req.type_filling = ORDER_FILLING_RETURN;
                  order_req.position = index;
                  //
                  order_act = CTP::OrderSend(order_req,order_res);
#endif
                 }
               return;
              }
            else
              {
               // 读取当前持仓信息
               int row_pos = m_position_mt.SelectedItem();
               ulong ticket = StringToInteger(m_position_mt.GetValue(1,row_pos));
               if(!MT5::PositionSelectByTicket(ticket))
                  return;
               ulong magic_number = MT5::PositionGetInteger(POSITION_MAGIC);
               string exchange = SymbolInfoString(order_symbol,SYMBOL_EXCHANGE);
               string product = SymbolInfoString(order_symbol,SYMBOL_CATEGORY);
               ENUM_POSITION_TYPE type = (ENUM_POSITION_TYPE)MT5::PositionGetInteger(POSITION_TYPE);
               // 报单
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               // 是否持仓筛选/遍历持仓/按筛选条件执行操作
               bool cond_check = m_select_magic.IsPressed()  ||
                                 m_select_exchange.IsPressed() ||
                                 m_select_product.IsPressed()  ||
                                 m_select_symbol.IsPressed()   ||
                                 m_select_buysell.IsPressed()  ;

               if(cond_check)
                 {
                  for(int index = MT5::PositionsTotal()-1; index>=0; index--)
                    {
                     ulong pos_ticket = MT5::PositionGetTicket(index);
                     if(!MT5::PositionSelectByTicket(pos_ticket))
                        continue;
                     string pos_symbol = MT5::PositionGetString(POSITION_SYMBOL);
                     // 账户删选
                     bool pos_check = false;
                     if(m_select_magic.IsPressed())
                       {
                        bool this_check = MT5::PositionGetInteger(POSITION_MAGIC) == magic_number && magic_number!=NULL;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 交易所删选
                     if(m_select_exchange.IsPressed())
                       {
                        bool this_check = SymbolInfoString(pos_symbol,SYMBOL_EXCHANGE) == exchange;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 品种删选
                     if(m_select_product.IsPressed())
                       {
                        bool this_check = SymbolInfoString(pos_symbol,SYMBOL_CATEGORY) == product;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 合约删选
                     if(m_select_symbol.IsPressed())
                       {
                        bool this_check = pos_symbol == order_symbol;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 方向筛选
                     if(m_select_buysell.IsPressed())
                       {
                        bool this_check = MT5::PositionGetInteger(POSITION_TYPE) == type;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 通过检查/报单
                     if(pos_check)
                       {
                        ENUM_ORDER_TYPE order_type = (MT5::PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY)?ORDER_TYPE_SELL:ORDER_TYPE_BUY;
                        double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(pos_symbol,SYMBOL_ASK):SymbolInfoDouble(pos_symbol,SYMBOL_BID);
                        ZeroMemory(order_req);
                        ZeroMemory(order_res);
                        order_req.action   = TRADE_ACTION_DEAL;
                        order_req.symbol   = pos_symbol;
                        order_req.price    = order_price;
                        order_req.volume   = MathFloor(MT5::PositionGetDouble(POSITION_VOLUME)*0.5);
                        order_req.position = pos_ticket;
                        order_req.type_filling = ORDER_FILLING_RETURN;
                        //
                        MT5::OrderSend(order_req,order_res);
                        // 报单频率限制
                        Sleep(10);
                       }
                    }
                 }
               else  // 选中操作
                 {
                  //
                  ENUM_ORDER_TYPE order_type = (type==POSITION_TYPE_BUY)?ORDER_TYPE_SELL:ORDER_TYPE_BUY;
                  double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(order_symbol,SYMBOL_ASK):SymbolInfoDouble(order_symbol,SYMBOL_BID);
                  ZeroMemory(order_req);
                  ZeroMemory(order_res);
                  order_req.action   = TRADE_ACTION_DEAL;
                  order_req.symbol   = order_symbol;
                  order_req.price    = order_price;
                  order_req.volume   = MathFloor(MT5::PositionGetDouble(POSITION_VOLUME)*0.5);
                  order_req.position = ticket;
                  order_req.type_filling = ORDER_FILLING_RETURN;
                  //
                  MT5::OrderSend(order_req,order_res);
                 }
               return;
              }
           }
         return;
        }
      if(lparam==m_pos_cls_button.Id())
        {
         string order_symbol = m_symbol_text.GetValue();
         bool is_custom;
         SymbolExist(order_symbol,is_custom);
         if(is_custom && CTP::AccountExists())
           {
            if(!m_position_mt.IsVisible())
              {
               // 读取当前持仓信息
               int row_pos = m_position_table.SelectedItem();
               string pos_ticket = m_position_table.GetValue(0,row_pos);
               char   buysell = '9';
               char   tdyd = '9';
               if(CTP::PositionSelectByTicket(pos_ticket))
                 {
                  buysell = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection);
                  tdyd = (char)CTP::PositionGetInteger(CTP::POSITION_PositionDate);
                 }
               string account = CTP::AccountInfoString(CTP::ACCOUNT_AccountID);
               string exchange = SymbolInfoString(order_symbol,SYMBOL_EXCHANGE);
               string product = CTP::SymbolInfoString(order_symbol,CTP::SYMBOL_ProductID);
               // 报单
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               // 是否持仓筛选/遍历持仓/按筛选条件执行操作
               bool cond_check = m_select_account.IsPressed()  ||
                                 m_select_exchange.IsPressed() ||
                                 m_select_product.IsPressed()  ||
                                 m_select_symbol.IsPressed()   ||
                                 m_select_buysell.IsPressed()  ;

               if(cond_check)
                 {
                  for(int index = CTP::PositionsTotal(); index>0; index--)
                    {
                     if(!CTP::PositionSelect(index))
                        continue;
                     long pos_volume = CTP::PositionGetInteger(CTP::POSITION_Position);
                     if(pos_volume<=0)
                        continue;
                     string pos_symbol = CTP::PositionGetSymbol();
                     // 账户删选
                     bool pos_check = false;
                     if(m_select_account.IsPressed())
                       {
                        bool this_check = CTP::PositionGetString(CTP::POSITION_InvestorID) == account;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 交易所删选
                     if(m_select_exchange.IsPressed())
                       {
                        bool this_check = CTP::PositionGetString(CTP::POSITION_ExchangeID) == exchange;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 品种删选
                     if(m_select_product.IsPressed())
                       {
                        bool this_check = CTP::SymbolInfoString(pos_symbol,CTP::SYMBOL_ProductID) == product;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 合约删选
                     if(m_select_symbol.IsPressed())
                       {
                        bool this_check = pos_symbol == order_symbol;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 方向筛选
                     if(m_select_buysell.IsPressed())
                       {
                        bool this_check = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection) == buysell;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 通过检查/报单
                     if(pos_check)
                       {
                        //
                        buysell = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection);
                        ENUM_ORDER_TYPE order_type = (buysell==CTP::POSITION_TYPE_BUY)?ORDER_TYPE_SELL:ORDER_TYPE_BUY;
                        double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(pos_symbol,SYMBOL_ASK):SymbolInfoDouble(pos_symbol,SYMBOL_BID);
                        int order_pos = -1;
                        if(exchange == "SHFE" || exchange == "INE")
                          {
                           if((char)CTP::PositionGetInteger(CTP::POSITION_PositionDate)=='1')
                             {
                              order_pos = -2;
                             }
                          }
                        bool order_act = false;
#ifdef __TRADE_CLASS__
                        order_act = m_trade.PositionClose(pos_symbol,order_type,pos_volume,order_price,order_pos,0);
#else
                        ZeroMemory(order_req);
                        ZeroMemory(order_res);
                        order_req.action   = TRADE_ACTION_DEAL;
                        order_req.symbol   = pos_symbol;
                        order_req.price    = order_price;
                        order_req.volume   = (double)pos_volume;
                        order_req.type     = order_type;
                        order_req.type_filling = ORDER_FILLING_RETURN;
                        order_req.position = order_pos;
                        //
                        order_act = CTP::OrderSend(order_req,order_res);
#endif
                        // 报单频率限制
                        Sleep(10);
                       }
                    }
                 }
               else  // 选中操作
                 {
                  //
                  ENUM_ORDER_TYPE order_type = (buysell==CTP::POSITION_TYPE_BUY)?ORDER_TYPE_SELL:ORDER_TYPE_BUY;
                  double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(order_symbol,SYMBOL_ASK):SymbolInfoDouble(order_symbol,SYMBOL_BID);
                  int order_pos = -1;
                  if(exchange == "SHFE" || exchange == "INE")
                    {
                     if(tdyd == '1')
                       {
                        order_pos = -2;
                       }
                    }
                  bool order_act = false;
#ifdef __TRADE_CLASS__
                  order_act = m_trade.PositionClose(order_symbol,order_type,StringToInteger(m_volume_text.GetValue()),order_price,order_pos,0);
#else
                  ZeroMemory(order_req);
                  ZeroMemory(order_res);
                  order_req.action   = TRADE_ACTION_DEAL;
                  order_req.symbol   = order_symbol;
                  order_req.price    = order_price;
                  order_req.volume   = StringToDouble(m_volume_text.GetValue());
                  order_req.type     = order_type;
                  order_req.type_filling = ORDER_FILLING_RETURN;
                  order_req.position = order_pos;
                  //
                  order_act = CTP::OrderSend(order_req,order_res);
#endif
                 }
               return;
              }
            else
              {
               // 读取当前持仓信息
               int row_pos = m_position_mt.SelectedItem();
               ulong ticket = StringToInteger(m_position_mt.GetValue(1,row_pos));
               if(!MT5::PositionSelectByTicket(ticket))
                  return;
               ulong magic_number = MT5::PositionGetInteger(POSITION_MAGIC);
               string exchange = SymbolInfoString(order_symbol,SYMBOL_EXCHANGE);
               string product = SymbolInfoString(order_symbol,SYMBOL_CATEGORY);
               ENUM_POSITION_TYPE type = (ENUM_POSITION_TYPE)MT5::PositionGetInteger(POSITION_TYPE);
               // 报单
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               // 是否持仓筛选/遍历持仓/按筛选条件执行操作
               bool cond_check = m_select_magic.IsPressed()  ||
                                 m_select_exchange.IsPressed() ||
                                 m_select_product.IsPressed()  ||
                                 m_select_symbol.IsPressed()   ||
                                 m_select_buysell.IsPressed()  ;

               if(cond_check)
                 {
                  for(int index = MT5::PositionsTotal()-1; index>=0; index--)
                    {
                     ulong pos_ticket = MT5::PositionGetTicket(index);
                     if(!MT5::PositionSelectByTicket(pos_ticket))
                        continue;
                     string pos_symbol = MT5::PositionGetString(POSITION_SYMBOL);
                     // 账户删选
                     bool pos_check = false;
                     if(m_select_magic.IsPressed())
                       {
                        bool this_check = MT5::PositionGetInteger(POSITION_MAGIC) == magic_number && magic_number!=NULL;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 交易所删选
                     if(m_select_exchange.IsPressed())
                       {
                        bool this_check = SymbolInfoString(pos_symbol,SYMBOL_EXCHANGE) == exchange;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 品种删选
                     if(m_select_product.IsPressed())
                       {
                        bool this_check = SymbolInfoString(pos_symbol,SYMBOL_CATEGORY) == product;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 合约删选
                     if(m_select_symbol.IsPressed())
                       {
                        bool this_check = pos_symbol == order_symbol;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 方向筛选
                     if(m_select_buysell.IsPressed())
                       {
                        bool this_check = MT5::PositionGetInteger(POSITION_TYPE) == type;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 通过检查/报单
                     if(pos_check)
                       {
                        ENUM_ORDER_TYPE order_type = (MT5::PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY)?ORDER_TYPE_SELL:ORDER_TYPE_BUY;
                        double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(pos_symbol,SYMBOL_ASK):SymbolInfoDouble(pos_symbol,SYMBOL_BID);
                        ZeroMemory(order_req);
                        ZeroMemory(order_res);
                        order_req.action   = TRADE_ACTION_DEAL;
                        order_req.symbol   = pos_symbol;
                        order_req.price    = order_price;
                        order_req.volume   = MT5::PositionGetDouble(POSITION_VOLUME);
                        order_req.position = pos_ticket;
                        order_req.type_filling = ORDER_FILLING_RETURN;
                        //
                        MT5::OrderSend(order_req,order_res);
                        // 报单频率限制
                        Sleep(10);
                       }
                    }
                 }
               else  // 选中操作
                 {
                  //
                  ENUM_ORDER_TYPE order_type = (type==POSITION_TYPE_BUY)?ORDER_TYPE_SELL:ORDER_TYPE_BUY;
                  double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(order_symbol,SYMBOL_ASK):SymbolInfoDouble(order_symbol,SYMBOL_BID);
                  ZeroMemory(order_req);
                  ZeroMemory(order_res);
                  order_req.action   = TRADE_ACTION_DEAL;
                  order_req.symbol   = order_symbol;
                  order_req.price    = order_price;
                  order_req.volume   = MT5::PositionGetDouble(POSITION_VOLUME);
                  order_req.position = ticket;
                  order_req.type_filling = ORDER_FILLING_RETURN;
                  //
                  MT5::OrderSend(order_req,order_res);
                 }
               return;
              }
           }
         return;
        }
      if(lparam==m_pos_hedg_button.Id())
        {
         string order_symbol = m_symbol_text.GetValue();
         bool is_custom;
         SymbolExist(order_symbol,is_custom);
         if(is_custom && CTP::AccountExists())
           {
            if(!m_position_mt.IsVisible())
              {
               // 读取当前持仓信息
               int row_pos = m_position_table.SelectedItem();
               string pos_ticket = m_position_table.GetValue(0,row_pos);
               char   buysell = '9';
               if(CTP::PositionSelectByTicket(pos_ticket))
                 {
                  buysell = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection);
                 }
               string account = CTP::AccountInfoString(CTP::ACCOUNT_AccountID);
               string exchange = SymbolInfoString(order_symbol,SYMBOL_EXCHANGE);
               string product = CTP::SymbolInfoString(order_symbol,CTP::SYMBOL_ProductID);
               // 报单
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               // 是否持仓筛选/遍历持仓/按筛选条件执行操作
               bool cond_check = m_select_account.IsPressed()  ||
                                 m_select_exchange.IsPressed() ||
                                 m_select_product.IsPressed()  ||
                                 m_select_symbol.IsPressed()   ||
                                 m_select_buysell.IsPressed()  ;

               if(cond_check)
                 {
                  for(int index = CTP::PositionsTotal(); index>0; index--)
                    {
                     if(!CTP::PositionSelect(index))
                        continue;
                     long pos_volume = CTP::PositionGetInteger(CTP::POSITION_Position);
                     if(pos_volume<=0)
                        continue;
                     string pos_symbol = CTP::PositionGetSymbol();
                     // 账户删选
                     bool pos_check = false;
                     if(m_select_account.IsPressed())
                       {
                        bool this_check = CTP::PositionGetString(CTP::POSITION_InvestorID) == account;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 交易所删选
                     if(m_select_exchange.IsPressed())
                       {
                        bool this_check = CTP::PositionGetString(CTP::POSITION_ExchangeID) == exchange;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 品种删选
                     if(m_select_product.IsPressed())
                       {
                        bool this_check = CTP::SymbolInfoString(pos_symbol,CTP::SYMBOL_ProductID) == product;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 合约删选
                     if(m_select_symbol.IsPressed())
                       {
                        bool this_check = pos_symbol == order_symbol;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 方向筛选
                     if(m_select_buysell.IsPressed())
                       {
                        bool this_check = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection) == buysell;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 通过检查/报单
                     if(pos_check)
                       {
                        //
                        buysell = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection);
                        ENUM_ORDER_TYPE order_type = (buysell==CTP::POSITION_TYPE_BUY)?ORDER_TYPE_SELL:ORDER_TYPE_BUY;
                        double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(pos_symbol,SYMBOL_ASK):SymbolInfoDouble(pos_symbol,SYMBOL_BID);
                        bool order_act = false;
#ifdef __TRADE_CLASS__
                        order_act = m_trade.PositionOpen(pos_symbol,order_type,pos_volume,order_price,0);
#else
                        ZeroMemory(order_req);
                        ZeroMemory(order_res);
                        order_req.action   = TRADE_ACTION_DEAL;
                        order_req.symbol   = pos_symbol;
                        order_req.price    = order_price;
                        order_req.volume   = (double)pos_volume;
                        order_req.type     = order_type;
                        order_req.type_filling = ORDER_FILLING_RETURN;
                        //
                        order_act = CTP::OrderSend(order_req,order_res);
#endif
                        // 报单频率限制
                        Sleep(10);
                       }
                    }
                 }
               else  // 选中操作
                 {
                  //
                  ENUM_ORDER_TYPE order_type = (buysell==CTP::POSITION_TYPE_BUY)?ORDER_TYPE_SELL:ORDER_TYPE_BUY;
                  double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(order_symbol,SYMBOL_ASK):SymbolInfoDouble(order_symbol,SYMBOL_BID);
                  bool order_act = false;
#ifdef __TRADE_CLASS__
                  order_act = m_trade.PositionOpen(order_symbol,order_type,StringToInteger(m_volume_text.GetValue()),order_price,0);
#else
                  ZeroMemory(order_req);
                  ZeroMemory(order_res);
                  order_req.action   = TRADE_ACTION_DEAL;
                  order_req.symbol   = order_symbol;
                  order_req.price    = order_price;
                  order_req.volume   = StringToDouble(m_volume_text.GetValue());
                  order_req.type     = order_type;
                  order_req.type_filling = ORDER_FILLING_RETURN;
                  //
                  order_act = CTP::OrderSend(order_req,order_res);
#endif
                 }
               return;
              }
            else
              {
               // 读取当前持仓信息
               int row_pos = m_position_mt.SelectedItem();
               ulong ticket = StringToInteger(m_position_mt.GetValue(1,row_pos));
               if(!MT5::PositionSelectByTicket(ticket))
                  return;
               ulong magic_number = MT5::PositionGetInteger(POSITION_MAGIC);
               string exchange = SymbolInfoString(order_symbol,SYMBOL_EXCHANGE);
               string product = SymbolInfoString(order_symbol,SYMBOL_CATEGORY);
               ENUM_POSITION_TYPE type = (ENUM_POSITION_TYPE)MT5::PositionGetInteger(POSITION_TYPE);
               // 报单
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               // 是否持仓筛选/遍历持仓/按筛选条件执行操作
               bool cond_check = m_select_magic.IsPressed()  ||
                                 m_select_exchange.IsPressed() ||
                                 m_select_product.IsPressed()  ||
                                 m_select_symbol.IsPressed()   ||
                                 m_select_buysell.IsPressed()  ;

               if(cond_check)
                 {
                  for(int index = MT5::PositionsTotal()-1; index>=0; index--)
                    {
                     ulong pos_ticket = MT5::PositionGetTicket(index);
                     if(!MT5::PositionSelectByTicket(pos_ticket))
                        continue;
                     string pos_symbol = MT5::PositionGetString(POSITION_SYMBOL);
                     // 账户删选
                     bool pos_check = false;
                     if(m_select_magic.IsPressed())
                       {
                        bool this_check = MT5::PositionGetInteger(POSITION_MAGIC) == magic_number && magic_number!=NULL;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 交易所删选
                     if(m_select_exchange.IsPressed())
                       {
                        bool this_check = SymbolInfoString(pos_symbol,SYMBOL_EXCHANGE) == exchange;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 品种删选
                     if(m_select_product.IsPressed())
                       {
                        bool this_check = SymbolInfoString(pos_symbol,SYMBOL_CATEGORY) == product;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 合约删选
                     if(m_select_symbol.IsPressed())
                       {
                        bool this_check = pos_symbol == order_symbol;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 方向筛选
                     if(m_select_buysell.IsPressed())
                       {
                        bool this_check = MT5::PositionGetInteger(POSITION_TYPE) == type;
                        if(pos_check)
                          {
                           pos_check = pos_check && this_check;
                          }
                        else
                          {
                           pos_check = this_check;
                          }
                       }
                     // 通过检查/报单
                     if(pos_check)
                       {
                        ENUM_ORDER_TYPE order_type = (MT5::PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY)?ORDER_TYPE_SELL:ORDER_TYPE_BUY;
                        double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(pos_symbol,SYMBOL_ASK):SymbolInfoDouble(pos_symbol,SYMBOL_BID);
                        ZeroMemory(order_req);
                        ZeroMemory(order_res);
                        order_req.action   = TRADE_ACTION_DEAL;
                        order_req.symbol   = pos_symbol;
                        order_req.price    = order_price;
                        order_req.volume   = MT5::PositionGetDouble(POSITION_VOLUME);
                        order_req.type     = order_type;
                        order_req.magic    = MT5::PositionGetInteger(POSITION_MAGIC);
                        order_req.type_filling = ORDER_FILLING_RETURN;
                        //
                        MT5::OrderSend(order_req,order_res);
                        // 报单频率限制
                        Sleep(10);
                       }
                    }
                 }
               else  // 选中操作
                 {
                  //
                  ENUM_ORDER_TYPE order_type = (type==POSITION_TYPE_BUY)?ORDER_TYPE_SELL:ORDER_TYPE_BUY;
                  double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(order_symbol,SYMBOL_ASK):SymbolInfoDouble(order_symbol,SYMBOL_BID);
                  ZeroMemory(order_req);
                  ZeroMemory(order_res);
                  order_req.action   = TRADE_ACTION_DEAL;
                  order_req.symbol   = order_symbol;
                  order_req.price    = order_price;
                  order_req.volume   = MT5::PositionGetDouble(POSITION_VOLUME);
                  order_req.type     = order_type;
                  order_req.magic    = MT5::PositionGetInteger(POSITION_MAGIC);
                  order_req.type_filling = ORDER_FILLING_RETURN;
                  //
                  MT5::OrderSend(order_req,order_res);
                 }
               return;
              }
           }
         return;
        }
      if(lparam==m_pos_back_button.Id())
        {
         string order_symbol = m_symbol_text.GetValue();
         bool is_custom;
         SymbolExist(order_symbol,is_custom);
         if(is_custom && CTP::AccountExists())
           {
            // 读取当前持仓信息
            int row_pos = m_position_table.SelectedItem();
            string pos_ticket = m_position_table.GetValue(0,row_pos);
            char   buysell = '9';
            char   tdyd = '9';
            if(CTP::PositionSelectByTicket(pos_ticket))
              {
               buysell = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection);
               tdyd = (char)CTP::PositionGetInteger(CTP::POSITION_PositionDate);
              }
            string account = CTP::AccountInfoString(CTP::ACCOUNT_AccountID);
            string exchange = SymbolInfoString(order_symbol,SYMBOL_EXCHANGE);
            string product = CTP::SymbolInfoString(order_symbol,CTP::SYMBOL_ProductID);
            // 报单
            MqlTradeRequest order_req = {0};
            MqlTradeResult order_res = {0};
            // 是否持仓筛选/遍历持仓/按筛选条件执行操作
            bool cond_check = m_select_account.IsPressed()  ||
                              m_select_exchange.IsPressed() ||
                              m_select_product.IsPressed()  ||
                              m_select_symbol.IsPressed()   ||
                              m_select_buysell.IsPressed()  ;

            if(cond_check)
              {
               for(int index = CTP::PositionsTotal(); index>0; index--)
                 {
                  if(!CTP::PositionSelect(index))
                     continue;
                  long pos_volume = CTP::PositionGetInteger(CTP::POSITION_Position);
                  if(pos_volume<=0)
                     continue;
                  string pos_symbol = CTP::PositionGetSymbol();
                  // 账户删选
                  bool pos_check = false;
                  if(m_select_account.IsPressed())
                    {
                     bool this_check = CTP::PositionGetString(CTP::POSITION_InvestorID) == account;
                     if(pos_check)
                       {
                        pos_check = pos_check && this_check;
                       }
                     else
                       {
                        pos_check = this_check;
                       }
                    }
                  // 交易所删选
                  if(m_select_exchange.IsPressed())
                    {
                     bool this_check = CTP::PositionGetString(CTP::POSITION_ExchangeID) == exchange;
                     if(pos_check)
                       {
                        pos_check = pos_check && this_check;
                       }
                     else
                       {
                        pos_check = this_check;
                       }
                    }
                  // 品种删选
                  if(m_select_product.IsPressed())
                    {
                     bool this_check = CTP::SymbolInfoString(pos_symbol,CTP::SYMBOL_ProductID) == product;
                     if(pos_check)
                       {
                        pos_check = pos_check && this_check;
                       }
                     else
                       {
                        pos_check = this_check;
                       }
                    }
                  // 合约删选
                  if(m_select_symbol.IsPressed())
                    {
                     bool this_check = pos_symbol == order_symbol;
                     if(pos_check)
                       {
                        pos_check = pos_check && this_check;
                       }
                     else
                       {
                        pos_check = this_check;
                       }
                    }
                  // 方向筛选
                  if(m_select_buysell.IsPressed())
                    {
                     bool this_check = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection) == buysell;
                     if(pos_check)
                       {
                        pos_check = pos_check && this_check;
                       }
                     else
                       {
                        pos_check = this_check;
                       }
                    }
                  // 通过检查/报单
                  if(pos_check)
                    {
                     //
                     buysell = (char)CTP::PositionGetInteger(CTP::POSITION_PosiDirection);
                     ENUM_ORDER_TYPE order_type = (buysell==CTP::POSITION_TYPE_BUY)?ORDER_TYPE_SELL:ORDER_TYPE_BUY;
                     double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(pos_symbol,SYMBOL_ASK):SymbolInfoDouble(pos_symbol,SYMBOL_BID);
                     int order_pos = -1;
                     if(exchange == "SHFE" || exchange == "INE")
                       {
                        if((char)CTP::PositionGetInteger(CTP::POSITION_PositionDate)=='1')
                          {
                           order_pos = -2;
                          }
                       }
                     // 平仓
                     bool order_act = false;
#ifdef __TRADE_CLASS__
                     order_act = m_trade.PositionClose(pos_symbol,order_type,pos_volume,order_price,order_pos,0);
#else
                     ZeroMemory(order_req);
                     ZeroMemory(order_res);
                     order_req.action   = TRADE_ACTION_DEAL;
                     order_req.symbol   = pos_symbol;
                     order_req.price    = order_price;
                     order_req.volume   = (double)pos_volume;
                     order_req.type     = order_type;
                     order_req.type_filling = ORDER_FILLING_RETURN;
                     order_req.position = order_pos;
                     //
                     order_act = CTP::OrderSend(order_req,order_res);
#endif
                     // 报单频率限制
                     Sleep(10);
                     // 开仓
                     bool order_act_back = false;
#ifdef __TRADE_CLASS__
                     order_act_back = m_trade.PositionOpen(pos_symbol,order_type,pos_volume,order_price,0);
#else
                     ZeroMemory(order_req);
                     ZeroMemory(order_res);
                     order_req.action   = TRADE_ACTION_DEAL;
                     order_req.symbol   = pos_symbol;
                     order_req.price    = order_price;
                     order_req.volume   = (double)pos_volume;
                     order_req.type     = order_type;
                     order_req.type_filling = ORDER_FILLING_RETURN;
                     //
                     order_act_back = CTP::OrderSend(order_req,order_res);
#endif
                     // 报单频率限制
                     Sleep(10);
                    }
                 }
              }
            else  // 选中操作
              {
               //
               ENUM_ORDER_TYPE order_type = (buysell==CTP::POSITION_TYPE_BUY)?ORDER_TYPE_SELL:ORDER_TYPE_BUY;
               double order_price = (order_type==ORDER_TYPE_BUY)?SymbolInfoDouble(order_symbol,SYMBOL_ASK):SymbolInfoDouble(order_symbol,SYMBOL_BID);
               int order_pos = -1;
               if(exchange == "SHFE" || exchange == "INE")
                 {
                  if(tdyd=='1')
                    {
                     order_pos = -2;
                    }
                 }
               double order_lots = StringToDouble(m_volume_text.GetValue());
               // 平仓
               bool order_act = false;
#ifdef __TRADE_CLASS__
               order_act = m_trade.PositionClose(order_symbol,order_type,(long)order_lots,order_price,order_pos,0);
#else
               ZeroMemory(order_req);
               ZeroMemory(order_res);
               order_req.action   = TRADE_ACTION_DEAL;
               order_req.symbol   = order_symbol;
               order_req.price    = order_price;
               order_req.volume   = order_lots;
               order_req.type     = order_type;
               order_req.type_filling = ORDER_FILLING_RETURN;
               order_req.position = order_pos;
               //
               order_act = CTP::OrderSend(order_req,order_res);
#endif
               // 报单频率限制
               Sleep(10);
               // 开仓
               bool order_act_back = false;
#ifdef __TRADE_CLASS__
               order_act_back = m_trade.PositionOpen(order_symbol,order_type,(long)order_lots,order_price,0);
#else
               ZeroMemory(order_req);
               ZeroMemory(order_res);
               order_req.action   = TRADE_ACTION_DEAL;
               order_req.symbol   = order_symbol;
               order_req.price    = order_price;
               order_req.volume   = order_lots;
               order_req.type     = order_type;
               order_req.type_filling = ORDER_FILLING_RETURN;
               //
               order_act_back = CTP::OrderSend(order_req,order_res);
#endif
              }
           }
         return;
        }
      if(lparam==m_pos_sync_button.Id())
        {
         Print("功能尚未开放");
         return;
        }
      if(lparam==m_pos_sltp_button.Id())
        {
         string order_symbol = m_symbol_text.GetValue();
         bool is_custom;
         SymbolExist(order_symbol,is_custom);
         if(is_custom && CTP::AccountExists())
           {
            if(!m_position_mt.IsVisible())
              {
               // 读取当前持仓信息
               int row_pos = m_position_table.SelectedItem();
               string pos_ticket = m_position_table.GetValue(0,row_pos);
               int pos_index = CTP::PositionsTotal();
               for(; pos_index>0; pos_index--)
                 {
                  if(CTP::PositionGetTicket(pos_index) == pos_ticket)
                    {
                     break;
                    }
                 }
               double new_pos_sl = m_stoploss_text.IsPressed()?StringToDouble(m_stoploss_text.GetValue()):0;
               double new_pos_tp = m_takeprofit_text.IsPressed()?StringToDouble(m_takeprofit_text.GetValue()):0;
               bool order_act = false;
#ifdef __TRADE_CLASS__
               order_act = m_trade.PositionModify(pos_index,new_pos_sl,new_pos_tp);
#else
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               order_req.action   = TRADE_ACTION_SLTP;
               order_req.symbol   = order_symbol;
               order_req.position = pos_index;
               if(new_pos_sl>0)
                 {
                  order_req.sl    = new_pos_sl;
                 }
               if(new_pos_tp>0)
                 {
                  order_req.tp    = new_pos_tp;
                 }
               //
               order_act = CTP::OrderSend(order_req,order_res);
#endif
              }
            else
              {
               // 读取当前持仓信息
               int row_pos = m_position_mt.SelectedItem();
               ulong ticket = StringToInteger(m_position_mt.GetValue(1,row_pos));
               if(!MT5::PositionSelectByTicket(ticket))
                  return;
               double new_pos_sl = m_stoploss_text.IsPressed()?StringToDouble(m_stoploss_text.GetValue()):MT5::PositionGetDouble(POSITION_SL);
               double new_pos_tp = m_takeprofit_text.IsPressed()?StringToDouble(m_takeprofit_text.GetValue()):MT5::PositionGetDouble(POSITION_TP);
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               order_req.action   = TRADE_ACTION_SLTP;
               order_req.symbol   = order_symbol;
               order_req.position = ticket;
               order_req.sl       = new_pos_sl;
               order_req.tp       = new_pos_tp;
               //
               MT5::OrderSend(order_req,order_res);
              }
           }
         return;
        }
      // 订单修改价格/只对选中合约有效
      if(lparam==m_order_modprice_button.Id())
        {
         string order_symbol = m_symbol_text.GetValue();
         bool is_custom;
         SymbolExist(order_symbol,is_custom);
         if(is_custom && CTP::AccountExists())
           {
            // 读取当前订单信息
            int row_pos = m_order_table.SelectedItem();
            string order_ticket = m_order_table.GetValue(0,row_pos);
            int order_index = CTP::OrdersTotal();
            for(; order_index>0; order_index--)
              {
               if(CTP::OrderGetTicket(order_index) == order_ticket)
                 {
                  break;
                 }
              }
            double old_price = 0;
            long old_volume = 0;
            double old_stoploss = 0;
            double old_takeprofit = 0;
            if(CTP::OrderSelectByTicket(order_ticket))
              {
               old_price = CTP::OrderGetDouble(CTP::ORDER_StopPrice);
               old_price = old_price>0?old_price:CTP::OrderGetDouble(CTP::ORDER_LimitPrice);
               old_volume = CTP::OrderGetInteger(CTP::ORDER_VolumeTotalOriginal);
               old_stoploss = CTP::OrderGetDouble(CTP::ORDER_StopLoss);
               old_takeprofit = CTP::OrderGetDouble(CTP::ORDER_TakeProfit);
              }
            double new_price = StringToDouble(m_price_text.GetValue());
            if(MathAbs(new_price-old_price)>0 && new_price>0)
              {
               bool order_act = false;
#ifdef __TRADE_CLASS__
               order_act = m_trade.OrderModify(order_index,old_volume,new_price,new_price);
#else
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               order_req.action   = TRADE_ACTION_MODIFY;
               order_req.symbol   = order_symbol;
               order_req.order    = order_index;
               order_req.price    = new_price;
               order_req.stoplimit= new_price;
               order_req.volume   = (double)old_volume;
               order_req.sl       = old_stoploss;
               order_req.tp       = old_takeprofit;
               //
               order_act = CTP::OrderSend(order_req,order_res);
#endif
              }
           }
         return;
        }
      // 订单修改数量/只对选中合约有效
      if(lparam==m_order_modvolume_button.Id())
        {
         string order_symbol = m_symbol_text.GetValue();
         bool is_custom;
         SymbolExist(order_symbol,is_custom);
         if(is_custom && CTP::AccountExists())
           {
            // 读取当前订单信息
            int row_pos = m_order_table.SelectedItem();
            string order_ticket = m_order_table.GetValue(0,row_pos);
            int order_index = CTP::OrdersTotal();
            for(; order_index>0; order_index--)
              {
               if(CTP::OrderGetTicket(order_index) == order_ticket)
                 {
                  break;
                 }
              }
            double old_price = 0;
            long old_volume = 0;
            double old_stoploss = 0;
            double old_takeprofit = 0;
            if(CTP::OrderSelectByTicket(order_ticket))
              {
               old_price = CTP::OrderGetDouble(CTP::ORDER_StopPrice);
               old_price = old_price>0?old_price:CTP::OrderGetDouble(CTP::ORDER_LimitPrice);
               old_volume = CTP::OrderGetInteger(CTP::ORDER_VolumeTotalOriginal);
               old_stoploss = CTP::OrderGetDouble(CTP::ORDER_StopLoss);
               old_takeprofit = CTP::OrderGetDouble(CTP::ORDER_TakeProfit);
              }
            double new_volume = StringToDouble(m_volume_text.GetValue());
            if(MathAbs(new_volume-old_volume)>0 && new_volume>0)
              {
               bool order_act = false;
#ifdef __TRADE_CLASS__
               order_act = m_trade.OrderModify(order_index,(int)new_volume,old_price,old_price);
#else
               MqlTradeRequest order_req = {0};
               MqlTradeResult order_res = {0};
               order_req.action   = TRADE_ACTION_MODIFY;
               order_req.symbol   = order_symbol;
               order_req.order    = order_index;
               order_req.price    = old_price;
               order_req.stoplimit= old_price;
               order_req.volume   = new_volume;
               order_req.sl       = old_stoploss;
               order_req.tp       = old_takeprofit;
               //
               order_act = CTP::OrderSend(order_req,order_res);
#endif
              }
           }
         return;
        }
      // 撤单/组合选择有效
      if(lparam==m_order_del_button.Id())
        {
         string order_symbol = m_symbol_text.GetValue();
         bool is_custom;
         SymbolExist(order_symbol,is_custom);
         if(is_custom && CTP::AccountExists())
           {
            // 读取当前订单信息
            int row_pos = m_order_table.SelectedItem();
            string order_ticket = m_order_table.GetValue(0,row_pos);
            char   buysell = '9';
            if(CTP::OrderSelectByTicket(order_ticket))
              {
               buysell = (char)CTP::OrderGetInteger(CTP::ORDER_Direction);
              }
            string account = CTP::AccountInfoString(CTP::ACCOUNT_AccountID);
            string exchange = SymbolInfoString(order_symbol,SYMBOL_EXCHANGE);
            string product = CTP::SymbolInfoString(order_symbol,CTP::SYMBOL_ProductID);
            // 撤单
            MqlTradeRequest order_req = {0};
            MqlTradeResult order_res = {0};
            // 是否持仓筛选/遍历持仓/按筛选条件执行操作
            bool cond_check = m_select_account.IsPressed()  ||
                              m_select_exchange.IsPressed() ||
                              m_select_product.IsPressed()  ||
                              m_select_symbol.IsPressed()   ||
                              m_select_buysell.IsPressed()  ;

            if(cond_check)
              {
               for(int index = CTP::OrdersTotal(); index>0; index--)
                 {
                  if(!CTP::OrderSelect(index))
                     continue;
                  string pos_symbol = CTP::OrderGetString(CTP::ORDER_InstrumentID);
                  // 账户删选
                  bool order_check = false;
                  if(m_select_account.IsPressed())
                    {
                     bool this_check = CTP::OrderGetString(CTP::ORDER_AccountID) == account;
                     if(order_check)
                       {
                        order_check = order_check && this_check;
                       }
                     else
                       {
                        order_check = this_check;
                       }
                    }
                  // 交易所删选
                  if(m_select_exchange.IsPressed())
                    {
                     bool this_check = CTP::OrderGetString(CTP::ORDER_ExchangeID) == exchange;
                     if(order_check)
                       {
                        order_check = order_check && this_check;
                       }
                     else
                       {
                        order_check = this_check;
                       }
                    }
                  // 品种删选
                  if(m_select_product.IsPressed())
                    {
                     bool this_check = CTP::SymbolInfoString(pos_symbol,CTP::SYMBOL_ProductID) == product;
                     if(order_check)
                       {
                        order_check = order_check && this_check;
                       }
                     else
                       {
                        order_check = this_check;
                       }
                    }
                  // 合约删选
                  if(m_select_symbol.IsPressed())
                    {
                     bool this_check = pos_symbol == order_symbol;
                     if(order_check)
                       {
                        order_check = order_check && this_check;
                       }
                     else
                       {
                        order_check = this_check;
                       }
                    }
                  // 方向筛选
                  if(m_select_buysell.IsPressed())
                    {
                     bool this_check = (char)CTP::OrderGetInteger(CTP::ORDER_Direction) == buysell;
                     if(order_check)
                       {
                        order_check = order_check && this_check;
                       }
                     else
                       {
                        order_check = this_check;
                       }
                    }
                  // 通过检查/报单
                  if(order_check)
                    {
                     bool order_act = false;
#ifdef __TRADE_CLASS__
                     order_act = m_trade.OrderDelete(index);
#else
                     ZeroMemory(order_req);
                     ZeroMemory(order_res);
                     order_req.action   = TRADE_ACTION_REMOVE;
                     order_req.symbol   = pos_symbol;
                     order_req.order    = index;
                     //
                     order_act = CTP::OrderSend(order_req,order_res);
#endif
                     // 报单频率限制
                     Sleep(10);
                    }
                 }
              }
            else  // 选中操作
              {
               //
               int order_index = CTP::OrdersTotal();
               for(; order_index>0; order_index--)
                 {
                  if(CTP::OrderGetTicket(order_index) == order_ticket)
                    {
                     break;
                    }
                 }
               bool order_act = false;
#ifdef __TRADE_CLASS__
               order_act = m_trade.OrderDelete(order_index);
#else
               ZeroMemory(order_req);
               ZeroMemory(order_res);
               order_req.action   = TRADE_ACTION_REMOVE;
               order_req.symbol   = order_symbol;
               order_req.order    = order_index;
               //
               order_act = CTP::OrderSend(order_req,order_res);
#endif
              }
           }
         return;
        }
      // 镜像挂单/组合选择有效
      if(lparam==m_order_back_button.Id())
        {
         Print("功能尚未开放");
         return;
        }
      // 一键全撤
      if(lparam==m_order_delall_button.Id())
        {
         if(CTP::AccountExists())
           {
            MqlTradeRequest order_req = {0};
            MqlTradeResult order_res = {0};
            for(int order_index = CTP::OrdersTotal(); order_index>0; order_index--)
              {
               if(!CTP::OrderSelect(order_index))
                  continue;
               // 撤单
               bool order_act = false;
#ifdef __TRADE_CLASS__
               order_act = m_trade.OrderDelete(order_index);
#else
               ZeroMemory(order_req);
               ZeroMemory(order_res);
               order_req.action   = TRADE_ACTION_REMOVE;
               order_req.symbol   = CTP::OrderGetString(CTP::ORDER_InstrumentID);
               order_req.order    = order_index;
               //
               order_act = CTP::OrderSend(order_req,order_res);
#endif
              }
           }
         return;
        }
      // 订单修改止损止盈/只对选中有效
      if(lparam==m_order_sltp_button.Id())
        {
         string order_symbol = m_symbol_text.GetValue();
         bool is_custom;
         SymbolExist(order_symbol,is_custom);
         if(is_custom && CTP::AccountExists())
           {
            // 读取当前订单信息
            int row_pos = m_order_table.SelectedItem();
            string order_ticket = m_order_table.GetValue(0,row_pos);
            int order_index = CTP::OrdersTotal();
            for(; order_index>0; order_index--)
              {
               if(CTP::OrderGetTicket(order_index) == order_ticket)
                 {
                  break;
                 }
              }
            //
            double old_price = 0;
            long old_volume = 0;
            double old_stoploss = 0;
            double old_takeprofit = 0;
            if(CTP::OrderSelectByTicket(order_ticket))
              {
               old_price = CTP::OrderGetDouble(CTP::ORDER_StopPrice);
               old_price = old_price>0?old_price:CTP::OrderGetDouble(CTP::ORDER_LimitPrice);
               old_volume = CTP::OrderGetInteger(CTP::ORDER_VolumeTotalOriginal);
               old_stoploss = CTP::OrderGetDouble(CTP::ORDER_StopLoss);
               old_takeprofit = CTP::OrderGetDouble(CTP::ORDER_TakeProfit);
              }
            //
            double new_sl = m_stoploss_text.IsPressed()?StringToDouble(m_stoploss_text.GetValue()):0;
            double new_tp = m_takeprofit_text.IsPressed()?StringToDouble(m_takeprofit_text.GetValue()):0;
            //
            bool order_act = false;
#ifdef __TRADE_CLASS__
            order_act = m_trade.OrderModify(order_index,new_sl,new_tp);
#else
            MqlTradeRequest order_req = {0};
            MqlTradeResult order_res = {0};
            order_req.action   = TRADE_ACTION_MODIFY;
            order_req.symbol   = order_symbol;
            order_req.order    = order_index;
            order_req.price    = old_price;
            order_req.stoplimit= old_price;
            order_req.volume   = (double)old_volume;
            if(new_sl>0)
              {
               order_req.sl    = new_sl;
              }
            if(new_tp>0)
              {
               order_req.tp    = new_tp;
              }
            //
            order_act = CTP::OrderSend(order_req,order_res);
#endif
           }
         return;
        }
      return;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
// 检查表中是否有对应的ticket
int ToolBox::CheckTicket(CTable& table,const string ticket)
  {
   int result = (int)table.RowsTotal()-1;
   for(int row = result; row >= 0; result = --row)
     {
      string table_str = table.GetValue(0,row);
      if(table_str == ticket)
        {
         break;
        }
     }
   return(result);
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
